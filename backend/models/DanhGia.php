<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ad_danh_gia".
 *
 * @property int $id
 * @property string|null $noi_dung
 * @property int|null $active
 * @property int|null $rate
 * @property string|null $created
 * @property string|null $updated
 * @property int|null $doanh_nghiep_id
 * @property int|null $user_created_id
 * @property string|null $type
 *
 * @property User $doanhNghiep
 * @property User $userCreated
 */
class DanhGia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ad_danh_gia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['noi_dung', 'type'], 'string'],
            [['active', 'rate', 'doanh_nghiep_id', 'user_created_id'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['doanh_nghiep_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['doanh_nghiep_id' => 'id']],
            [['user_created_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_created_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'noi_dung' => 'Nội dung',
            'active' => 'Active',
            'rate' => 'Đánh giá',
            'created' => 'Ngày đánh giá',
            'updated' => 'Cập nhật',
            'doanh_nghiep_id' => 'Doanh Nghiep ID',
            'user_created_id' => 'User Created ID',
            'type' => 'Type',
        ];
    }

    /**
     * Gets query for [[DoanhNghiep]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDoanhNghiep()
    {
        return $this->hasOne(User::className(), ['id' => 'doanh_nghiep_id']);
    }

    /**
     * Gets query for [[UserCreated]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUserCreated()
    {
        return $this->hasOne(User::className(), ['id' => 'user_created_id']);
    }
}
