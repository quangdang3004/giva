<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ad_user".
 *
 * @property int $id
 * @property string|null $username
 * @property string|null $password_hash
 * @property string|null $exp_token_reset
 * @property string|null $hoten
 * @property string|null $email
 * @property string|null $dien_thoai
 * @property string|null $auth_key
 * @property int|null $exp_auth_key
 * @property string|null $avatar
 * @property float|null $so_du
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $type
 * @property string|null $trang_thai
 * @property int|null $status
 * @property int|null $doanh_nghiep_id
 * @property string|null $password_reset_token
 * @property string|null $password
 *
 * @property ChiTietTienDoGiaoDich[] $chiTietTienDoGiaoDiches
 * @property DanhGia[] $danhGias
 * @property DanhGia[] $danhGias0
 * @property DoanhNghiep[] $doanhNghieps
 * @property DonHang[] $donHangs
 * @property GiaoDich[] $giaoDiches
 * @property GiaoDich[] $giaoDiches0
 * @property GiaoDich[] $giaoDiches1
 * @property GiayToLienKet[] $giayToLienKets
 * @property LichSuTrangThaiDonHang[] $lichSuTrangThaiDonHangs
 * @property LichSuTrangThaiUser[] $lichSuTrangThaiUsers
 * @property TrangThaiGiaoDich[] $trangThaiGiaoDiches
 * @property DoanhNghiep $doanhNghiep
// * @property NhaTuyenDungCongTac[] $nhaTuyenDungCongTacs
 * @property Vaitrouser[] $vaitrousers
 */
class User extends \yii\db\ActiveRecord
{
    const CHUA_XAC_MINH = 'Chưa xác minh';
    const DA_XAC_MINH = 'Đã xác minh';
    const DANG_XAC_MINH = 'Đang xác minh';


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ad_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['exp_auth_key', 'status', 'doanh_nghiep_id'], 'integer'],
            [['avatar', 'type', 'trang_thai'], 'string'],
            [['so_du'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['username', 'email'], 'string', 'max' => 50],
            [['password_hash', 'hoten', 'auth_key'], 'string', 'max' => 100],
            [['exp_token_reset'], 'string', 'max' => 32],
            [['dien_thoai'], 'string', 'max' => 13],
            [['password_reset_token', 'password'], 'string', 'max' => 20],
            [['doanh_nghiep_id'], 'exist', 'skipOnError' => true, 'targetClass' => DoanhNghiep::className(), 'targetAttribute' => ['doanh_nghiep_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password_hash' => 'Password Hash',
            'exp_token_reset' => 'Exp Token Reset',
            'hoten' => 'Họ tên',
            'email' => 'Email',
            'dien_thoai' => 'Điện thoại',
            'auth_key' => 'Auth Key',
            'exp_auth_key' => 'Exp Auth Key',
            'avatar' => 'Avatar',
            'so_du' => 'Số dư',
            'created_at' => 'Ngày tạo',
            'updated_at' => 'Ngày cập nhật',
            'type' => 'Vai trò',
            'trang_thai' => 'Trạng thái',
            'status' => 'Status',
            'doanh_nghiep_id' => 'ID doanh nghiệp',
            'password_reset_token' => 'Password Reset Token',
            'password' => 'Password',
        ];
    }

    /**
     * Gets query for [[ChiTietTienDoGiaoDiches]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChiTietTienDoGiaoDiches()
    {
        return $this->hasMany(ChiTietTienDoGiaoDich::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[DanhGias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDanhGias()
    {
        return $this->hasMany(DanhGia::className(), ['doanh_nghiep_id' => 'id']);
    }

    /**
     * Gets query for [[DanhGias0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDanhGias0()
    {
        return $this->hasMany(DanhGia::className(), ['user_created_id' => 'id']);
    }

    /**
     * Gets query for [[DoanhNghieps]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDoanhNghieps()
    {
        return $this->hasMany(DoanhNghiep::className(), ['user_created' => 'id']);
    }

    /**
     * Gets query for [[DonHangs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDonHangs()
    {
        return $this->hasMany(DonHang::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[GiaoDiches]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGiaoDiches()
    {
        return $this->hasMany(GiaoDich::className(), ['user_created' => 'id']);
    }

    /**
     * Gets query for [[GiaoDiches0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGiaoDiches0()
    {
        return $this->hasMany(GiaoDich::className(), ['nguoi_cung_cap_id' => 'id']);
    }

    /**
     * Gets query for [[GiaoDiches1]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGiaoDiches1()
    {
        return $this->hasMany(GiaoDich::className(), ['nguoi_nhan_id' => 'id']);
    }

    /**
     * Gets query for [[GiayToLienKets]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGiayToLienKets()
    {
        return $this->hasMany(GiayToLienKet::className(), ['nha_tuyen_dung_id' => 'id']);
    }

    /**
     * Gets query for [[LichSuTrangThaiDonHangs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLichSuTrangThaiDonHangs()
    {
        return $this->hasMany(LichSuTrangThaiDonHang::className(), ['user_createad_id' => 'id']);
    }

    /**
     * Gets query for [[LichSuTrangThaiUsers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLichSuTrangThaiUsers()
    {
        return $this->hasMany(LichSuTrangThaiUser::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[TrangThaiGiaoDiches]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTrangThaiGiaoDiches()
    {
        return $this->hasMany(TrangThaiGiaoDich::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[DoanhNghiep]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDoanhNghiep()
    {
        return $this->hasOne(DoanhNghiep::className(), ['id' => 'doanh_nghiep_id']);
    }



    /**
     * Gets query for [[Vaitrousers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVaitrousers()
    {
        return $this->hasMany(Vaitrouser::className(), ['user_id' => 'id']);
    }

    public static function getListUserDisplay(){
        return [
            self::CHUA_XAC_MINH => '<span class="text-danger"><strong>Chưa xác minh</strong></span>',
            self::DANG_XAC_MINH => '<span class="text-warning"><strong>Đang xác minh</strong></span>',
            self::DA_XAC_MINH => '<span class="text-success"><strong>Đã xác minh</strong></span>'
        ];
    }

    static function getLinkAnh(){
        $link_hinh_anh_he_thong = \backend\models\CauHinh::findOne(['code' => 'images'])->value;

        return $link_hinh_anh_he_thong;
    }

    public static function getListTrangThai(){
        return [
          self::CHUA_XAC_MINH => self::CHUA_XAC_MINH,
          self::DANG_XAC_MINH => self::DANG_XAC_MINH,
          self::DA_XAC_MINH => self::DA_XAC_MINH,
        ];
    }
}
