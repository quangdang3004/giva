<?php

namespace backend\models;

use common\models\myAPI;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "ad_don_hang".
 *
 * @property int $id
 * @property int|null $cong_ty_id
 * @property int|null $nganh_nghe_id
 * @property float|null $bao_gia
 * @property string|null $gioi_tinh
 * @property string|null $trinh_do
 * @property int|null $tinh_thanh_id
 * @property int|null $tuoi_tu
 * @property int|null $tuoi_den
 * @property int|null $viem_gan_b
 * @property string|null $xam_tro
 * @property float|null $phi_don_hang
 * @property float|null $back_don_hang
 * @property float|null $tien_ho_tro
 * @property string|null $ngay_thi
 * @property string|null $ghi_chu
 * @property int|null $so_luong_can_tuyen
 * @property int|null $so_luong_da_tuyen
 * @property string|null $ghi_chu_cho_don
 * @property string|null $created
 * @property string|null $updated
 * @property int|null $so_ngay_mua
 * @property string|null $han_tuyen
 * @property int|null $parent_id
 * @property string|null $trang_thai
 * @property int|null $user_id
 * @property int|null $active
 *
 * @property AnhLienKetDonHang[] $anhLienKetDonHangs
 * @property DanhMuc $nganhNghe
 * @property DanhMuc $tinhThanh
 * @property DoanhNghiep $congTy
 * @property DonHang $parent
 * @property DonHang[] $donHangs
 * @property User $user
 * @property GiaoDich[] $giaoDiches
 * @property LichSuTrangThaiDonHang[] $lichSuTrangThaiDonHangs
 */
class DonHang extends \yii\db\ActiveRecord
{
    const DANG_TUYEN = 'Đang tuyển';
    const DA_DU = 'Đã đủ';
    const HET_HAN = 'Hết hạn';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ad_don_hang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cong_ty_id', 'nganh_nghe_id', 'tinh_thanh_id', 'tuoi_tu', 'tuoi_den', 'viem_gan_b', 'so_luong_can_tuyen',
                'so_luong_da_tuyen', 'so_ngay_mua', 'parent_id', 'user_id', 'active'], 'integer'],
            [['bao_gia', 'phi_don_hang', 'back_don_hang', 'tien_ho_tro'], 'number'],
            [['gioi_tinh', 'trinh_do', 'xam_tro', 'ghi_chu', 'ghi_chu_cho_don', 'trang_thai'], 'string'],
            [['ngay_thi', 'created', 'updated', 'han_tuyen'], 'safe'],
            [['nganh_nghe_id'], 'exist', 'skipOnError' => true, 'targetClass' => DanhMuc::className(), 'targetAttribute' => ['nganh_nghe_id' => 'id']],
            [['tinh_thanh_id'], 'exist', 'skipOnError' => true, 'targetClass' => DanhMuc::className(), 'targetAttribute' => ['tinh_thanh_id' => 'id']],
            [['cong_ty_id'], 'exist', 'skipOnError' => true, 'targetClass' => DoanhNghiep::className(), 'targetAttribute' => ['cong_ty_id' => 'id']],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => DonHang::className(), 'targetAttribute' => ['parent_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cong_ty_id' => 'Cong Ty ID',
            'nganh_nghe_id' => 'Nganh Nghe ID',
            'bao_gia' => 'Bao Gia',
            'gioi_tinh' => 'Gioi Tinh',
            'trinh_do' => 'Trinh Do',
            'ghi_chu' => 'Ghi Chu',
            'so_luong_can_tuyen' => 'So Luong Can Tuyen',
            'so_luong_da_tuyen' => 'So Luong Da Tuyen',
            'created' => 'Created',
            'updated' => 'Updated',
            'han_tuyen' => 'Han Tuyen',
            'parent_id' => 'Parent ID',
            'trang_thai' => 'Trang Thai',
            'user_id' => 'User ID',
            'active' => 'Active',
        ];
    }

    /**
     * Gets query for [[AnhLienKetDonHangs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAnhLienKetDonHangs()
    {
        return $this->hasMany(AnhLienKetDonHang::className(), ['don_hang_id' => 'id']);
    }

    /**
     * Gets query for [[CongTy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCongTy()
    {
        return $this->hasOne(CongTy::className(), ['id' => 'cong_ty_id']);
    }

    /**
     * Gets query for [[NganhNghe]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNganhNghe()
    {
        return $this->hasOne(DanhMuc::className(), ['id' => 'nganh_nghe_id']);
    }

    /**
     * Gets query for [[Parent]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(DonHang::className(), ['id' => 'parent_id']);
    }

    /**
     * Gets query for [[DonHangs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDonHangs()
    {
        return $this->hasMany(DonHang::className(), ['parent_id' => 'id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Gets query for [[GiaoDiches]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGiaoDiches()
    {
        return $this->hasMany(GiaoDich::className(), ['don_hang_id' => 'id']);
    }

    /**
     * Gets query for [[LichSuTrangThaiDonHangs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLichSuTrangThaiDonHangs()
    {
        return $this->hasMany(LichSuTrangThaiDonHang::className(), ['don_hang_id' => 'id']);
    }

    public function beforeSave($insert)
    {
        $this->han_tuyen = $this->han_tuyen ? myAPI::convertDateSaveIntoDb($this->han_tuyen) : null;
        $this->ngay_thi = $this->ngay_thi ? myAPI::convertDateSaveIntoDb($this->ngay_thi) : null;

        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public static function getListTrinhDo(){
        $trinhDo = DonHang::find()->all();

        return ArrayHelper::map($trinhDo, 'trinh_do', 'trinh_do');
    }
}
