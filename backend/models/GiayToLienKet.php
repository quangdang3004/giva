<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ad_giay_to_lien_ket".
 *
 * @property int $id
 * @property int|null $nha_tuyen_dung_id
 * @property int|null $doanh_nghiep_id
 * @property int|null $cong_ti_id
 * @property string|null $link
 * @property int|null $active
 * @property string|null $type
 *
 * @property User $nhaTuyenDung
 * @property CongTy $congTi
 * @property DoanhNghiep $doanhNghiep
 */
class GiayToLienKet extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ad_giay_to_lien_ket';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nha_tuyen_dung_id', 'doanh_nghiep_id', 'cong_ti_id', 'active'], 'integer'],
            [['type'], 'string'],
            [['link'], 'string', 'max' => 150],
            [['nha_tuyen_dung_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['nha_tuyen_dung_id' => 'id']],
            [['cong_ti_id'], 'exist', 'skipOnError' => true, 'targetClass' => CongTy::className(), 'targetAttribute' => ['cong_ti_id' => 'id']],
            [['doanh_nghiep_id'], 'exist', 'skipOnError' => true, 'targetClass' => DoanhNghiep::className(), 'targetAttribute' => ['doanh_nghiep_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nha_tuyen_dung_id' => 'Nha Tuyen Dung ID',
            'doanh_nghiep_id' => 'Doanh Nghiep ID',
            'cong_ti_id' => 'Cong Ti ID',
            'link' => 'Link',
            'active' => 'Active',
            'type' => 'Type',
        ];
    }

    /**
     * Gets query for [[NhaTuyenDung]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNhaTuyenDung()
    {
        return $this->hasOne(User::className(), ['id' => 'nha_tuyen_dung_id']);
    }

    /**
     * Gets query for [[CongTi]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCongTi()
    {
        return $this->hasOne(CongTy::className(), ['id' => 'cong_ti_id']);
    }

    /**
     * Gets query for [[DoanhNghiep]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDoanhNghiep()
    {
        return $this->hasOne(DoanhNghiep::className(), ['id' => 'doanh_nghiep_id']);
    }
}
