<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ad_anh_lien_ket_chi_tiet_tien_do".
 *
 * @property int $id
 * @property int|null $tien_do_id
 * @property string|null $link_anh
 * @property string|null $created
 */
class AnhLienKetChiTietTienDo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ad_anh_lien_ket_chi_tiet_tien_do';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tien_do_id'], 'integer'],
            [['created'], 'safe'],
            [['link_anh'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tien_do_id' => 'Tien Do ID',
            'link_anh' => 'Link Anh',
            'created' => 'Created',
        ];
    }
}
