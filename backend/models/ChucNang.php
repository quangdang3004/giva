<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ad_chuc_nang".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $nhom
 * @property string|null $controller_action
 *
 * @property PhanQuyen[] $phanQuyens
 */
class ChucNang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ad_chuc_nang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'nhom', 'controller_action'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Tên',
            'nhom' => 'Nhóm',
            'controller_action' => 'Controller Action',
        ];
    }

    /**
     * Gets query for [[PhanQuyens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPhanQuyens()
    {
        return $this->hasMany(PhanQuyen::className(), ['chuc_nang_id' => 'id']);
    }
}
