<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ad_trang_thai_giao_dich".
 *
 * @property int $id
 * @property int|null $giao_dich_id
 * @property string|null $trang_thai
 * @property string|null $created
 * @property int|null $user_id
 *
 * @property GiaoDich $giaoDich
 * @property User $user
 */
class TrangThaiGiaoDich extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ad_trang_thai_giao_dich';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['giao_dich_id', 'user_id'], 'integer'],
            [['trang_thai'], 'string'],
            [['created'], 'safe'],
            [['giao_dich_id'], 'exist', 'skipOnError' => true, 'targetClass' => GiaoDich::className(), 'targetAttribute' => ['giao_dich_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'giao_dich_id' => 'Giao Dich ID',
            'trang_thai' => 'Trang Thai',
            'created' => 'Created',
            'user_id' => 'User ID',
        ];
    }

    /**
     * Gets query for [[GiaoDich]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGiaoDich()
    {
        return $this->hasOne(GiaoDich::className(), ['id' => 'giao_dich_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
