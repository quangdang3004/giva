<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ad_nha_tuyen_dung_cong_tac".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $ten_cong_ty
 * @property string|null $thoi_gian_vao
 * @property string|null $thoi_gian_nghi
 * @property string|null $trang_thai
 * @property string|null $trang_thai_lam_viec
 * @property string|null $created
 * @property int|null $active
 *
 * @property User $user
 */
class NhaTuyenDungCongTac extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ad_nha_tuyen_dung_cong_tac';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'active'], 'integer'],
            [['ten_cong_ty', 'trang_thai', 'trang_thai_lam_viec'], 'string'],
            [['thoi_gian_vao', 'thoi_gian_nghi', 'created'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'ten_cong_ty' => 'Ten Cong Ty',
            'thoi_gian_vao' => 'Thoi Gian Vao',
            'thoi_gian_nghi' => 'Thoi Gian Nghi',
            'trang_thai' => 'Trang Thai',
            'trang_thai_lam_viec' => 'Trang Thai Lam Viec',
            'created' => 'Created',
            'active' => 'Active',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
