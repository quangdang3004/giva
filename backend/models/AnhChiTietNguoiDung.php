<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ad_anh_chi_tiet_nguoi_dung".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $link
 * @property string|null $created
 * @property int|null $active
 *
 * @property User $user
 */
class AnhChiTietNguoiDung extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ad_anh_chi_tiet_nguoi_dung';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'active'], 'integer'],
            [['created'], 'safe'],
            [['link'], 'string', 'max' => 150],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'link' => 'Link',
            'created' => 'Created',
            'active' => 'Active',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
