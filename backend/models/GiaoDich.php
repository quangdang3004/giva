<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ad_giao_dich".
 *
 * @property int $id
 * @property int|null $user_created
 * @property int|null $don_hang_id
 * @property int|null $nguoi_cung_cap_id
 * @property int|null $nguoi_nhan_id
 * @property string|null $type
 * @property float|null $so_luong
 * @property string|null $don_vi
 * @property string|null $trang_thai
 * @property string|null $tien_do
 * @property int|null $active
 *
 * @property ChiTietTienDoGiaoDich[] $chiTietTienDoGiaoDiches
 * @property DonHang $donHang
 * @property User $userCreated
 * @property User $nguoiCungCap
 * @property User $nguoiNhan
 * @property TrangThaiGiaoDich[] $trangThaiGiaoDiches
 */
class GiaoDich extends \yii\db\ActiveRecord
{
    const YEU_CAU= 'Yêu cầu';
    const XAC_NHAN = 'Xác nhận';
    const TU_CHOI = 'Từ chối';
    const CHUA_HOAN_THANH = 'Chưa hoàn thành';
    const DA_HOAN_THANH = 'Đã hoàn thành';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ad_giao_dich';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_created', 'don_hang_id', 'nguoi_cung_cap_id', 'nguoi_nhan_id', 'active'], 'integer'],
            [['type', 'don_vi', 'trang_thai', 'tien_do'], 'string'],
            [['so_luong'], 'number'],
            [['don_hang_id'], 'exist', 'skipOnError' => true, 'targetClass' => DonHang::className(), 'targetAttribute' => ['don_hang_id' => 'id']],
            [['user_created'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_created' => 'id']],
            [['nguoi_cung_cap_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['nguoi_cung_cap_id' => 'id']],
            [['nguoi_nhan_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['nguoi_nhan_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_created' => 'Ngày tạo',
            'don_hang_id' => 'ID đơn hàng',
            'nguoi_cung_cap_id' => 'ID người cung cấp',
            'nguoi_nhan_id' => 'ID người nhận',
            'type' => 'Loại giao dịch',
            'so_luong' => 'Số lượng',
            'don_vi' => 'Đơn vị',
            'trang_thai' => 'Trạng thái',
            'tien_do' => 'Tiến độ',
            'active' => 'Active',
        ];
    }

    /**
     * Gets query for [[ChiTietTienDoGiaoDiches]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChiTietTienDoGiaoDiches()
    {
        return $this->hasMany(ChiTietTienDoGiaoDich::className(), ['giao_dich_id' => 'id']);
    }

    /**
     * Gets query for [[DonHang]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDonHang()
    {
        return $this->hasOne(DonHang::className(), ['id' => 'don_hang_id']);
    }

    /**
     * Gets query for [[UserCreated]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUserCreated()
    {
        return $this->hasOne(User::className(), ['id' => 'user_created']);
    }

    /**
     * Gets query for [[NguoiCungCap]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNguoiCungCap()
    {
        return $this->hasOne(User::className(), ['id' => 'nguoi_cung_cap_id']);
    }

    /**
     * Gets query for [[NguoiNhan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNguoiNhan()
    {
        return $this->hasOne(User::className(), ['id' => 'nguoi_nhan_id']);
    }

    /**
     * Gets query for [[TrangThaiGiaoDiches]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTrangThaiGiaoDiches()
    {
        return $this->hasMany(TrangThaiGiaoDich::className(), ['giao_dich_id' => 'id']);
    }

    public static function getListTTGDDisplay(){
        return [
            self::YEU_CAU => '<span class="text-primary">Yêu cầu</span>',
            self::XAC_NHAN => '<span class="text-success">Xác nhận</span>',
            self::TU_CHOI => '<span class="text-danger">Từ chối</span>',
        ];
    }

    public static function getListTTGD(){
        return [
            self::YEU_CAU => self::YEU_CAU ,
            self::XAC_NHAN => self::XAC_NHAN,
            self::TU_CHOI => self::TU_CHOI,
        ];
    }

    public static function getListTDGDDisplay(){
        return [
            self::CHUA_HOAN_THANH =>'<span class="text-danger">Chưa hoàn thành</span>',
            self::DA_HOAN_THANH =>'<span class="text-success">Đã hoàn thành</span>',
        ];
    }

    public static function getListTDGD(){
        return [
            self::CHUA_HOAN_THANH =>self::CHUA_HOAN_THANH,
            self::DA_HOAN_THANH =>self::DA_HOAN_THANH,
        ];
    }
}
