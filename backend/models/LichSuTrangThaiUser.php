<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ad_lich_su_trang_thai_user".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $trang_thai
 * @property string|null $created
 *
 * @property User $user
 */
class LichSuTrangThaiUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ad_lich_su_trang_thai_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['trang_thai'], 'string'],
            [['created'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'trang_thai' => 'Trang Thai',
            'created' => 'Created',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
