<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ad_lich_su_trang_thai_don_hang".
 *
 * @property int $id
 * @property int|null $don_hang_id
 * @property string|null $trang_thai
 * @property int|null $so_luong
 * @property string|null $created
 * @property int|null $user_createad_id
 *
 * @property DonHang $donHang
 * @property User $userCreatead
 */
class LichSuTrangThaiDonHang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ad_lich_su_trang_thai_don_hang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['don_hang_id', 'so_luong', 'user_createad_id'], 'integer'],
            [['trang_thai'], 'string'],
            [['created'], 'safe'],
            [['don_hang_id'], 'exist', 'skipOnError' => true, 'targetClass' => DonHang::className(), 'targetAttribute' => ['don_hang_id' => 'id']],
            [['user_createad_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_createad_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'don_hang_id' => 'Don Hang ID',
            'trang_thai' => 'Trang Thai',
            'so_luong' => 'So Luong',
            'created' => 'Created',
            'user_createad_id' => 'User Createad ID',
        ];
    }

    /**
     * Gets query for [[DonHang]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDonHang()
    {
        return $this->hasOne(DonHang::className(), ['id' => 'don_hang_id']);
    }

    /**
     * Gets query for [[UserCreatead]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUserCreatead()
    {
        return $this->hasOne(User::className(), ['id' => 'user_createad_id']);
    }
}
