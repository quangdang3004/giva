<?php
//
//namespace backend\models;
//
//use Yii;
//
///**
// * This is the model class for table "ad_quan_ly_ung_vien".
// *
// * @property int $id
// * @property string|null $ngay_sinh
// * @property string|null $dien_thoai
// * @property string|null $email
// */
//class QuanLyUngVien extends \yii\db\ActiveRecord
//{
//    /**
//     * {@inheritdoc}
//     */
//    public static function tableName()
//    {
//        return 'ad_quan_ly_ung_vien';
//    }
//
//    /**
//     * {@inheritdoc}
//     */
//    public function rules()
//    {
//        return [
//            [['id'], 'integer'],
//            [['ngay_sinh'], 'safe'],
//            [['dien_thoai'], 'string', 'max' => 20],
//            [['email'], 'string', 'max' => 100],
//        ];
//    }
//
//    /**
//     * {@inheritdoc}
//     */
//    public function attributeLabels()
//    {
//        return [
//            'id' => 'ID',
//            'ngay_sinh' => 'Ngay Sinh',
//            'dien_thoai' => 'Dien Thoai',
//            'email' => 'Email',
//        ];
//    }
//}
