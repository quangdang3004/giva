<?php

namespace backend\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "ad_yeu_cau_xac_thuc".
 *
 * @property int $id
 * @property string|null $type
 * @property int|null $user_id
 * @property int|null $cong_ty_id
 * @property int|null $doanh_nghiep_id
 * @property string|null $trang_thai
 * @property string|null $ghi_chu
 * @property string|null $created
 * @property string|null $updated
 * @property int|null $user_created_id
 * @property int|null $active
 *
 * @property GiayToLienKet[] $giayToLienKets
 * @property CongTy $congTy
 * @property DoanhNghiep $doanhNghiep
 * @property User $userCreated
 * @property User $user
 */
class YeuCauXacThuc extends \yii\db\ActiveRecord
{
    const DA_XAC_MINH = 'Đã xác minh';
    const CHUA_XAC_MINH = 'Chưa xác minh';
    const DANG_XAC_MINH = 'Đang xác minh';
    const TU_CHOI = 'Từ chối';
    const CA_NHAN = 'Cá nhân';
    const DOANH_NGHIEP = 'Doanh nghiệp';
    public $tu_khoa;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ad_yeu_cau_xac_thuc';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'trang_thai'], 'string'],
            [['user_id', 'cong_ty_id', 'doanh_nghiep_id', 'user_created_id', 'active'], 'integer'],
            [['created', 'updated', 'hoten'], 'safe'],
            [['ghi_chu'], 'string', 'max' => 150],
            [['cong_ty_id'], 'exist', 'skipOnError' => true, 'targetClass' => CongTy::className(), 'targetAttribute' => ['cong_ty_id' => 'id']],
            [['doanh_nghiep_id'], 'exist', 'skipOnError' => true, 'targetClass' => DoanhNghiep::className(), 'targetAttribute' => ['doanh_nghiep_id' => 'id']],
            [['user_created_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_created_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Đối tượng xác thực',
            'user_id' => 'User ID',
            'cong_ty_id' => 'Cong Ty ID',
            'doanh_nghiep_id' => 'Doanh Nghiep ID',
            'trang_thai' => 'Trạng thái',
            'ghi_chu' => 'Ghi chú',
            'created' => 'Ngày tạo',
            'updated' => 'Ngày cập nhật',
            'user_created_id' => 'User Created ID',
            'active' => 'Active',
        ];
    }

    /**
     * Gets query for [[GiayToLienKets]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGiayToLienKets()
    {
        return $this->hasMany(GiayToLienKet::className(), ['yeu_cau_id' => 'id']);
    }

    /**
     * Gets query for [[CongTy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCongTy()
    {
        return $this->hasOne(CongTy::className(), ['id' => 'cong_ty_id']);
    }

    /**
     * Gets query for [[DoanhNghiep]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDoanhNghiep()
    {
        return $this->hasOne(DoanhNghiep::className(), ['id' => 'doanh_nghiep_id']);
    }

    /**
     * Gets query for [[UserCreated]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUserCreated()
    {
        return $this->hasOne(User::className(), ['id' => 'user_created_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
