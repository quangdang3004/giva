<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ad_anh_lien_ket_don_hang".
 *
 * @property int $id
 * @property string|null $link
 * @property int|null $don_hang_id
 * @property string|null $created
 * @property int|null $active
 *
 * @property DonHang $donHang
 */
class AnhLienKetDonHang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ad_anh_lien_ket_don_hang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['don_hang_id', 'active'], 'integer'],
            [['created'], 'safe'],
            [['link'], 'string', 'max' => 150],
            [['don_hang_id'], 'exist', 'skipOnError' => true, 'targetClass' => DonHang::className(), 'targetAttribute' => ['don_hang_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'link' => 'Link',
            'don_hang_id' => 'Don Hang ID',
            'created' => 'Created',
            'active' => 'Active',
        ];
    }

    /**
     * Gets query for [[DonHang]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDonHang()
    {
        return $this->hasOne(DonHang::className(), ['id' => 'don_hang_id']);
    }
}
