<?php

namespace backend\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "ad_anh_nguoi_dung".
 *
 * @property int $id
 * @property string|null $file_name
 * @property string|null $created
 * @property int|null $active
 * @property int|null $nguoi_dung_id
 * @property int|null $user_id
 *
 * @property User $nguoiDung
 * @property User $user
 */
class AnhNguoiDung extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ad_anh_nguoi_dung';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file_name'], 'string'],
            [['created'], 'safe'],
            [['active', 'nguoi_dung_id', 'user_id'], 'integer'],
            [['nguoi_dung_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['nguoi_dung_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file_name' => 'File Name',
            'created' => 'Created',
            'active' => 'Active',
            'nguoi_dung_id' => 'Nguoi Dung ID',
            'user_id' => 'User ID',
        ];
    }

    /**
     * Gets query for [[NguoiDung]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNguoiDung()
    {
        return $this->hasOne(User::className(), ['id' => 'nguoi_dung_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
