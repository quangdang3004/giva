<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ad_quan_ly_don_hang".
 *
 * @property int $id
 * @property int|null $cong_ty_id
 * @property int|null $tinh_thanh_id
 * @property string|null $ten_khu_vuc
 * @property string|null $ten_tinh_thanh
 * @property int|null $nganh_nghe_id
 * @property float|null $bao_gia
 * @property string|null $gioi_tinh
 * @property string|null $trinh_do
 * @property int|null $tuoi_tu
 * @property int|null $tuoi_den
 * @property int|null $viem_gan_b
 * @property string|null $xam_tro
 * @property float|null $phi_don_hang
 * @property float|null $back_don_hang
 * @property float|null $tien_ho_tro
 * @property string|null $ngay_thi
 * @property string|null $ghi_chu
 * @property int|null $so_luong_can_tuyen
 * @property int|null $so_luong_da_tuyen
 * @property string|null $ghi_chu_cho_don
 * @property string|null $created
 * @property string|null $updated
 * @property int|null $so_ngay_mua
 * @property string|null $han_tuyen
 * @property int|null $parent_id
 * @property string|null $trang_thai
 * @property int|null $user_id
 * @property int|null $active
 * @property string|null $ten_nha_tuyen_dung
 * @property string|null $trang_thai_nha_tuyen_dung
 * @property string|null $dien_thoai_nha_tuyen_dung
 * @property string|null $email_nha_tuyen_dung
 * @property string|null $avatar_nha_tuyen_dung
 * @property int|null $doanh_nghiep_id
 * @property string|null $ten_doanh_nghiep
 * @property string|null $trang_thai_doanh_nghiep
 * @property string|null $dia_chi_doanh_nghiep
 * @property string|null $dien_thoai_doanh_nghiep
 * @property string|null $email_doanh_nghiep
 * @property string|null $ten_cong_ty
 * @property string|null $trang_thai_cong_ty
 * @property string|null $dien_thoai_cong_ty
 * @property string|null $email_cong_ty
 * @property string|null $dia_chi_cong_ty
 * @property int|null $nghe_nghiep_id
 * @property string|null $ten_nghe_nghiep
 * @property int|null $khu_vuc_id
 */
class QuanLyDonHang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ad_quan_ly_don_hang';
    }

    const DANG_TUYEN = 'Đang tuyển';
    const DA_DU = 'Đã đủ';
    const HET_HAN = 'Hết hạn';

    public function rules()
    {
        return [
            [['id', 'cong_ty_id', 'tinh_thanh_id', 'nganh_nghe_id', 'tuoi_tu', 'tuoi_den', 'viem_gan_b', 'so_luong_can_tuyen', 'so_luong_da_tuyen', 'so_ngay_mua', 'parent_id', 'user_id', 'active', 'doanh_nghiep_id', 'nghe_nghiep_id', 'khu_vuc_id'], 'integer'],
            [['ten_khu_vuc', 'ten_tinh_thanh', 'gioi_tinh', 'trinh_do', 'xam_tro', 'ghi_chu', 'ghi_chu_cho_don', 'trang_thai', 'trang_thai_nha_tuyen_dung', 'avatar_nha_tuyen_dung', 'ten_doanh_nghiep', 'trang_thai_doanh_nghiep', 'dia_chi_doanh_nghiep', 'ten_cong_ty', 'trang_thai_cong_ty', 'dia_chi_cong_ty', 'ten_nghe_nghiep'], 'string'],
            [['bao_gia', 'phi_don_hang', 'back_don_hang', 'tien_ho_tro'], 'number'],
            [['ngay_thi', 'created', 'updated', 'han_tuyen'], 'safe'],
            [['ten_nha_tuyen_dung'], 'string', 'max' => 100],
            [['dien_thoai_nha_tuyen_dung'], 'string', 'max' => 13],
            [['email_nha_tuyen_dung', 'email_doanh_nghiep', 'email_cong_ty'], 'string', 'max' => 50],
            [['dien_thoai_doanh_nghiep', 'dien_thoai_cong_ty'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bao_gia' => 'Báo giá',
            'so_luong_can_tuyen' => 'Số lượng cần tuyển',
            'gioi_tinh' => 'Giới tính',
            'trinh_do' => 'Trình độ',
            'ghi_chu' => 'Ghi chú',
            'so_luong_da_tuyen' => 'Số lượng đã tuyển',
            'created' => 'Created',
            'han_tuyen' => 'Han Tuyen',
            'trang_thai' => 'Trạng thái',
            'user_id' => 'User ID',
            'ten_nha_tuyen_dung' => 'Tên nhà tuyển dụng',
            'dien_thoai_nha_tuyen_dung' => 'Điện thoại nhà tuyển dụng',
            'email_nha_tuyen_dung' => 'Email nhà tuyển dụng',
            'avatar_nha_tuyen_dung' => 'Avatar nhà tuyển dụng',
            'ten_doanh_nghiep' => 'Tên doanh nghiệp',
            'dien_thoai_doanh_nghiep' => 'Điện thoại doanh nghiệp',
            'email_doanh_nghiep' => 'Email doanh nghiệp',
            'cong_ty_id' => 'ID công ty',
            'ten_cong_ty' => 'Tên công ty',
            'dien_thoai_cong_ty' => 'Điện thoại công ty',
            'email_cong_ty' => 'Email công ty',
            'dia_chi' => 'Địa chỉ',
            'logo_cong_ty' => 'Logo công ty',
            'nghe_nghiep_id' => 'Nghe Nghiep ID',
            'ten_nghe_nghiep' => 'Tên nghề nghiệp',
        ];
    }

    public static function getListTrangThaiDonHang(){
        return [
            self::DANG_TUYEN => self::DANG_TUYEN,
            self::DA_DU => self::DA_DU,
            self::HET_HAN => self::HET_HAN
        ];
    }

    public static function getListTrangThaiDonHangDisplay(){
        return [
            self::DANG_TUYEN => "<span class='text-warning'><strong>".self::DANG_TUYEN."</strong></span>",
            self::DA_DU =>  "<span class='text-success'><strong>".self::DA_DU."</strong></span>",
            self::HET_HAN =>  "<span class='text-muted'><strong>".self::HET_HAN."</strong></span>"
        ];
    }

}
