<?php

namespace backend\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "ad_lich_su_user".
 *
 * @property int $id
 * @property int|null $nguoi_dung_id
 * @property string|null $trang_thai
 * @property string|null $created
 * @property int|null $user_id
 * @property int|null $active
 *
 * @property User $nguoiDung
 * @property User $user
 */
class LichSuUser extends \yii\db\ActiveRecord
{
    const arr_trang_thai = [
      'Đã xác minh' =>  '<span class="text-success"><i class="fa fa-check-circle"></i> Đã xác minh</span>',
      'Chờ xác minh' =>  '<span class="text-warning"><i class="fa fa-spinner"><i/> Chờ xác minh</span>',
      'Chưa xác minh' =>  '<span class="text-danger"><i class="fa fa-warning"><i/> Chưa xác minh</span>',
    ];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ad_lich_su_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nguoi_dung_id', 'user_id', 'active'], 'integer'],
            [['trang_thai'], 'string'],
            [['created'], 'safe'],
            [['nguoi_dung_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['nguoi_dung_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nguoi_dung_id' => 'Nguoi Dung ID',
            'trang_thai' => 'Trang Thai',
            'created' => 'Created',
            'user_id' => 'User ID',
            'active' => 'Active',
        ];
    }

    /**
     * Gets query for [[NguoiDung]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNguoiDung()
    {
        return $this->hasOne(User::className(), ['id' => 'nguoi_dung_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
