<?php
//
//namespace backend\models;
//
//use Yii;
//
///**
// * This is the model class for table "{{%quan_ly_user}}".
// *
// * @property integer $id
// * @property string $username
// * @property string $password_hash
// * @property string $password_reset_token
// * @property integer $email
// * @property string $auth_key
// * @property int $status
// * @property string $created_at
// * @property string $updated_at
// * @property integer $nhom
// * @property string $hoten
// * @property string $dien_thoai
// * @property string $cmnd
// * @property string $dia_chi
// * @property int $active
// * @property int $user_id
// * @property string $ngay_sinh
// * @property string $nhu_cau_quan
// * @property string $nhu_cau_huong
// * @property float $nhu_cau_gia_tu
// * @property float $nhu_cau_gia_den
// * @property float $nhu_cau_dien_tich_tu
// * @property float $nhu_cau_dien_tich_den
// * @property string $ghi_chu
// * @property string $khoang_gia
// * @property float $he_so_luong
// * @property string $ngay_nghi
// * @property string $vai_tro
// * @property int $so_san_pham
// */
//class QuanLyUser extends \yii\db\ActiveRecord
//{
//    /**
//     * @inheritdoc
//     */
//    public static function tableName()
//    {
//        return '{{%quan_ly_user}}';
//    }
//
//    /**
//     * @inheritdoc
//     */
//    public function rules()
//    {
//        return [
//            [['id', 'active', 'status', 'user_id', 'so_san_pham'], 'integer'],
//            [['nhu_cau_gia_tu', 'nhu_cau_gia_den', 'nhu_cau_dien_tich_tu', 'nhu_cau_dien_tich_den', 'he_so_luong'], 'number'],
//            [['username', 'password_hash', 'password_reset_token', 'email', 'auth_key', 'password', 'nhom'], 'string'],
//            [['hoten', 'dien_thoai', 'cmnd', 'email', 'dia_chi', 'nhu_cau_quan', 'nhu_cau_huong', 'ghi_chu', 'khoang_gia'], 'string'],
//            [['vai_tro'], 'string'],
//            [['created_at', 'updated_at', 'ngay_sinh', 'ngay_nghi'], 'safe'],
//        ];
//    }
//
//    /**
//     * @inheritdoc
//     */
//    public function attributeLabels()
//    {
//        return [
//            'id' => 'ID',
//            'hoten' => 'Họ tên',
//            'username' => 'Tên đăng nhập',
//            'password_hash' => 'Mật khẩu',
//            'quyenadmin' => 'Quyenadmin',
//            'vaitro' => 'Vai trò',
//            'password_reset_token' => 'Password Reset Token',
//            'email' => 'Email',
//            'auth_key' => 'Auth Key',
//            'status' => 'Status',
//            'created_at' => 'Created At',
//            'updated_at' => 'Updated At',
//            'vai_tro' => 'Vai Tro',
//            'phong_ban' => 'Phong Ban',
//            'dienThoai' => 'Điện thoại',
//        ];
//    }
//}
