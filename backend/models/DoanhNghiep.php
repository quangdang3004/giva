<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ad_doanh_nghiep".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $nguoi_dai_dien
 * @property string|null $dien_thoai
 * @property string|null $ma_so_thue
 * @property string|null $dia_chi
 * @property string|null $email
 * @property string|null $trang_thai
 * @property int|null $status
 * @property string|null $created
 * @property string|null $updated
 * @property int|null $user_created
 * @property int|null $user_quan_li
 *
 * @property User $userCreated
 * @property User $userQuanLi
 * @property GiayToLienKet[] $giayToLienKets
 * @property User[] $users
 */
class DoanhNghiep extends \yii\db\ActiveRecord
{
    const CHUA_XAC_MINH = 'Chưa xác minh';
    const DANG_XAC_MINH = 'Đang xác minh';
    const DA_XAC_MINH = 'Đã xác minh';
    const TU_CHOI = 'Từ chối';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ad_doanh_nghiep';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'dia_chi'], 'required'],
            [['trang_thai'], 'string'],
            [['status', 'user_created'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['nguoi_dai_dien', 'ma_so_thue'], 'required'],
            [['dien_thoai'], 'required'],
            [['email', 'user_quan_li'], 'required'],
            [['user_created'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_created' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Tên doanh nghiệp',
            'nguoi_dai_dien' => 'Người đại diện',
            'dien_thoai' => 'Điện thoại',
            'ma_so_thue' => 'Mã số thuế',
            'dia_chi' => 'Địa chỉ',
            'email' => 'Email',
            'trang_thai' => 'Trạng thái',
            'status' => 'Status',
            'created' => 'Created',
            'updated' => 'Updated',
            'user_created' => 'User Created',
        ];
    }

    /**
     * Gets query for [[UserCreated]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUserCreated()
    {
        return $this->hasOne(User::className(), ['id' => 'user_created']);
    }
    /**
     * Gets query for [[UserQuanLi]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUserQuanLi()
    {
        return $this->hasOne(User::className(), ['id' => 'user_quan_li']);
    }

    /**
     * Gets query for [[GiayToLienKets]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGiayToLienKets()
    {
        return $this->hasMany(GiayToLienKet::className(), ['doanh_nghiep_id' => 'id']);
    }

    /**
     * Gets query for [[Users]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['doanh_nghiep_id' => 'id']);
    }

    public static function getListTrangThaiDisplay()
    {
        return [
            self::CHUA_XAC_MINH => '<span style="color: gray"><strong>'.self::CHUA_XAC_MINH.'</strong></span>',
            self::DANG_XAC_MINH => '<span style="color: orange"><strong>'.self::DANG_XAC_MINH.'</strong></span>',
            self::DA_XAC_MINH => '<span class="text-success"><strong>'.self::DA_XAC_MINH.'</strong></span>',
            self::TU_CHOI => '<span class="text-danger"><strong>'.self::TU_CHOI.'</strong></span>'
        ];
    }

    public static function getListUser()
    {
        return \common\models\User::find()->all();
    }

}
