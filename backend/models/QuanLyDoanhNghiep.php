<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ad_quan_ly_doanh_nghiep".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $nguoi_dai_dien
 * @property string|null $avatar
 * @property string|null $dien_thoai
 * @property string|null $link_diu_tup
 * @property string|null $ma_so_thue
 * @property string|null $dia_chi
 * @property string|null $email
 * @property string|null $trang_thai
 * @property int|null $status
 * @property string|null $created
 * @property string|null $updated
 * @property int|null $user_created
 * @property int|null $user_quan_li
 * @property string|null $nguoi_tao_doanh_nghiep
 * @property string|null $nguoi_quan_ly_doanh_nghiep
 * @property string|null $trang_thai_nguoi_quan_ly
 */
class QuanLyDoanhNghiep extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ad_quan_ly_doanh_nghiep';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'user_created', 'user_quan_li'], 'integer'],
            [['name', 'avatar', 'dia_chi', 'trang_thai', 'trang_thai_nguoi_quan_ly'], 'string'],
            [['created', 'updated'], 'safe'],
            [['nguoi_dai_dien','ma_so_thue'], 'string', 'max' => 32],
            [['dien_thoai'], 'string', 'max' => 10],
            [['link_diu_tup'], 'string', 'max' => 200],
            [['email'], 'email', 'max' => 50],
            [[ 'nguoi_tao_doanh_nghiep', 'nguoi_quan_ly_doanh_nghiep'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'nguoi_dai_dien' => 'Nguoi Dai Dien',
            'avatar' => 'Avatar',
            'dien_thoai' => 'Dien Thoai',
            'link_diu_tup' => 'Link Diu Tup',
            'ma_so_thue' => 'Ma So Thue',
            'dia_chi' => 'Dia Chi',
            'email' => 'Email',
            'trang_thai' => 'Trang Thai',
            'status' => 'Status',
            'created' => 'Created',
            'updated' => 'Updated',
            'user_created' => 'User Created',
            'user_quan_li' => 'User Quan Li',
            'nguoi_tao_doanh_nghiep' => 'Nguoi Tao Doanh Nghiep',
            'nguoi_quan_ly_doanh_nghiep' => 'Nguoi Quan Ly Doanh Nghiep',
            'trang_thai_nguoi_quan_ly' => 'Trang Thai Nguoi Quan Ly',
        ];
    }
}
