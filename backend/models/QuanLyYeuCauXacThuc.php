<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ad_quan_ly_yeu_cau_xac_thuc".
 *
 * @property int $id
 * @property string|null $type
 * @property int|null $user_id
 * @property int|null $cong_ty_id
 * @property int|null $doanh_nghiep_id
 * @property string|null $trang_thai
 * @property string|null $ghi_chu
 * @property string|null $created
 * @property string|null $updated
 * @property int|null $user_created_id
 * @property int|null $active
 * @property string|null $ten_doanh_nghiep
 * @property string|null $ten_user
 */
class QuanLyYeuCauXacThuc extends \yii\db\ActiveRecord
{
    const DA_XAC_MINH = 'Đã xác minh';
    const CHUA_XAC_MINH = 'Chưa xác minh';
    const DANG_XAC_MINH = 'Đang xác minh';
    const TU_CHOI = 'Từ chối';
    const CA_NHAN = 'Cá nhân';
    const DOANH_NGHIEP = 'Doanh nghiệp';
    public $tu_khoa;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ad_quan_ly_yeu_cau_xac_thuc';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'cong_ty_id', 'doanh_nghiep_id', 'user_created_id', 'active'], 'integer'],
            [['type', 'trang_thai', 'ten_doanh_nghiep'], 'string'],
            [['created', 'updated'], 'safe'],
            [['ghi_chu'], 'string', 'max' => 150],
            [['ten_user'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Đối tượng xác thực',
            'user_id' => 'User ID',
            'cong_ty_id' => 'Cong Ty ID',
            'doanh_nghiep_id' => 'Doanh Nghiep ID',
            'trang_thai' => 'Trang Thai',
            'ghi_chu' => 'Ghi chú',
            'created' => 'Ngày tạo',
            'updated' => 'Ngày cập nhật',
            'user_created_id' => 'User Created ID',
            'active' => 'Active',
            'ten_doanh_nghiep' => 'Tên doanh nghiệp',
            'ten_user' => 'Tên cá nhân',
        ];
    }
    public static function getListTrangThaiDisplay(){
        return [
            self::CHUA_XAC_MINH => '<span style="color: orange">Chưa xác minh</span>',
            self::DANG_XAC_MINH => '<span style="color: orange">Đang xác minh</span>',
            self::DA_XAC_MINH => '<span class="text-success">Đã xác minh</span>',
            self::TU_CHOI => '<span class="text-danger">Từ chối</span>'
        ];
    }
    public static function getListTrangThai(){
        return [
            self::DANG_XAC_MINH => self::DANG_XAC_MINH,
            self::DA_XAC_MINH => self::DA_XAC_MINH,
            self::TU_CHOI => self::TU_CHOI,
        ];
    }
}
