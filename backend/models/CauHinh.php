<?php

namespace backend\models;

use Yii;
use common\models\myActiveRecord;

/**
 * @property integer $id
 * @property string $content
 * @property string $name
 * @property string $ghi_chu
 */
class CauHinh extends myActiveRecord
{
    const HE_SO_LUONG = 'Hệ số lương';

    public static function tableName()
    {
        return '{{ad_cau_hinh}}';
    }

    public function rules()
    {
        return [
            [['ghi_chu', 'name'], 'safe'],
            [['content'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Nội dung',
            'ghi_chu' => 'Ký hiệu',
            'name' => 'Tên',
        ];
    }
}
