<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ad_vai_tro".
 *
 * @property int $id
 * @property string $name
 *
 * @property Vaitrouser[] $vaitrousers
 */
class VaiTro extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ad_vai_tro';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * Gets query for [[Vaitrousers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVaitrousers()
    {
        return $this->hasMany(Vaitrouser::className(), ['vaitro_id' => 'id']);
    }
}
