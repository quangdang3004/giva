<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ad_trang_thai_tien_do_giao_dich".
 *
 * @property int $id
 * @property int|null $giao_dich_id
 * @property string|null $trang_thai
 * @property string|null $created
 */
class TrangThaiTienDoGiaoDich extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ad_trang_thai_tien_do_giao_dich';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['giao_dich_id'], 'integer'],
            [['trang_thai'], 'string'],
            [['created'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'giao_dich_id' => 'Giao Dich ID',
            'trang_thai' => 'Trang Thai',
            'created' => 'Created',
        ];
    }
}
