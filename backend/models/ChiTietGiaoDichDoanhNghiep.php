<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ad_chi_tiet_giao_dich_doanh_nghiep".
 *
 * @property int $id
 * @property int|null $don_hang_id
 * @property int|null $chu_don_hang
 * @property string|null $ten_chu_don_hang
 * @property string|null $avatar_chu_don_hang
 * @property int|null $doanh_nghiep_id_chu_don_hang
 * @property string|null $trang_thai_chu_don_hang
 * @property string|null $email_chu_don_hang
 * @property string|null $dien_thoai_chu_don_hang
 * @property string|null $dia_chi_chu_don_hang
 * @property int|null $id_doanh_nghiep_chu_don_hang
 * @property string|null $ten_doanh_nghiep_chu_don_hang
 * @property string|null $trang_thai_doanh_nghiep_chu_don_hang
 * @property string|null $trang_thai_don_hang
 * @property string|null $ten_tinh_thanh
 * @property string|null $ten_nghe_nghiep
 * @property int|null $so_luong_can_tuyen
 * @property int|null $so_luong_da_tuyen
 * @property int|null $nguoi_cung_cap_id
 * @property string|null $type
 * @property float|null $so_luong
 * @property string|null $don_vi
 * @property string|null $trang_thai
 * @property string|null $tien_do
 * @property int|null $active
 * @property string|null $created
 * @property string|null $createdS
 * @property string|null $updated
 * @property string|null $ho_ten_nguoi_yeu_cau
 * @property string|null $avatar
 * @property string|null $email_nguoi_yeu_cau
 * @property string|null $dien_thoai_nguoi_yeu_cau
 * @property string|null $dia_chi_nguoi_yeu_cau
 * @property string|null $trang_thai_nguoi_yeu_cau
 * @property int|null $doanh_nghiep_id
 * @property string|null $nguoi_dai_dien_doanh_nghiep
 * @property string|null $ten_doanh_nghiep
 * @property string|null $ten_cong_ty
 * @property string|null $trang_thai_doanh_nghiep
 * @property string|null $dia_chi_doanh_nghiep
 * @property string|null $dien_thoai_doanh_nghiep
 * @property string|null $email_doanh_nghiep
 * @property int|null $rate
 * @property string|null $last_mess
 * @property string|null $created_mess
 */
class ChiTietGiaoDichDoanhNghiep extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'ad_chi_tiet_giao_dich_doanh_nghiep';
    }

    const CUNG_CAP_UNG_VIEN = 'Cung cấp ứng viên';
    const TRU_TIEN =  'Trừ tiền';
    const CONG_TIEN =  'Cộng tiền';
    const CHUYEN_TIEN = 'Chuyển tiền';

    const XU = 'Xu';
    const UNG_VIEN = 'Ứng viên';



    public function rules()
    {
        return [
            [['id', 'don_hang_id', 'chu_don_hang', 'doanh_nghiep_id_chu_don_hang', 'id_doanh_nghiep_chu_don_hang', 'so_luong_can_tuyen', 'so_luong_da_tuyen', 'nguoi_cung_cap_id', 'active', 'doanh_nghiep_id', 'rate'], 'integer'],
            [['avatar_chu_don_hang', 'trang_thai_chu_don_hang', 'ten_doanh_nghiep_chu_don_hang', 'trang_thai_doanh_nghiep_chu_don_hang', 'trang_thai_don_hang', 'ten_tinh_thanh', 'ten_nghe_nghiep', 'type', 'don_vi', 'trang_thai', 'tien_do', 'avatar', 'trang_thai_nguoi_yeu_cau', 'ten_doanh_nghiep', 'trang_thai_doanh_nghiep', 'dia_chi_doanh_nghiep', 'last_mess'], 'string'],
            [['so_luong'], 'number'],
            [['createdS', 'created_mess','ten_cong_ty'], 'safe'],
            [['ten_chu_don_hang', 'ho_ten_nguoi_yeu_cau'], 'string', 'max' => 100],
            [['email_chu_don_hang', 'email_nguoi_yeu_cau', 'email_doanh_nghiep'], 'string', 'max' => 50],
            [['dien_thoai_chu_don_hang', 'dien_thoai_nguoi_yeu_cau'], 'string', 'max' => 13],
            [['dia_chi_chu_don_hang', 'dia_chi_nguoi_yeu_cau'], 'string', 'max' => 150],
            [['created', 'updated'], 'string', 'max' => 21],
            [['nguoi_dai_dien_doanh_nghiep'], 'string', 'max' => 32],
            [['dien_thoai_doanh_nghiep'], 'string', 'max' => 10],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'don_hang_id' => 'Don Hang ID',
            'chu_don_hang' => 'Chu Don Hang',
            'ten_chu_don_hang' => 'Ten Chu Don Hang',
            'avatar_chu_don_hang' => 'Avatar Chu Don Hang',
            'doanh_nghiep_id_chu_don_hang' => 'Doanh Nghiep Id Chu Don Hang',
            'trang_thai_chu_don_hang' => 'Trang Thai Chu Don Hang',
            'email_chu_don_hang' => 'Email Chu Don Hang',
            'dien_thoai_chu_don_hang' => 'Dien Thoai Chu Don Hang',
            'dia_chi_chu_don_hang' => 'Dia Chi Chu Don Hang',
            'id_doanh_nghiep_chu_don_hang' => 'Id Doanh Nghiep Chu Don Hang',
            'ten_doanh_nghiep_chu_don_hang' => 'Ten Doanh Nghiep Chu Don Hang',
            'trang_thai_doanh_nghiep_chu_don_hang' => 'Trang Thai Doanh Nghiep Chu Don Hang',
            'trang_thai_don_hang' => 'Trang Thai Don Hang',
            'ten_tinh_thanh' => 'Ten Tinh Thanh',
            'ten_nghe_nghiep' => 'Ten Nghe Nghiep',
            'so_luong_can_tuyen' => 'So Luong Can Tuyen',
            'so_luong_da_tuyen' => 'So Luong Da Tuyen',
            'nguoi_cung_cap_id' => 'Nguoi Cung Cap ID',
            'type' => 'Type',
            'so_luong' => 'So Luong',
            'don_vi' => 'Don Vi',
            'trang_thai' => 'Trang Thai',
            'tien_do' => 'Tien Do',
            'active' => 'Active',
            'created' => 'Created',
            'createdS' => 'Created S',
            'updated' => 'Updated',
            'ho_ten_nguoi_yeu_cau' => 'Ho Ten Nguoi Yeu Cau',
            'avatar' => 'Avatar',
            'email_nguoi_yeu_cau' => 'Email Nguoi Yeu Cau',
            'dien_thoai_nguoi_yeu_cau' => 'Dien Thoai Nguoi Yeu Cau',
            'dia_chi_nguoi_yeu_cau' => 'Dia Chi Nguoi Yeu Cau',
            'trang_thai_nguoi_yeu_cau' => 'Trang Thai Nguoi Yeu Cau',
            'doanh_nghiep_id' => 'Doanh Nghiep ID',
            'nguoi_dai_dien_doanh_nghiep' => 'Nguoi Dai Dien Doanh Nghiep',
            'ten_doanh_nghiep' => 'Ten Doanh Nghiep',
            'trang_thai_doanh_nghiep' => 'Trang Thai Doanh Nghiep',
            'dia_chi_doanh_nghiep' => 'Dia Chi Doanh Nghiep',
            'dien_thoai_doanh_nghiep' => 'Dien Thoai Doanh Nghiep',
            'email_doanh_nghiep' => 'Email Doanh Nghiep',
            'rate' => 'Rate',
            'last_mess' => 'Last Mess',
            'created_mess' => 'Created Mess',
        ];
    }

    public static function getListLoaiGiaoDich(){
        return[
            self::CUNG_CAP_UNG_VIEN => self::CUNG_CAP_UNG_VIEN,
            self::CONG_TIEN => self::CONG_TIEN,
            self::TRU_TIEN => self::TRU_TIEN,
            self::CHUYEN_TIEN => self::CHUYEN_TIEN,
        ];
    }

    public static function getListDonViGiaoDich(){
        return[
            self::XU => self::XU,
            self::UNG_VIEN => self::UNG_VIEN,
        ];
    }
}
