<?php

namespace backend\models\search;

use backend\models\User;
use backend\models\UserVaiTro;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\DanhMuc;

/**
 * DanhMucSearch represents the model behind the search form about `backend\models\DanhMuc`.
 */
class UserVaiTroSearch extends UserVaiTro
{

    public function rules()
    {
        return [
            [['id', 'status', 'VIP', 'hoat_dong', 'customer', 'branch_id', 'vai_tro_id'], 'safe'],
            [['created_at', 'updated_at', 'birth_day',  'ten_vai_tro'], 'safe'],
            [['vi_dien_tu', 'credits'], 'safe'],
            [['loai_hop_dong', 'trang_thai'], 'safe'],
            [['username', 'password_hash', 'email', 'password', 'hoten', 'anhdaidien', 'full_name', 'qr_codes', 'ten_vai_tro'], 'safe'],
            [['password_reset_token'], 'safe'],
            [['auth_key'], 'safe'],
            [['dien_thoai', 'cmnd'], 'safe'],
            [['dia_chi'], 'safe'],
            [['ma_chuc_danh'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserVaiTro::find()->andWhere(['status' => \common\models\User::STATUS_ACTIVE]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        if (array_key_exists('doi_tuong',$params)){
            $query->andFilterWhere(['like', 'username', $this->username])
                ->andFilterWhere(['like', 'ten_vai_tro', $this->ten_vai_tro])
                ->andFilterWhere(['like', 'dien_thoai', $this->dien_thoai])
                ->andFilterWhere(['type' => 'Nhà tuyền dụng'])
                ->andFilterWhere(['like', 'hoten', $this->hoten]);
        } else {
            $query->andFilterWhere(['like', 'username', $this->username])
                ->andFilterWhere(['like', 'ten_vai_tro', $this->ten_vai_tro])
                ->andFilterWhere(['like', 'dien_thoai', $this->dien_thoai])
                ->andFilterWhere(['like','email', $this->email])
                ->andFilterWhere(['like', 'hoten', $this->hoten])
                ->andFilterWhere(['like', 'vai_tro_id', 1]);
        }

        $query->andFilterWhere([
            'trang_thai' => $this->trang_thai,
            'email' => $this->email
        ]);


        return $dataProvider;
    }
}
