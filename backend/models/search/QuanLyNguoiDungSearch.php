<?php
//
//namespace backend\models\search;
//
//use backend\models\QuanLyNguoiDung;
//use backend\models\QuanLyUser;
//use backend\models\Vaitrouser;
//use common\models\myAPI;
//use common\models\User;
//use Yii;
//use yii\base\Model;
//use yii\data\ActiveDataProvider;
//use backend\models\Cauhinh;
//
///**
// * CauhinhSearch represents the model behind the search form about `backend\models\Cauhinh`.
// */
//class QuanLyNguoiDungSearch extends QuanLyNguoiDung
//{
//    /**
//     * @inheritdoc
//     */
//    public function rules()
//    {
//        return [
//            [['id', 'status', 'created_at', 'updated_at', 'active', 'user_id'], 'safe'],
//            [['nhom', 'vai_tro'], 'safe'],
//            [['ngay_sinh'], 'safe'],
//            [['username', 'password_hash', 'email', 'password', 'hoten'], 'safe'],
//            [['password_reset_token'], 'safe'],
//            [['auth_key'], 'safe'],
//            [['dien_thoai', 'cmnd'], 'safe'],
//            [['dia_chi'], 'safe'],
//        ];
//    }
//
//    /**
//     * @inheritdoc
//     */
//    public function scenarios()
//    {
//        // bypass scenarios() implementation in the parent class
//        return Model::scenarios();
//    }
//
//    /**
//     * Creates data provider instance with search query applied
//     *
//     * @param array $params
//     *
//     * @return ActiveDataProvider
//     */
//    public function search($params, $idSearch = null)
//    {
//        $query = QuanLyNguoiDung::find();
//
//        $dataProvider = new ActiveDataProvider([
//            'query' => $query,
//        ]);
//
//        $this->load($params);
//
//        if (!$this->validate()) {
//            // uncomment the following line if you do not want to return any records when validation fails
//            // $query->where('0=1');
//            return $dataProvider;
//        }
//
//        $query->andFilterWhere([
//            'id' => $this->id,
//        ]);
//
//        $query->andFilterWhere(['like', 'hoten', $this->hoten])
//            ->andFilterWhere(['like', 'dien_thoai', $this->dien_thoai])
//            ->andFilterWhere(['like', 'dia_chi', $this->dia_chi])
//            ->andFilterWhere(['like', 'username', $this->username])
//            ->andFilterWhere(['like', 'email', $this->email]);
//
//        return $dataProvider;
//    }
//
//}
