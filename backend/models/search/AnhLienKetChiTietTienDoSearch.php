<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\AnhLienKetChiTietTienDo;

/**
 * AnhLienKetChiTietTienDoSearch represents the model behind the search form about `backend\models\AnhLienKetChiTietTienDo`.
 */
class AnhLienKetChiTietTienDoSearch extends AnhLienKetChiTietTienDo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tien_do_id'], 'integer'],
            [['link_anh', 'created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AnhLienKetChiTietTienDo::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'tien_do_id' => $this->tien_do_id,
            'created' => $this->created,
        ]);

        $query->andFilterWhere(['like', 'link_anh', $this->link_anh]);

        return $dataProvider;
    }
}
