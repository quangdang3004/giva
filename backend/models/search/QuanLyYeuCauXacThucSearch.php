<?php

namespace backend\models\search;

use backend\models\QuanLyYeuCauXacThuc;
use backend\models\YeuCauXacThuc;
use common\models\myAPI;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\CauHinh;
use yii\helpers\VarDumper;

/**
 * CauhinhSearch represents the model behind the search form about `backend\models\Cauhinh`.
 */
class QuanLyYeuCauXacThucSearch extends QuanLyYeuCauXacThuc
{
    const CHUA_XAC_MINH = 'Chưa xác minh';
    const DANG_XAC_MINH = 'Đang xác minh';
    const DA_XAC_MINH = 'Đã xác minh';
    const TU_CHOI = 'Từ chối';
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'cong_ty_id', 'doanh_nghiep_id', 'user_created_id', 'active'], 'safe'],
            [['type', 'trang_thai', 'ten_doanh_nghiep'], 'safe'],
            [['created', 'updated'], 'safe'],
            [['ghi_chu', 'tu_khoa'], 'safe'],
            [['ten_user'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $doituong)
    {
        $query = QuanLyYeuCauXacThuc::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->andFilterWhere(['<>','trang_thai', YeuCauXacThuc::CHUA_XAC_MINH ]);

        if($doituong == 'ca_nhan'){
            $query->andFilterWhere([
                'id' => $this->id,
                'user_id' => $this->user_id,
                'cong_ty_id' => $this->cong_ty_id,
                'doanh_nghiep_id' => $this->doanh_nghiep_id,
                'user_created_id' => $this->user_created_id,
                'active' => $this->active,
                'type' => 'Cá nhân'
            ]);
        }elseif ($doituong == 'doanh_nghiep'){
            $query->andFilterWhere([
                'id' => $this->id,
                'user_id' => $this->user_id,
                'cong_ty_id' => $this->cong_ty_id,
                'doanh_nghiep_id' => $this->doanh_nghiep_id,
                'user_created_id' => $this->user_created_id,
                'active' => $this->active,
                'type' => 'Doanh nghiệp'
            ])
                ->andFilterWhere(['<>','trang_thai', YeuCauXacThuc::TU_CHOI ]);

        }
          $query->andFilterWhere(//
              ['date(created)'=> myAPI::convertDateSaveIntoDb($this->created)]);

//            VarDumper::dump(date($this->created), 10, true); exit();
        $query->andFilterWhere(['or',
            ['like', 'ten_user', $this->ten_user],
            ['like', 'ten_doanh_nghiep', $this->ten_doanh_nghiep]
        ])
            ->andFilterWhere(['active' => $this->active])
            ->andFilterWhere(['trang_thai' => $this->trang_thai])
            ->andFilterWhere(['type' => $this->type])
            ->andFilterWhere(['like', 'ghi_chu', $this->ghi_chu]);

        $query->orderBy(['created' => SORT_DESC]);

        return $dataProvider;
    }



}
