<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\YeuCauXacThuc;
use yii\helpers\VarDumper;

/**
 * YeuCauXacThucSearch represents the model behind the search form about `backend\models\YeuCauXacThuc`.
 */
class YeuCauXacThucSearch extends YeuCauXacThuc
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'cong_ty_id', 'doanh_nghiep_id', 'user_created_id', 'active'], 'integer'],
            [['type', 'trang_thai', 'created', 'updated','tu_khoa'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $doituong)
    {
        $query = YeuCauXacThuc::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        if($doituong == 'ca_nhan'){
            $query->andFilterWhere([
                'type' => 'Cá nhân'
            ]);
        }elseif ($doituong == 'doanh_nghiep'){
        $query->andFilterWhere([
            'type' => 'Doanh nghiệp'
        ]);

        }



        $query->andFilterWhere(['or',
            ['like','hoten',$this->tu_khoa],
            ['like','name',$this->tu_khoa],
            ]);
        $query->andFilterWhere(['like', 'type', $this->type])
                    ->andFilterWhere(['like', 'ghi_chu', $this->ghi_chu]);

        return $dataProvider;
    }
}
