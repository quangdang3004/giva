<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\GiayToLienKet;

/**
 * GiayToLienKetSearch represents the model behind the search form about `backend\models\GiayToLienKet`.
 */
class GiayToLienKetSearch extends GiayToLienKet
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nha_tuyen_dung_id', 'doanh_nghiep_id', 'cong_ti_id', 'active'], 'integer'],
            [['link', 'type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GiayToLienKet::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'nha_tuyen_dung_id' => $this->nha_tuyen_dung_id,
            'doanh_nghiep_id' => $this->doanh_nghiep_id,
            'cong_ti_id' => $this->cong_ti_id,
            'active' => $this->active,
        ]);

        $query->andFilterWhere(['like', 'link', $this->link])
            ->andFilterWhere(['like', 'type', $this->type]);

        return $dataProvider;
    }
}
