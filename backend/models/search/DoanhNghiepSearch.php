<?php

namespace backend\models\search;

use common\models\myAPI;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\DoanhNghiep;
use yii\helpers\VarDumper;

/**
 * DoanhNghiepSearch represents the model behind the search form about `backend\models\DoanhNghiep`.
 */
class DoanhNghiepSearch extends DoanhNghiep
{
    const CHUA_XAC_MINH = 'Chưa xác minh';
    const DANG_XAC_MINH = 'Đang xác minh';
    const DA_XAC_MINH = 'Đã xác minh';
    const TU_CHOI = 'Từ chối';
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'user_created'], 'integer'],
            [['name', 'nguoi_dai_dien', 'dien_thoai', 'ma_so_thue', 'dia_chi', 'email', 'trang_thai', 'created', 'updated'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DoanhNghiep::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created' => $this->created,
            'updated' => $this->updated,
            'user_created' => $this->user_created,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'nguoi_dai_dien', $this->nguoi_dai_dien])
            ->andFilterWhere(['like', 'dien_thoai', $this->dien_thoai])
            ->andFilterWhere(['like', 'ma_so_thue', $this->ma_so_thue])
            ->andFilterWhere(['like', 'dia_chi', $this->dia_chi])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['status' => 10])
            ->andFilterWhere(['like', 'trang_thai', $this->trang_thai]);

        return $dataProvider;
    }
    public function searchDoanhNghiep($params, $type)
    {
        $query = DoanhNghiepSearch::find()->select(['id','dien_thoai', 'ma_so_thue', 'trang_thai',
            'status', 'name', 'nguoi_dai_dien', 'dia_chi', 'created'])
            ->andFilterWhere(['status' => 10])
            ->orderBy(['created' => SORT_DESC]);
//        VarDumper::dump($query, 10, true); exit();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        if($type == 'chua_xac_minh'){
            $trang_thai = 'Chưa xác minh';
        }
        else if($type == 'cho_xac_minh'){
            $trang_thai = 'Đang xác minh';
        }
        else if($type == 'da_xac_minh'){
            $trang_thai = 'Đã xác minh';
        }
        else{
            $trang_thai = 'Từ chối';
        }

        $query->andFilterWhere(//
            ['date(created)'=> myAPI::convertDateSaveIntoDb($this->created)]);

        $query
            ->andFilterWhere(['dien_thoai' => $this->dien_thoai])
            ->andFilterWhere(['ma_so_thue' => $this->ma_so_thue])
            ->andFilterWhere(['trang_thai' => $trang_thai, 'status' => 10])
            ->andFilterWhere(['status' => 10])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'nguoi_dai_dien', $this->nguoi_dai_dien])
            ->andFilterWhere(['like', 'dia_chi', $this->dia_chi]);

        return $dataProvider;
    }

    public static function getListTrangThai(){
        return [
            self::CHUA_XAC_MINH => self::CHUA_XAC_MINH,
            self::DANG_XAC_MINH => self::DANG_XAC_MINH,
            self::DA_XAC_MINH => self::DA_XAC_MINH,
            self::TU_CHOI => self::TU_CHOI,
        ];
    }
}
