<?php

namespace backend\models\search;

use backend\models\QuanLyDonHang;
use common\models\myAPI;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\CauHinh;
use yii\helpers\VarDumper;

/**
 * CauhinhSearch represents the model behind the search form about `backend\models\Cauhinh`.
 */
class QuanLyDonHangSearch extends QuanLyDonHang
{
    public $phi_don_hang_den;
    public function rules()
    {
        return [
            [['id', 'cong_ty_id', 'tinh_thanh_id', 'nganh_nghe_id', 'tuoi_tu', 'tuoi_den', 'viem_gan_b', 'so_luong_can_tuyen',
                'so_luong_da_tuyen', 'so_ngay_mua', 'parent_id', 'user_id', 'active', 'doanh_nghiep_id', 'nghe_nghiep_id',
                'khu_vuc_id'], 'safe'],
            [['ten_khu_vuc', 'ten_tinh_thanh', 'gioi_tinh', 'trinh_do', 'xam_tro', 'ghi_chu', 'ghi_chu_cho_don', 'trang_thai',
                'trang_thai_nha_tuyen_dung', 'avatar_nha_tuyen_dung', 'ten_doanh_nghiep', 'trang_thai_doanh_nghiep',
                'dia_chi_doanh_nghiep', 'ten_cong_ty', 'trang_thai_cong_ty', 'dia_chi_cong_ty', 'ten_nghe_nghiep'], 'safe'],
            [['bao_gia', 'phi_don_hang', 'back_don_hang', 'tien_ho_tro'], 'safe'],
            [['ngay_thi', 'created', 'updated', 'han_tuyen'], 'safe'],
            [['ten_nha_tuyen_dung', 'phi_don_hang_den'], 'safe'],
            [['dien_thoai_nha_tuyen_dung'], 'safe'],
            [['email_nha_tuyen_dung', 'email_doanh_nghiep', 'email_cong_ty'], 'safe'],
            [['dien_thoai_doanh_nghiep', 'dien_thoai_cong_ty'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = QuanLyDonHang::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        /**
         * ten_cong_ty
        trang_thai_cong_ty
        dien_thoai_cong_ty
        email_cong_ty
        dia_chi_cong_ty
        logo_cong_ty
         */

        /**
         * ten_doanh_nghiep
        trang_thai_doanh_nghiep
        dia_chi_doanh_nghiep
        dien_thoai_doanh_nghiep
        email_doanh_nghiep
         */

        $query->andFilterWhere([
            'id' => $this->id,
            'active' => 1,
            'so_luong_can_tuyen' => $this->so_luong_can_tuyen,
            'so_luong_da_tuyen' => $this->so_luong_da_tuyen,
            'trang_thai' => $this->trang_thai,
            'tuoi_tu' => $this->tuoi_tu,
            'tuoi_den' => $this->tuoi_den,
            'trang_thai_nha_tuyen_dung' => $this->trang_thai_nha_tuyen_dung,
            'dien_thoai_nha_tuyen_dung' => $this->dien_thoai_nha_tuyen_dung,
            'trang_thai_doanh_nghiep' => $this->trang_thai_doanh_nghiep,
            'dien_thoai_doanh_nghiep' => $this->dien_thoai_doanh_nghiep,
            'trang_thai_cong_ty' => $this->trang_thai_cong_ty,
            'dien_thoai_cong_ty' => $this->dien_thoai_cong_ty,
            'viem_gan_b' => $this->viem_gan_b,
            'xam_tro' => $this->xam_tro,
        ]);

//        VarDumper::dump($this->han_tuyen, 10, true);exit();
//        VarDumper::dump(myAPI::convertDateSaveIntoDb($this->han_tuyen), 10, true);exit();
        if($this->han_tuyen){
            $query->andFilterWhere(['like', 'han_tuyen', myAPI::convertDateSaveIntoDb($this->han_tuyen)]);
        }
        if($this->created){
            $query->andFilterWhere(['like', 'created', myAPI::convertDateSaveIntoDb($this->created)]);
        }
        if($this->updated){
            $query->andFilterWhere(['like', 'updated', myAPI::convertDateSaveIntoDb($this->updated)]);
        }
//        VarDumper::dump($this->updated, 10, true);exit();
        if($this->ngay_thi){
            $query->andFilterWhere(['like', 'ngay_thi', myAPI::convertDateSaveIntoDb($this->ngay_thi)]);
        }

        if($this->phi_don_hang){
            $query->andFilterWhere(['>=', 'phi_don_hang', $this->phi_don_hang]);
        }

        if($this->phi_don_hang_den){
            $query->andFilterWhere(['<=', 'phi_don_hang', $this->phi_don_hang_den]);
        }

        $query->andFilterWhere(['like', 'ten_nha_tuyen_dung', $this->ten_nha_tuyen_dung])
            ->andFilterWhere(['like', 'ghi_chu', $this->ghi_chu])
            ->andFilterWhere(['like', 'ten_nghe_nghiep', $this->ten_nghe_nghiep])
            ->andFilterWhere(['like', 'trinh_do', $this->trinh_do])
            ->andFilterWhere(['like', 'ten_khu_vuc', $this->ten_khu_vuc])
            ->andFilterWhere(['like', 'ten_tinh_thanh', $this->ten_tinh_thanh])
            ->andFilterWhere(['like', 'ten_nha_tuyen_dung', $this->ten_nha_tuyen_dung])
            ->andFilterWhere(['like', 'email_nha_tuyen_dung', $this->email_nha_tuyen_dung])
            ->andFilterWhere(['like', 'ghi_chu_cho_don', $this->ghi_chu_cho_don])
            ->andFilterWhere(['like', 'ten_doanh_nghiep', $this->ten_doanh_nghiep])
            ->andFilterWhere(['like', 'dia_chi_doanh_nghiep', $this->dia_chi_doanh_nghiep])
            ->andFilterWhere(['like', 'email_doanh_nghiep', $this->email_doanh_nghiep])
            ->andFilterWhere(['like', 'ten_cong_ty', $this->ten_cong_ty])
            ->andFilterWhere(['like', 'dia_chi_cong_ty', $this->dia_chi_cong_ty])
            ->andFilterWhere(['like', 'email_cong_ty', $this->email_cong_ty]);


        return $dataProvider;
    }
}
