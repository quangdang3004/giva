<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\DanhGia;

/**
 * DanhGiaSearch represents the model behind the search form about `backend\models\DanhGia`.
 */
class DanhGiaSearch extends DanhGia
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'active', 'rate', 'doanh_nghiep_id', 'user_created_id'], 'integer'],
            [['noi_dung', 'updated', 'type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DanhGia::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'active' => $this->active,
            'rate' => $this->rate,
            'created' => $this->created,
            'updated' => $this->updated,
            'doanh_nghiep_id' => $this->doanh_nghiep_id,
            'user_created_id' => $this->user_created_id,
        ]);

        $query->andFilterWhere(['like', 'noi_dung', $this->noi_dung])
            ->andFilterWhere(['like', 'type', $this->type]);

        return $dataProvider;
    }
}
