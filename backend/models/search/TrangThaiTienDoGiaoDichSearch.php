<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TrangThaiTienDoGiaoDich;

/**
 * TrangThaiTienDoGiaoDichSearch represents the model behind the search form about `backend\models\TrangThaiTienDoGiaoDich`.
 */
class TrangThaiTienDoGiaoDichSearch extends TrangThaiTienDoGiaoDich
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'giao_dich_id'], 'integer'],
            [['trang_thai', 'created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TrangThaiTienDoGiaoDich::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'giao_dich_id' => $this->giao_dich_id,
            'created' => $this->created,
        ]);

        $query->andFilterWhere(['like', 'trang_thai', $this->trang_thai]);

        return $dataProvider;
    }
}
