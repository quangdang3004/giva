<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\AnhLienKetDonHang;

/**
 * AnhLienKetDonHangSearch represents the model behind the search form about `backend\models\AnhLienKetDonHang`.
 */
class AnhLienKetDonHangSearch extends AnhLienKetDonHang
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'don_hang_id', 'active'], 'integer'],
            [['link', 'created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AnhLienKetDonHang::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'don_hang_id' => $this->don_hang_id,
            'created' => $this->created,
            'active' => $this->active,
        ]);

        $query->andFilterWhere(['like', 'link', $this->link]);

        return $dataProvider;
    }
}
