<?php
//
//namespace backend\models\search;
//
//use backend\models\QuanLyUser;
//use common\models\User;
//use Yii;
//use yii\base\Model;
//use yii\data\ActiveDataProvider;
//use backend\models\DonVi;
//
///**
// * DonViSearch represents the model behind the search form about `backend\models\DonVi`.
// */
//class QuanLyUserSearch extends QuanLyUser
//{
//    /**
//     * @inheritdoc
//     */
//    public function rules()
//    {
//        return [
//            [['id', 'active', 'status', 'user_id', 'so_san_pham'], 'safe'],
//            [['nhu_cau_gia_tu', 'nhu_cau_gia_den', 'nhu_cau_dien_tich_tu', 'nhu_cau_dien_tich_den', 'he_so_luong'], 'safe'],
//            [['username', 'password_hash', 'password_reset_token', 'email', 'auth_key', 'password', 'nhom'], 'safe'],
//            [['hoten', 'dien_thoai', 'cmnd', 'email', 'dia_chi', 'nhu_cau_quan', 'nhu_cau_huong', 'ghi_chu', 'khoang_gia'], 'safe'],
//            [['vai_tro'], 'safe'],
//            [['created_at', 'updated_at', 'ngay_sinh', 'ngay_nghi'], 'safe'],
//        ];
//    }
//
//    /**
//     * @inheritdoc
//     */
//    public function scenarios()
//    {
//        // bypass scenarios() implementation in the parent class
//        return Model::scenarios();
//    }
//
//    /**
//     * Creates data provider instance with search query applied
//     *
//     * @param array $params
//     *
//     * @return ActiveDataProvider
//     */
//    public function search($params, $idSearch = null)
//    {
//        $query = QuanLyUser::find();
//
//        $dataProvider = new ActiveDataProvider([
//            'query' => $query,
//        ]);
//
//        $this->load($params);
//        if(!is_null($idSearch))
//        {
//            if(!isset($_GET['id'])){
//                $query->andFilterWhere(['id' => -1]);
////                phong_ban
//            }
//        }
//        if (!$this->validate()) {
//            // uncomment the following line if you do not want to return any records when validation fails
//            // $query->where('0=1');
//            return $dataProvider;
//        }
//        $query->andFilterWhere(['nhom' => User::THANH_VIEN]);
//        $query->andFilterWhere(['like', 'hoten', $this->hoten])
//            ->andFilterWhere(['like', 'dienThoai', $this->dien_thoai])
//            ->andFilterWhere(['like', 'username', $this->username])
//            ->andFilterWhere(['like', 'email', $this->email]);
//
//        return $dataProvider;
//    }
//
//    public function searchKhachHangCuaToi($params, $idSearch = null)
//    {
//        $query = QuanLyUser::find();
//
//        $dataProvider = new ActiveDataProvider([
//            'query' => $query,
//        ]);
//
//        $this->load($params);
//
//        if (!$this->validate()) {
//            return $dataProvider;
//        }
//
//        $query->andFilterWhere([
//            'user_id' => Yii::$app->user->id,
//            'active'=>1
//        ]);
//
//        $query->andFilterWhere(['like', 'hoten', $this->hoten])
//            ->andFilterWhere(['like', 'dien_thoai', $this->dien_thoai])
//            ->andFilterWhere(['like', 'dia_chi', $this->dia_chi])
//            ->andFilterWhere(['like', 'username', $this->username])
//            ->andFilterWhere(['like', 'email', $this->email])
//            ->andFilterWhere(['like', 'ghi_chu', $this->ghi_chu]);
//
//        return $dataProvider;
//    }
//
//    public function searchKhachHangChoChapNhanChiaSe($params, $idSearch = null)
//    {
//        $query = QuanLyUser::find();
//
//        $dataProvider = new ActiveDataProvider([
//            'query' => $query,
//        ]);
//
//        $this->load($params);
//
//        if (!$this->validate()) {
//            return $dataProvider;
//        }
//
//        $query->andFilterWhere([
//            'user_id' => Yii::$app->user->id,
//            'active'=>1
//        ]);
//
//        $query->andFilterWhere(['like', 'hoten', $this->hoten])
//            ->andFilterWhere(['like', 'dien_thoai', $this->dien_thoai])
//            ->andFilterWhere(['like', 'dia_chi', $this->dia_chi])
//            ->andFilterWhere(['like', 'username', $this->username])
//            ->andFilterWhere(['like', 'email', $this->email])
//            ->andFilterWhere(['like', 'ghi_chu', $this->ghi_chu]);
//
//        return $dataProvider;
//    }
//}
