<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\DonHang;

/**
 * DonHangSearch represents the model behind the search form about `backend\models\DonHang`.
 */
class DonHangSearch extends DonHang
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'cong_ty_id', 'nganh_nghe_id', 'so_luong_can_tuyen', 'so_luong_da_tuyen', 'parent_id', 'user_id', 'active'], 'integer'],
            [['bao_gia'], 'number'],
            [['gioi_tinh', 'trinh_do', 'ghi_chu', 'created', 'updated', 'han_tuyen', 'trang_thai'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DonHang::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'cong_ty_id' => $this->cong_ty_id,
            'nganh_nghe_id' => $this->nganh_nghe_id,
            'bao_gia' => $this->bao_gia,
            'so_luong_can_tuyen' => $this->so_luong_can_tuyen,
            'so_luong_da_tuyen' => $this->so_luong_da_tuyen,
            'created' => $this->created,
            'updated' => $this->updated,
            'han_tuyen' => $this->han_tuyen,
            'parent_id' => $this->parent_id,
            'user_id' => $this->user_id,
            'active' => $this->active,
        ]);

        $query->andFilterWhere(['like', 'gioi_tinh', $this->gioi_tinh])
            ->andFilterWhere(['like', 'trinh_do', $this->trinh_do])
            ->andFilterWhere(['like', 'ghi_chu', $this->ghi_chu])
            ->andFilterWhere(['like', 'trang_thai', $this->trang_thai]);

        return $dataProvider;
    }
}
