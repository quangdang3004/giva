<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\User;

/**
 * UserSearch represents the model behind the search form about `backend\models\User`.
 */
class UserSearch extends User
{
    const CHUA_XAC_MINH = 'Chưa xác minh';
    const DANG_XAC_MINH = 'Đang xác minh';
    const DA_XAC_MINH = 'Đã xác minh';
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'exp_auth_key', 'status', 'doanh_nghiep_id'], 'integer'],
            [['username', 'password_hash', 'exp_token_reset', 'hoten', 'email', 'dien_thoai', 'auth_key', 'avatar', 'created_at', 'updated_at', 'type', 'trang_thai', 'password_reset_token', 'password'], 'safe'],
            [['so_du'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'exp_auth_key' => $this->exp_auth_key,
            'so_du' => $this->so_du,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'status' => $this->status,
            'doanh_nghiep_id' => $this->doanh_nghiep_id,
            'trang_thai' => $this->trang_thai,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'exp_token_reset', $this->exp_token_reset])
            ->andFilterWhere(['like', 'hoten', $this->hoten])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'dien_thoai', $this->dien_thoai])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'avatar', $this->avatar])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'password', $this->password]);

        return $dataProvider;
    }
    public static function getListTrangThaiDisplay(){
        return [
            self::CHUA_XAC_MINH => '<span style="color: orange">Chưa xác minh</span>',
            self::DANG_XAC_MINH => '<span style="color: orange">Đang xác minh</span>',
            self::DA_XAC_MINH => '<span class="text-success">Đã xác minh</span>',
        ];
    }
}
