<?php

namespace backend\models\search;

use backend\models\ChiTietGiaoDichDoanhNghiep;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ChiTietGiaoDichDoanhNghiepSearch represents the model behind the search form about `backend\models\use backend\models\ChiTietGiaoDichDoanhNghiep;
`.
 */
class ChiTietGiaoDichDoanhNghiepSearch extends ChiTietGiaoDichDoanhNghiep
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'don_hang_id', 'chu_don_hang', 'doanh_nghiep_id_chu_don_hang', 'id_doanh_nghiep_chu_don_hang', 'so_luong_can_tuyen', 'so_luong_da_tuyen', 'nguoi_cung_cap_id', 'active', 'doanh_nghiep_id', 'rate'], 'integer'],
            [['avatar_chu_don_hang', 'trang_thai_chu_don_hang', 'ten_doanh_nghiep_chu_don_hang', 'trang_thai_doanh_nghiep_chu_don_hang', 'trang_thai_don_hang', 'ten_tinh_thanh', 'ten_nghe_nghiep', 'type', 'don_vi', 'trang_thai', 'tien_do', 'avatar', 'trang_thai_nguoi_yeu_cau', 'ten_doanh_nghiep', 'trang_thai_doanh_nghiep', 'dia_chi_doanh_nghiep', 'last_mess'], 'string'],
            [['so_luong'], 'number'],
            [['createdS', 'created_mess', 'ten_cong_ty'], 'safe'],
            [['ten_chu_don_hang', 'ho_ten_nguoi_yeu_cau'], 'string', 'max' => 100],
            [['email_chu_don_hang', 'email_nguoi_yeu_cau', 'email_doanh_nghiep'], 'string', 'max' => 50],
            [['dien_thoai_chu_don_hang', 'dien_thoai_nguoi_yeu_cau'], 'string', 'max' => 13],
            [['dia_chi_chu_don_hang', 'dia_chi_nguoi_yeu_cau'], 'string', 'max' => 150],
            [['created', 'updated'], 'string', 'max' => 21],
            [['nguoi_dai_dien_doanh_nghiep'], 'string', 'max' => 32],
            [['dien_thoai_doanh_nghiep'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ChiTietGiaoDichDoanhNghiep::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created' => $this->created,
            'type' => $this->type,
            'so_luong' => $this->so_luong,
            'don_vi' => $this->don_vi,
            'trang_thai' => $this->trang_thai,
            'tien_do' => $this->tien_do,
        ]);

        $query->andFilterWhere(['like', 'ten_nghe_nghiep', $this->ten_nghe_nghiep])
            ->andFilterWhere(['like', 'ten_cong_ty', $this->ten_cong_ty])
            ->andFilterWhere(['like', 'ho_ten_nguoi_yeu_cau', $this->ho_ten_nguoi_yeu_cau])
            ->andFilterWhere(['like', 'ten_chu_don_hang', $this->ten_chu_don_hang]);


        return $dataProvider;
    }
}
