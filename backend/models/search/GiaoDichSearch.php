<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\GiaoDich;

/**
 * GiaoDichSearch represents the model behind the search form about `backend\models\GiaoDich`.
 */
class GiaoDichSearch extends GiaoDich
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_created', 'don_hang_id', 'nguoi_cung_cap_id', 'nguoi_nhan_id', 'active'], 'integer'],
            [['type', 'don_vi', 'trang_thai', 'tien_do'], 'safe'],
            [['so_luong'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GiaoDich::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_created' => $this->user_created,
            'don_hang_id' => $this->don_hang_id,
            'nguoi_cung_cap_id' => $this->nguoi_cung_cap_id,
            'nguoi_nhan_id' => $this->nguoi_nhan_id,
            'so_luong' => $this->so_luong,
            'active' => $this->active,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'don_vi', $this->don_vi])
            ->andFilterWhere(['like', 'trang_thai', $this->trang_thai])
            ->andFilterWhere(['like', 'tien_do', $this->tien_do]);

        return $dataProvider;
    }
}
