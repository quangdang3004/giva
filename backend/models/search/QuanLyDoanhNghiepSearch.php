<?php

namespace backend\models\search;

use backend\models\DoanhNghiep;
use backend\models\QuanLyDoanhNghiep;
use common\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\DonVi;

/**
 * DonViSearch represents the model behind the search form about `backend\models\DonVi`.
 */
class QuanLyDoanhNghiepSearch extends QuanLyDoanhNghiep
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'user_created', 'user_quan_li'], 'integer'],
            [['name', 'avatar', 'dia_chi', 'trang_thai', 'trang_thai_nguoi_quan_ly'], 'string'],
            [['created', 'updated'], 'safe'],
            [['nguoi_dai_dien', 'ma_so_thue'], 'string', 'max' => 32],
            [['dien_thoai'], 'string', 'max' => 10],
            [['link_diu_tup'], 'string', 'max' => 30],
            [['email'], 'string', 'max' => 50],
            [['nguoi_tao_doanh_nghiep', 'nguoi_quan_ly_doanh_nghiep'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $idSearch = null)
    {
        $query = QuanLyDoanhNghiep::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if(!is_null($idSearch))
        {
            if(!isset($_GET['id'])){
                $query->andFilterWhere(['id' => -1]);
//                phong_ban
            }
        }
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
//        $query ->andFilterWhere(['kich_hoat_doanh_nghiep'=>User::DA_XAC_MINH])
//            ->andFilterWhere(['or',
//                ['like', 'ten_doanh_nghiep', $this->ten_doanh_nghiep],
//                ['like', 'ma_so_thue', $this->ten_doanh_nghiep],
//            ])
//            ->andFilterWhere(['like','nguoi_dai_dien',$this->nguoi_dai_dien])
//        ;
        $query->andFilterWhere(['<>', 'trang_thai', DoanhNghiep::TU_CHOI]);
        $query->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['email'=>$this->email])
                ->andFilterWhere(['like', 'nguoi_dai_dien', $this->nguoi_dai_dien])
                ->andFilterWhere(['like', 'nguoi_quan_ly_doanh_nghiep', $this->nguoi_quan_ly_doanh_nghiep])
                ->andFilterWhere(['trang_thai' => $this->trang_thai])
                ->andFilterWhere(['dien_thoai' =>$this->dien_thoai])
                ->andFilterWhere(['ma_so_thue' =>$this->ma_so_thue]);
        return $dataProvider;
    }
}
