<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\NhaTuyenDungCongTac;

/**
 * NhaTuyenDungCongTacSearch represents the model behind the search form about `backend\models\NhaTuyenDungCongTac`.
 */
class NhaTuyenDungCongTacSearch extends NhaTuyenDungCongTac
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'active'], 'integer'],
            [['ten_cong_ty', 'thoi_gian_vao', 'thoi_gian_nghi', 'trang_thai', 'trang_thai_lam_viec', 'created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NhaTuyenDungCongTac::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'thoi_gian_vao' => $this->thoi_gian_vao,
            'thoi_gian_nghi' => $this->thoi_gian_nghi,
            'created' => $this->created,
            'active' => $this->active,
        ]);

        $query->andFilterWhere(['like', 'ten_cong_ty', $this->ten_cong_ty])
            ->andFilterWhere(['like', 'trang_thai', $this->trang_thai])
            ->andFilterWhere(['like', 'trang_thai_lam_viec', $this->trang_thai_lam_viec]);

        return $dataProvider;
    }
}
