<?php

namespace backend\models\search;

use common\models\myAPI;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\BannerApp;
use yii\helpers\VarDumper;

/**
 * BannerAppSearch represents the model behind the search form about `backend\models\BannerApp`.
 */
class BannerAppSearch extends BannerApp
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'active'], 'integer'],
            [['title', 'link', 'created', 'exp', 'url_chuyen_huong'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BannerApp::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ($this->created){
            $this->created = myAPI::convertDateSaveIntoDb($this->created);
        }
        if ($this->exp){
            $this->exp = myAPI::convertDateSaveIntoDb($this->exp);
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'active' => 1,
        ]);

        $query
            ->andFilterWhere(['like', 'link', $this->link])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'exp', $this->exp])
            ->andFilterWhere(['like', 'created', $this->created])
            ->andFilterWhere(['like', 'updated', $this->updated])
            ->andFilterWhere(['like', 'url_chuyen_huong', $this->url_chuyen_huong]);

        $query->orderBy(['created' => SORT_DESC]);

        return $dataProvider;
    }
}
