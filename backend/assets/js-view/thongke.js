$(document).ready(function () {
    $(document).on('change', '#loai-thong-ke', function () {
        if($(this).val() == 'Theo tháng'){
            $("#block-tu-thang-den-thang").removeClass('hidden');
            $("#block-tu-ngay-den-ngay").addClass('hidden');
        }
        else{
            $("#block-tu-thang-den-thang").addClass('hidden');
            $("#block-tu-ngay-den-ngay").removeClass('hidden');
        }
    });

    $(document).on('click', '.btn-thong-ke-khach-hang', function (e) {
        e.preventDefault();
        var cart = [];
        $.ajax({
            url: 'index.php?r=san-pham/thong-ke',
            data: $("#thong-ke-khach-hang").serializeArray(),
            dataType: 'json',
            type: 'post',
            beforeSend: function () {
                // $("#ket-qua").html('');
                $('.thongbao').html('');
                Metronic.blockUI();
            },
            success: function (data) {
                // $("#ket-qua").html(data.content);
                for(var i in data.date) {
                    cart.push({country: data.date[i], visits: data.value[i]});
                }

                am4core.ready(function() {

// Themes begin
                    am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
                    var chart = am4core.create("chartdiv", am4charts.XYChart);

// Add data
                    chart.data = cart;
                    //
                    // chart.data = [{
                    //     "country": "USA",
                    //     "visits": 2025
                    // },{
                    //     "country": "Brazil",
                    //     "visits": 395
                    // }];

// Create axes

                    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                    categoryAxis.dataFields.category = "country";
                    categoryAxis.renderer.grid.template.location = 0;
                    categoryAxis.renderer.minGridDistance = 30;

                    categoryAxis.renderer.labels.template.adapter.add("dy", function(dy, target) {
                        if (target.dataItem && target.dataItem.index & 2 == 2) {
                            return dy + 25;
                        }
                        return dy;
                    });

                    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

// Create series
                    var series = chart.series.push(new am4charts.ColumnSeries());
                    series.dataFields.valueY = "visits";
                    series.dataFields.categoryX = "country";
                    series.name = "Visits";
                    series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
                    series.columns.template.fillOpacity = .8;

                    var columnTemplate = series.columns.template;
                    columnTemplate.strokeWidth = 2;
                    columnTemplate.strokeOpacity = 1;

                }); // end am4core.ready()


                // console.log(data.date);
//                 am4core.ready(function() {
//
// // Themes begin
//                     am4core.useTheme(am4themes_animated);
// // Themes end
//
// // Create chart instance
//                     var chart = am4core.create("chartdiv", am4charts.XYChart);
//
// // Add data
// //                     chart.data = [{
// //                         "date": "2012-12-27",
// //                         "value": 50
// //                     }, {
// //                         "date": "2012-12-28",
// //                         "value": 50
// //                     }, {
// //                         "date": "2012-12-29",
// //                         "value": 51
// //                     }];
//                     chart.data = cart;
//
// // Set input format for the dates
//                     chart.dateFormatter.inputDateFormat = "yyyy-MM-dd";
//
// // Create axes
//                     var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
//                     var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
//
// // Create series
//                     var series = chart.series.push(new am4charts.LineSeries());
//                     series.dataFields.valueY = "value";
//                     series.dataFields.dateX = "date";
//                     series.tooltipText = "{value}";
//                     series.strokeWidth = 2;
//                     series.minBulletDistance = 15;
//
// // Drop-shaped tooltips
//                     series.tooltip.background.cornerRadius = 20;
//                     series.tooltip.background.strokeOpacity = 0;
//                     series.tooltip.pointerOrientation = "vertical";
//                     series.tooltip.label.minWidth = 40;
//                     series.tooltip.label.minHeight = 40;
//                     series.tooltip.label.textAlign = "middle";
//                     series.tooltip.label.textValign = "middle";
//
// // Make bullets grow on hover
//                     var bullet = series.bullets.push(new am4charts.CircleBullet());
//                     bullet.circle.strokeWidth = 2;
//                     bullet.circle.radius = 4;
//                     bullet.circle.fill = am4core.color("#fff");
//
//                     var bullethover = bullet.states.create("hover");
//                     bullethover.properties.scale = 1.3;
//
// // Make a panning cursor
//                     chart.cursor = new am4charts.XYCursor();
//                     chart.cursor.behavior = "panXY";
//                     chart.cursor.xAxis = dateAxis;
//                     chart.cursor.snapToSeries = series;
//
// // Create vertical scrollbar and place it before the value axis
//                     chart.scrollbarY = new am4core.Scrollbar();
//                     chart.scrollbarY.parent = chart.leftAxesContainer;
//                     chart.scrollbarY.toBack();
//
// // Create a horizontal scrollbar with previe and place it underneath the date axis
//                     chart.scrollbarX = new am4charts.XYChartScrollbar();
//                     chart.scrollbarX.series.push(series);
//                     chart.scrollbarX.parent = chart.bottomAxesContainer;
//
//                     dateAxis.start = 0.79;
//                     dateAxis.keepSelection = true;
//
//
//                 }); // end am4core.ready()
            },
            error: function (r1, r2) {
                $('.thongbao').html(r1.responseText)
            },
            complete: function () {
                Metronic.unblockUI();
            }
        })

    });

    $(document).on('click', '.btn-thong-ke-san-pham', function (e) {
        e.preventDefault();
        var $message = '';
        var $loi = 0;
        var cart = [];
        if($("#kieu-thong-ke").val() == ''){
            $loi++;
            $message += '<p>Vui lòng chọn kiểu thống kê</p>';
        }
        if($loi == 0){
            $.ajax({
                url: 'index.php?r=san-pham/thong-ke-san-pham',
                data: $("#thong-ke-san-pham").serializeArray(),
                dataType: 'json',
                type: 'post',
                beforeSend: function () {
                    // $("#ket-qua").html('');
                    $('.thongbao').html('');
                    Metronic.blockUI();
                },
                success: function (data) {
                    // $("#ket-qua-thong-ke").html(data.content);
                    for(var i in data.date) {
                        cart.push({country: data.date[i], visits: data.value[i]});
                    }

                    am4core.ready(function() {

// Themes begin
                        am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
                        var chart = am4core.create("chartdiv", am4charts.XYChart);

// Add data
                        chart.data = cart;
                        //
                        // chart.data = [{
                        //     "country": "USA",
                        //     "visits": 2025
                        // },{
                        //     "country": "Brazil",
                        //     "visits": 395
                        // }];

// Create axes

                        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                        categoryAxis.dataFields.category = "country";
                        categoryAxis.renderer.grid.template.location = 0;
                        categoryAxis.renderer.minGridDistance = 30;

                        categoryAxis.renderer.labels.template.adapter.add("dy", function(dy, target) {
                            if (target.dataItem && target.dataItem.index & 2 == 2) {
                                return dy + 25;
                            }
                            return dy;
                        });

                        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

// Create series
                        var series = chart.series.push(new am4charts.ColumnSeries());
                        series.dataFields.valueY = "visits";
                        series.dataFields.categoryX = "country";
                        series.name = "Visits";
                        series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
                        series.columns.template.fillOpacity = .8;

                        var columnTemplate = series.columns.template;
                        columnTemplate.strokeWidth = 2;
                        columnTemplate.strokeOpacity = 1;

                    }); // end am4core.ready()
                },
                error: function (r1, r2) {
                    $('.thongbao').html(r1.responseText)
                },
                complete: function () {
                    Metronic.unblockUI();
                }
            })
        }else {
            $.alert($message);
        }


    });

    // $(document).on('click', '.tai-ket-qua-thong-ke', function (e) {
    //     e.preventDefault();
    //     taiFileExcel('thu-phi-hoi-vien/tai-ket-qua-thong-ke',$("#thong-ke-thu-phi").serializeArray());
    // });


});





