$(document).ready(function() {
    $(document).on('click', '.btn-sua-don-hang', function (e) {
        e.preventDefault();
        $.ajax({
            url: 'index.php?r=don-hang/sua-don-hang',
            data: { id_donhang: $(this).attr('data-value') },
            type: 'post',
            dataType: 'json',
            beforeSend: function () {
                $(".thongbao").html('');
                Metronic.blockUI();
            },
            complete: function () {
                Metronic.unblockUI();
            },
            success: function (data){
                $("#modal-sua-don-hang .content-body").html(data.content);
                $("#modal-sua-don-hang .modal-title").html(data.header);
                // mo modal
                $("#modal-sua-don-hang").modal('show');
            },
            error: function (r1, r2) {
                $('.thongbao').html(r1.responseText);
            },
        })
    })

    $(document).on("click", "#luu-chi-tiet-don-hang", function (e) {
            e.preventDefault();
            SaveObject(
                "don-hang/luu-chi-tiet-don-hang",
                $("#form-sua-don-hang").serializeArray(),
                function () {
                    setTimeout(function () {
                        window.location.reload()
                    }, 2 * 1000)

                }
            );
        }
    );
})