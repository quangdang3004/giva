function initiateSelect2() {
    $('#quanlysanphamsearch-quan_huyen').select2();
}

$(document).ready(function () {
    initiateSelect2();

    $('#ajaxCrudModal').on('shown.bs.modal', function () {
        initiateSelect2();
    })

    $("#crud-datatable-pjax").on('pjax:success', function() {
        initiateSelect2();
    });
})
