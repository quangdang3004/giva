$(document).ready(function (){
    $(document).on('click','.btn-xem-chi-tiet-doanh-nghiep',function (e){
        e.preventDefault();
        loadForm({type:'xem-chi-tiet-doanh-nghiep',id:$(this).attr('data-value')},'xl',function (data){},
            function (data){

            });
    })
    $(document).on('click','.btn-xac-minh',function (e){
        e.preventDefault();
        var $idND = $(this).attr('data-value');
        $.confirm({
            content: 'Bạn có chắc chắn xác minh doanh nghiệp này không ?',
            text: 'Xác minh doanh nghiệp',
            icon: 'fa fa-question',
            title: 'Xác minh doanh nghiệp',
            type: 'blue',
            buttons: {
                btnOK: {
                    text: '<i class="fa fa-check"></i> Xác minh',
                    btnClass: 'btn-primary',
                    action: function () {
                        SaveObject('user/xac-minh-doanh-nghiep', {id: $idND},function (data){
                            $.pjax.reload({
                                container:'#crud-datatable-pjax'
                            })
                        });
                    }
                },
                btnClose: {
                    text: '<i class="fa fa-close"></i> Huỷ'
                }
            }
        });
    })
    $(document).on('click', '.xoa-anh-doanh-nghiep', function (e) {
        e.preventDefault();
        var $idND = $(this).attr('data-value');
        var $myCol = $(this).parent().parent();

        $.confirm({
            content: 'Bạn có chắc chắn xoá ảnh này không?',
            text: 'Thông báo',
            icon: 'fa fa-question',
            title: 'Xoá ảnh sản phẩm',
            type: 'blue',
            buttons: {
                btnOK: {
                    text: '<i class="fa fa-check"></i> Chấp nhận',
                    btnClass: 'btn-primary',
                    action: function () {
                        viewData('user/xoa-anh', {isNGuoiDung: $idND}, 'm', function () {
                            $myCol.remove();
                        });
                    }
                },
                btnClose: {
                    text: '<i class="fa fa-close"></i> Huỷ và đóng lại'
                }
            }
        });
    });
    $(document).on('click', '.xoa-doanh-nghiep', function (e) {
        e.preventDefault();
        var $idND = $(this).attr('data-value');


        $.confirm({
            content: 'Bạn có chắc chắn xoá doanh nghiệp này không?',
            text: 'Thông báo',
            icon: 'fa fa-question',
            title: 'Xoá doanh nghiệp',
            type: 'blue',
            buttons: {
                btnOK: {
                    text: '<i class="fa fa-check"></i> Xác nhận',
                    btnClass: 'btn-primary',
                    action: function () {
                        SaveObject('user/xoa-doanh-nghiep', {id: $idND}, function (data) {
                            $.pjax.reload({
                                container:'#crud-datatable-pjax'
                            })
                        });
                    }
                },
                btnClose: {
                    text: '<i class="fa fa-close"></i> Huỷ và đóng lại'
                }
            }
        });
    });
})