function changeKhuVuc($value, $object, $type) {
    $.ajax({
        type: "POST",
        url: 'index.php?r=danh-muc/get-khu-vuc-chon-nhieu',
        data: {value: $value, type: $type},
        dataType: 'json',
        beforeSend: function () {
            Metronic.blockUI();
        },
        success: function (response) {
            $(".div").append(response)
            if (response !== '') {
                $.each(response, function (k, value) {
                    $object.append('<option value="' + value.name + '">' + value.name + '</option>');
                });
            }
        }, complete: function (response) {
            Metronic.unblockUI();
        },
        error: function (r1, r2) {
            console.log(r1.responseText)
        }
    });
}
// Restricts input for each element in the set of matched elements to the given inputFilter.
(function($) {
    $.fn.inputFilter = function(callback, errMsg) {
        return this.on("input keydown keyup mousedown mouseup select contextmenu drop focusout", function(e) {
            if (callback(this.value)) {
                // Accepted value
                if (["keydown","mousedown","focusout"].indexOf(e.type) >= 0){
                    $(this).removeClass("input-error");
                    this.setCustomValidity("");
                }
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                // Rejected value - restore the previous one
                $(this).addClass("input-error");
                this.setCustomValidity(errMsg);
                this.reportValidity();
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            } else {
                // Rejected value - nothing to restore
                this.value = "";
            }
        });
    };
}(jQuery));


$(document).ready(function () {//
    $('.ten-tinh-thanh-dropdown').select2();

    $("#ajaxCrudModal").on('shown.bs.modal', function (e) {
        setTimeout(function () {
            $("#danhmuc-parent_id").select2();
        }, 100);
    });

    $("#crud-datatable-pjax").on('pjax:success', function () {
        $('#quanlykhachhangsearch-tu_ngay').datepicker($.extend({}, $.datepicker.regional['vi'], {
            "dateFormat": "dd/mm/yy",
            "changeMonth": true,
            "yearRange": "2015:2025",
            "changeYear": true
        }));
        $('#quanlykhachhangsearch-den_ngay').datepicker($.extend({}, $.datepicker.regional['vi'], {
            "dateFormat": "dd/mm/yy",
            "changeMonth": true,
            "yearRange": "2015:2025",
            "changeYear": true
        }));
        $('#quanlyyeucauxacthucsearch-created').datepicker($.extend({}, $.datepicker.regional['vi'], {
            "dateFormat": "dd/mm/yy",
            "changeMonth": true,
            "yearRange": "2015:2025",
            "changeYear": true
        }));
    });
//
    $(document).on('click', '.btn-xem-khu-vuc', function (e) {
        e.preventDefault();
        id = $(this).attr('data-value');
        viewData('danh-muc/chi-tiet-khu-vuc', {id}, 's');
    });

    $(document).on('click', '.btn-xem-don-hang', function (e) {
        e.preventDefault();
        id = $(this).attr('data-value');
        viewData('don-hang/chi-tiet-don-hang', {id}, 's');
    });
    //check null danh muc
    $(document).on('keyup mousemove', "#danhmuc-name, #form-tao-moi-danh-muc", function (e) {
        e.preventDefault();
        if ($("#danhmuc-name").val() == '' ) {
            $('.btn-them-moi').prop('disabled', true)
        } else {
            $('.btn-them-moi').prop('disabled', false)
        }
        if ($('#danhmuc-id').val() !== 0 && $("#danhmuc-id").val() !== '') {
            $(".btn-them-moi").prop('disabled', false)
            if ($("#danhmuc-name").val() == '' ) {
                $('.btn-them-moi').prop('disabled', true)
            }
        }
    })

    $(document).on('click', '.btn-update-danh-muc', function (e) {
        e.preventDefault();
        id = $(this).attr('data-value');
        loadFormDanhMuc({type: 'them_moi_danh_muc', id: $(this).attr('data-value')}, 's', function () {

        }, function () {

        });
    });

    $(document).on('click', '.btn-them-moi-danh-muc', function (e) {
        e.preventDefault();
        doi_tuong = $(this).attr('data-value');

        loadFormDanhMuc({type: 'them_moi_danh_muc', id: ''}, 's', function () {
        }, function () {
        });

    });

    $(document).on('click', '.btn-tao-moi-doanh-nghiep', function (e) {
        e.preventDefault();
        loadFormDoanhNghiep({type: 'update_doanh_nghiep', id: ''}, 'l', function () {
        }, function () {
        });

    });

    $(document).on('click', '.btn-them-moi', function (e) {
        e.preventDefault();
        var id = $('#danhmuc-id').val();
        var name = $('#danhmuc-name').val();
        var form_data = new FormData();
        form_data.append('id', id);
        form_data.append('name', name);
        // form_data.append('active',1);
        // console.log(name);
        $.ajax({
            url: 'index.php?r=danh-muc/them-moi',
            data: form_data,
            dataType: 'json',
            processData: false,  // tell jQuery not to process the data
            contentType: false,
            type: 'post',
            beforeSend: function () {
                // $('.thongbao').html('');
                Metronic.blockUI();
            },
            success: function (data) {
                alertify.success(data.content)

                // setTimeout(function () {
                //     window.location.reload()
                // }, 1500)
            },
            error: function (r1, r2) {
                alertify.error(r1.responseJSON.message)
            },
            complete: function () {
                Metronic.unblockUI();
            }
        })
    })

    $(document).on('click', '.btn-them-doanh-nghiep', function (e) {
        e.preventDefault();
        var id = $('#doanhnghiep-id').val();
        var name = $('#name').val();
        var form_data = new FormData();
        form_data.append('id', id);
        form_data.append('name', name);
        // form_data.append('active',1);
        // console.log(name);
        $.ajax({
            url: 'index.php?r=doanh-nghiep/tao-moi-doanh-nghiep',
            data: form_data,
            dataType: 'json',
            processData: false,  // tell jQuery not to process the data
            contentType: false,
            type: 'post',
            beforeSend: function () {
                // $('.thongbao').html('');
                Metronic.blockUI();
            },
            success: function (data) {
                alertify.success(data.content)

                setTimeout(function () {
                    window.location.reload()
                }, 1500)
            },
            error: function (r1, r2) {
                alertify.error(r1.responseJSON.message)
            },
            complete: function () {
                Metronic.unblockUI();
            }
        })
    })


    $(document).on('click', '.btn-huy-khoi-phuc-hoat-dong', function (e) {
        e.preventDefault();
        var $uid = $(this).attr('data-value');
        $.alert({
            title: 'Thông báo',
            icon: 'fa fa-warning',
            type: 'red',
            text: 'Bạn có chắc chắn muốn thực hiện việc này?',
            buttons: {
                btnAccept: {
                    text: '<i class="fa fa-check-circle-o"></i> Đồng ý',
                    action: function () {
                        SaveObject('user/change-status', {uid: $uid}, function (data) {
                            $.pjax({container: "#crud-datatable-pjax"});
                        })
                    },
                    btnClass: 'btn-primary'
                },
                btnCancel: {
                    text: '<i class="fa fa-ban"></i> Huỷ'
                }
            }
        })
    });

    $(document).on('click', '.btn-xem-doanh-nghiep', function (e) {
        e.preventDefault();
        viewData('doanh-nghiep/xem-doanh-nghiep', {id: $(this).attr('data-value')}, 'xl');
    });
    //check null doanh nghiep
    $(document).on('keyup mousemove', '#doanhnghiep-name, #doanhnghiep-nguoi_dai_dien, #doanhnghiep-dien_thoai, #doanhnghiep-ma_so_thue, #doanhnghiep-dia_chi, #doanhnghiep-email, #doanhnghiep-user_quan_li', function (e) {
        e.preventDefault();
        if ($("#doanhnghiep-name").val() === '' || $("#doanhnghiep-nguoi_dai_dien").val() === '' || $("#doanhnghiep-dien_thoai").val() === '' || $("#doanhnghiep-ma_so_thue").val() === '' || $("#doanhnghiep-dia_chi").val() === '' || $("#doanhnghiep-email").val() === '' || $("#doanhnghiep-user_quan_li").val() === '') {
            $('.btn-luu-doanh-nghiep').prop('disabled', true)
        } else {
            $('.btn-luu-doanh-nghiep').prop('disabled', false)
        }
    })
    $(document).on('click', '.btn-xem-giao-dich', function (e) {
        e.preventDefault();
        viewData('giao-dich/xem-chi-tiet-giao-dich', {id: $(this).attr('data-value')}, 'xl')
    });

    $(document).on('click', '.btn-update-doanh-nghiep', function (e) {
        e.preventDefault();
        viewDataNew('doanh-nghiep/update-doanh-nghiep', {id: $(this).attr('data-value')}, 'l');
    });
    $(document).on('click', '.btn-chitietnguoidung', function (e) {
        e.preventDefault();
        viewDataGet('user/update', {id: $(this).attr('data-value')}, 'l');
    });
    $(document).on('click', '.btn-xoa-doanh-nghiep', function (e) {
        e.preventDefault();
        id = $(this).attr('data-value'); // id doanh nghiep
        viewDataDel('doanh-nghiep/delete', {id: $(this).attr('data-value')}, 's');

    });

    $(document).on('click', '.btn-xem-chi-tiet', function (e) {
        e.preventDefault();
        $active = $(this).attr('data-active');
        if ($active === 'Chưa xác minh' || $active === 'Đang xác minh') {
            loadFormNew({
                type: 'xem-chi-tiet-xac-thuc',
                id: $(this).attr('data-value'),
                active: $(this).attr('data-active')
            }, 'xl', function (data) {
                setTimeout(function () {
                    $('.anh-yeu-cau-xac-thuc').magnificPopup({
                        delegate: 'a',
                        type: 'image',
                        tLoading: 'Loading image #%curr%...',
                        mainClass: 'mfp-img-mobile',
                        gallery: {
                            enabled: true,
                            navigateByImgClick: true,
                            preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
                        },
                        image: {
                            tError: '',
                            titleSrc: function (item) {
                                return '';
                            }
                        }
                    });
                }, 500)

            }, function () {
            })
        } else
            loadForm({
                type: 'xem-chi-tiet-xac-thuc',
                id: $(this).attr('data-value'),
                active: $(this).attr('data-active')
            }, 'xl', function (data) {
                setTimeout(function () {
                    $('.anh-yeu-cau-xac-thuc').magnificPopup({
                        delegate: 'a',
                        type: 'image',
                        tLoading: 'Loading image #%curr%...',
                        mainClass: 'mfp-img-mobile',
                        gallery: {
                            enabled: true,
                            navigateByImgClick: true,
                            preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
                        },
                        image: {
                            tError: '',
                            titleSrc: function (item) {
                                return '';
                            }
                        }
                    });
                }, 500)

            }, function () {
            });
    });
    //         },
    //         error: function (r1, r2) {
    //             $('.thongbao').html(r1.responseText)
    //         },
    //         complete: function () {
    //             Metronic.unblockUI();
    //         }
    //     })

    // });

    $(document).on('click', '.btn-xac-minh-yeu-cau', function (e) {
        e.preventDefault()
        $.ajax({
            url: 'index.php?r=yeu-cau-xac-thuc/xac-minh-yeu-cau',
            data: {id: $(this).attr('data-value')},
            dataType: 'json',
            type: 'post',
            beforeSend: function () {
            },
            success: function (data) {

                alertify.success(data.content)

                setTimeout(function () {
                    window.location.reload()
                },  1500)
            },
            error: function (r1, r2) {
                alertify.error(r1.responseJSON.message)
            },
        })
    })
    $(document).on('click', '.btn-huy-yeu-cau', function (e) {
        e.preventDefault()
        const id = $(this).attr('data-value');
        loadForm2({type: 'huy-yeu-cau-xac-thuc', id: $(this).attr('data-value')}, 'm', function (data) {

        }, function () {
            $.ajax({
                url: 'index.php?r=yeu-cau-xac-thuc/huy-yeu-cau-xac-thuc',
                data: {
                    ghi_chu: $('#form-huy-yeu-cau').serializeArray(),
                    id: id,
                },
                dataType: 'json',
                type: 'post',
                beforeSend: function () {
                },
                success: function (data) {

                    alertify.success(data.content)

                    setTimeout(function () {
                        window.location.reload()
                    }, 1500)
                },
                error: function (r1, r2) {
                    alertify.error(r1.responseJSON.message)
                },
            })
        })
    })

    $(document).on('click', '.btn-xem-chi-tiet-nha-tuyen-dung', function (e) {
        e.preventDefault()
        const id = $(this).attr('data-value')
        loadFormCongXu({type: 'xem-chi-tiet-nha-tuyen-dung', id: $(this).attr('data-value')}, 'xl', function (data) {
            setTimeout(function () {
                $('.anh-thong-tin-nguoi-dung').magnificPopup({
                    delegate: 'a',
                    type: 'image',
                    tLoading: 'Loading image #%curr%...',
                    mainClass: 'mfp-img-mobile',
                    gallery: {
                        enabled: true,
                        navigateByImgClick: true,
                        preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
                    },
                    image: {
                        tError: '',
                        titleSrc: function (item) {
                            return '';
                        }
                    }
                });
            }, 500)
        }, function () {

        })
    })

    $("#so-xu-nhap-vao").inputFilter(function(value) {
        return /^\d*$/.test(value); }, "Must be an unsigned integer");

    $(document).on('click', '.btn-them-banner-app', function (e) {
        e.preventDefault()
        loadFormBanner({type: 'them-banner-app', id: ''}, 'l', function (data) {
        }, function () {
        })
    })

    $(document).on('click', '.btn-sua-banner', function (e) {
        e.preventDefault()
        loadFormBanner({type: 'them-banner-app', id: $(this).attr('data-value')}, 'l', function (data) {
            setTimeout(function () {
                $('.bannerapps-sua-banner').magnificPopup({
                    delegate: 'a',
                    type: 'image',
                    tLoading: 'Loading image #%curr%...',
                    mainClass: 'mfp-img-mobile',
                    gallery: {
                        enabled: true,
                        navigateByImgClick: true,
                        preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
                    },
                    image: {
                        tError: '',
                        titleSrc: function (item) {
                            return '';
                        }
                    }
                });
            }, 500)
        }, function () {

        })
    })

    $(document).on('keyup mousemove', '#anh_banner_app, #them_banner_moi, #bannerapp-title, #bannerapp-exp, #bannerapp-url_chuyen_huong', function (e) {
        e.preventDefault();
        if ($("#anh_banner_app").val() === '' || $("#bannerapp-title").val() === '' || $("#bannerapp-exp").val() === '' || $("#bannerapp-url_chuyen_huong").val() === '') {
            $('.btn-them-banner').prop('disabled', true)
        } else {
            $('.btn-them-banner').prop('disabled', false)
        }
        if ($('#bannerapp-id').val() !== 0 && $("#bannerapp-id").val() !== '') {
            $(".btn-them-banner").prop('disabled', false)
            if ($('#bannerapp-title').val() === '' || $('#bannerapp-exp').val() === '' || $('#bannerapp-url_chuyen_huong').val() === '') {
                $('.btn-them-banner').prop('disabled', true)
            }
        }
    })

    $(document).on('click', '.btn-them-banner', function (e) {
        e.preventDefault();
        var id = $('#bannerapp-id').val();
        var anh_banner = $('#anh_banner_app');
        var exp = $('#bannerapp-exp').val();
        var title = $('#bannerapp-title').val();
        var url_chuyen_huong = $('#bannerapp-url_chuyen_huong').val();

        var form_data = new FormData();
        form_data.append('file_anh', $('#anh_banner_app')[0].files[0]);
        form_data.append('id', id);
        form_data.append('exp', exp);
        form_data.append('title', title);
        form_data.append('url_chuyen_huong', url_chuyen_huong);

        $.ajax({
            url: 'index.php?r=banner-app/them-banner',
            data: form_data,
            dataType: 'json',
            processData: false,  // tell jQuery not to process the data
            contentType: false,
            type: 'post',
            beforeSend: function () {
                // $('.thongbao').html('');
                Metronic.blockUI();
            },
            success: function (data) {
                alertify.success(data.content)

                setTimeout(function () {
                    window.location.reload()
                }, 1500)
            },
            error: function (r1, r2) {
                alertify.error(r1.responseJSON.message)
            },
            complete: function () {
                Metronic.unblockUI();
            }
        })
        // SaveObject('detail/luu-detail', {id_detail, anh_selected, text_detail, id_kichban, ngay_detail, gio_detail, phut_detail}, function (data) {
        // })

    })

    $(document).on('click', '.btn-xoa-banner', function (e) {
        id = $(this).attr('data-value')
        viewDataDel('banner-app/delete', {id: $(this).attr('data-value')}, 'm', function () {
        })
    })
    $(document).on('click', '.btn-xoa-thanh-vien', function (e) {
        id = $(this).attr('data-value')
        viewDataDel('user/xoa-nguoi-dung', {id: $(this).attr('data-value')}, 'm', function () {
        })
    })

    $(document).on('click', '.them-thanh-vien', function (e) {
        e.preventDefault();
        loadForm2({type: 'them_thanh_vien', id: ''}, 'l', function (data) {

        }, function () {

        })
    })

    $('#crud-datatable-pjax').on('pjax:success', function () {
        $('.created-date-field').datepicker({dateFormat: 'dd/mm/yy'})
        $('.ten-tinh-thanh-dropdown, #doanhnghiep_nguoi_quan_li_doanh_nghiep').select2()
    })

    $(document).on('change', '#quanlydonhang-ten-khu-vuc', function () {
        $('#quanlydonhang-ten-tinh-thanh').val('').trigger('change');
        $('#quanlydonhang-ten-tinh-thanh').empty();
        changeKhuVuc($(this).val(), $('#quanlydonhang-ten-tinh-thanh'), "Tỉnh thành")
    })


    function formatCash(str) {
        return str.split('').reverse().reduce((prev, next, index) => {
            return ((index % 3) ? next : (next + ',')) + prev
        })
    }
});
