$(document).ready(function () {
    $(document).on('click','#danh-sach-cho-chap-nhan-chia-se',function (e) {
        e.preventDefault();
        viewData('user/danh-sach-cho-chap-nhan-chia-se',{id: $(this).attr('data-value')},'l')
    });

    $(document).on('click','#danh-sach-nhan-chia-se',function (e) {
        e.preventDefault();
        viewData('user/danh-sach-nhan-chia-se',{id: $(this).attr('data-value')},'l')
    });
    $(document).on('click','.btn-xac-nhan-yeu-cau-chia-se',function (e) {
        e.preventDefault();
        var $id = $(this).attr('data-value');
        var $mytr = $(this).parent().parent();
        loadForm({type: 'xac_nhan_yeu_cau_chia_se', id: $id}, 'm', function (data) {
        }, function () {
            var $loi = 0;
            var $message = '';
            if($("#chiasekhachhang-trang_thai_chia_se").val() == ''){
                $loi++;
                $message += '<p>Vui lòng xác nhận yêu cầu</p>';
            }
            if($loi == 0){
                SaveObject('user/xac-nhan-yeu-cau-chia-se', $("#form-xac-nhan-yeu-cau-chia-se").serializeArray(), function (data) {
                    // $.pjax.reload({container: "#crud-datatable-pjax"});
                    $mytr.remove();
                });
            }else {
                $.alert($message);
                return false;
            }

        })
    });

    $(document).on('click','.btn-nhan-chia-se',function (e) {
        e.preventDefault();
        var $id = $(this).attr('data-value');
        var $mytr = $(this).parent().parent();
        loadForm({type: 'nhan_chia_se', id: $id}, 'm', function (data) {
        }, function () {
            var $loi = 0;
            var $message = '';
            if($("#chiasekhachhang-trang_thai_chia_se").val() == ''){
                $loi++;
                $message += '<p>Vui lòng xác nhận yêu cầu</p>';
            }
            if($loi == 0){
                SaveObject('user/nhan-chia-se', $("#form-nhan-chia-se").serializeArray(), function (data) {
                    // $.pjax.reload({container: "#crud-datatable-pjax"});
                    $mytr.remove();
                });
            }else {
                $.alert($message);
                return false;
            }
        })
    });

});