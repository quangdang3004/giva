<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\GiayToLienKet */
?>
<div class="giay-to-lien-ket-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nha_tuyen_dung_id',
            'doanh_nghiep_id',
            'cong_ti_id',
            'link',
            'active',
            'type',
        ],
    ]) ?>

</div>
