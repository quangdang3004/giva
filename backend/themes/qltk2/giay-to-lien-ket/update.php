<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\GiayToLienKet */
?>
<div class="giay-to-lien-ket-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
