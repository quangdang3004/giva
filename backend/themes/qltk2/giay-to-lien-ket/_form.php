<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\GiayToLienKet */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="giay-to-lien-ket-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nha_tuyen_dung_id')->textInput() ?>

    <?= $form->field($model, 'doanh_nghiep_id')->textInput() ?>

    <?= $form->field($model, 'cong_ti_id')->textInput() ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'active')->textInput() ?>

    <?= $form->field($model, 'type')->dropDownList([ 'Doanh nghiệp' => 'Doanh nghiệp', 'Công ty' => 'Công ty', 'Ứng viên' => 'Ứng viên', ], ['prompt' => '']) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
