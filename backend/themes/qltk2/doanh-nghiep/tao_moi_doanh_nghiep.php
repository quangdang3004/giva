<?php
/** @var $model \backend\models\DoanhNghiep */
use yii\bootstrap\ActiveForm;
?>
<div style="height: auto; border: none">
    <?php  $form = ActiveForm::begin(['id' => 'form-tao-moi-doanh-nghiep']); ?>
        <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>

        <div class="row">
            <div class="col-md-12 " style="margin: 10px 0 10px 0">
                <label for="name">Tên doanh nghiệp</label>
                <input type="text" name="name" id="name">
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 " style="margin: 10px 0 10px 0">
                <label for="nguoi_dai_dien">Tên người đại diện</label>
                <input name="nguoi_dai_dien" id="nguoi_dai_dien" type="text">
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 " style="margin: 10px 0 10px 0">
                <label for="dien_thoai">Điện thoại</label>
                <input name="dien_thoai" id="dien_thoai" type="text">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 " style="margin: 10px 0 10px 0">
                <label for="ma_so_thue">Mã số thuế</label>
                <input name="ma_so_thue" id="ma_so_thue" type="text">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 " style="margin: 10px 0 10px 0">
                <label for="dia_chi">Địa chỉ</label>
                <input name="dia_chi" id="dia_chi" type="text">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 " style="margin: 10px 0 10px 0">
                <label for="email">Địa chỉ</label>
                <input name="email" id="email" type="text">
            </div>
        </div>
        <?= $form->field($model, 'trang_thai')->dropDownList([ 'Chưa xác minh' => 'Chưa xác minh', 'Đang xác minh' => 'Đang xác minh', 'Đã xác minh' => 'Đã xác minh', ], ['prompt' => '']) ?>

    <?php ActiveForm::end(); ?>
</div>
