<?php
/* @var $searchModel backend\models\search\DoanhNghiepSearch */

use backend\models\QuanLyYeuCauXacThuc;
use backend\models\search\DoanhNghiepSearch;
use yii\helpers\Url;
return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '1%',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '1%',
        'header' =>'STT',
        'headerOptions' => ['class' => 'text-center text-nowrap text-primary']
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'nguoi_dai_dien',
        'width' => '1%',
        'headerOptions' => ['class' => 'text-primary text-nowrap'],
        'contentOptions' => ['class' => 'text-nowrap'],
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'trang_thai',
//        'header' => 'Trạng thái',
//        'headerOptions' => ['class' => 'text-primary text-nowrap text-center'],
//        'contentOptions' => ['class' => 'text-nowrap'],
//        'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'trang_thai', DoanhNghiepSearch::getListTrangThai(),
//            ['class' => 'form-control', 'prompt' => '-- Chọn --']),
//        'value' => function($data){
//            /** @var $data DoanhNghiepSearch */
//            return $data->trang_thai ? DoanhNghiepSearch::getListTrangThaiDisplay()[$data->trang_thai]
//                : '<i>Đang cập nhật</i>';
//        },
//        'width' => '1%',
//        'format' =>'raw'
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'dien_thoai',
        'width' => '1%',
        'header' => 'Số điện thoại',
        'headerOptions' => ['class' => 'text-primary text-nowrap'],
        'contentOptions' => ['class' => 'text-nowrap text-right'],

    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'ma_so_thue',
        'width' => '1%',
        'headerOptions' => ['class' => 'text-primary text-nowrap'],
        'contentOptions' => ['class' => 'text-nowrap text-right'],
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'dia_chi',
//    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'email',
    // ],

    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'status',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'created',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'updated',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'user_created',
    // ],
    [
        'value' => function($data){
            return \yii\bootstrap\Html::a('<i class="fa fa-eye"></i>','#',
                ['class' => 'text-gray btn-xem-doanh-nghiep', 'title'=>'View','data-toggle'=>'tooltip', 'data-value' =>$data->id]);
        },
        'label' => 'Xem',
        'format' => 'raw',
        'width' => '1%',
        'contentOptions' => ['class' => 'text-center','style'=>'width:3%;'],
        'headerOptions' => ['class' => 'text-primary text-center','style'=>'width:3%; vertical-align: inherit;', 'rowspan'=>2, ]
    ],
    [
        'value' => function($data){
            return \yii\bootstrap\Html::a('<i class="fa fa-edit"></i>','#',
                ['class' => 'text-gray btn-update-doanh-nghiep', 'title'=>'Update', 'data-toggle'=>'tooltip','data-value' =>$data->id ]);
        },
        'label' => 'Sửa',
        'format' => 'raw',
        'width' => '1%',
        'contentOptions' => ['class' => 'text-center','style'=>'width:3%;'],
        'headerOptions' => ['class' => 'text-primary text-center','style'=>'width:3%; vertical-align: inherit;', 'rowspan' =>2]
    ],
    [
        'value' => function($data){
            $model = \backend\models\DoanhNghiep::findOne($data->id);
            return \yii\bootstrap\Html::a('<i class="fa fa-trash"></i>','#',
                ['class' => 'text-danger btn-xoa-doanh-nghiep', 'title'=>'Delete', 'data-toggle'=>'tooltip','data-value' =>$data->id ]);

//            \yii\helpers\VarDumper::dump($model->active, 10, true); exit();
//            if($model->status == 10)
//                return \common\models\myAPI::createDeleteBtnInGrid(Url::toRoute(['doanh-nghiep/delete', 'id' => $data->id]));
        },
        'label' => 'Xóa',
        'format' => 'raw',
        'width' => '1%',
        'contentOptions' => ['class' => 'text-center text-danger','style'=>'width:3%;'],
        'headerOptions' => ['class' => 'text-primary text-center','style'=>'width:3%; vertical-align: inherit;', 'rowspan' =>2]
    ],

];