<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\DoanhNghiep */

?>
<div class="doanh-nghiep-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
