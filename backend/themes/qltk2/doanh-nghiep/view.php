<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\DoanhNghiep */
?>
<div class="doanh-nghiep-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name:ntext',
            'nguoi_dai_dien',
            'dien_thoai',
            'ma_so_thue',
            'dia_chi:ntext',
            'email:email',
            'trang_thai',
            'status',
            'created',
            'updated',
            'user_created',
        ],
    ]) ?>

</div>
