<?php
/** @var $model \backend\models\DoanhNghiep */
use yii\bootstrap\ActiveForm;
use backend\models\DoanhNghiep;
?>
<div class="form-control" style="height: auto; border: none">
    <?php  $form = ActiveForm::begin(['id' => 'form-update-doanh-nghiep']); ?>

    <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'name')->textInput(['rows' => 6]) ?>

    <?= $form->field($model, 'nguoi_dai_dien')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dien_thoai')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ma_so_thue')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dia_chi')->textInput(['rows' => 6]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_quan_li')->dropDownList(\yii\helpers\ArrayHelper::map(DoanhNghiep::getListUser(), 'id', 'hoten'), ['prompt' => '- Chọn -','class' => 'form-control' , 'id' => 'doanhnghiep_nguoi_quan_li_doanh_nghiep'])->label('Người quản lí') ?>

    <?php ActiveForm::end(); ?>
</div>

<script>
    $('#doanhnghiep_nguoi_quan_li_doanh_nghiep').select2()
</script>



