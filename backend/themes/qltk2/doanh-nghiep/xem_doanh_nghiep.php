<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\QuanLyDoanhNghiep */
?>
<div class="form-control" style="height: auto; border: none">
    <div class="row">
        <div class="col-md-12">
            <h4 class="col-md-12 text-primary">Thông tin doanh nghiệp</h4>
            <div class="col-md-2"><img src="<?= \backend\models\User::getLinkAnh().$model->avatar ?>" alt="Avt doanh nghiệp" width="100px"></div>
            <div class="col-md-10">
                <div class="col-md-4"><strong>Tên doanh nghiệp: </strong> <?= $model->name  ?></div>
                <div class="col-md-4"><strong>Người đại diện: </strong> <?= $model->nguoi_dai_dien  ?></div>
                <div class="col-md-4"><strong>Điện thoại: </strong> <?= $model->dien_thoai  ?></div>
                <div class="col-md-4"><strong>Email: </strong> <?= $model->email  ?></div>
                <div class="col-md-4"><strong>Mã số thuế: </strong> <?= $model->ma_so_thue  ?></div>
                <div class="col-md-4"><strong>Địa chỉ: </strong> <?= $model->dia_chi  ?></div>
                <div class="col-md-4"><strong>Trạng thái: </strong> <?= \backend\models\DoanhNghiep::getListTrangThaiDisplay()[$model->trang_thai]?></div>
                <div class="col-md-4"><strong>Người quản lý: </strong> <?= $model->nguoi_quan_ly_doanh_nghiep ?></div>
                <div class="col-md-4"><strong>Trạng thái người quản lý: </strong> <?= \backend\models\User::getListUserDisplay()[$model->trang_thai_nguoi_quan_ly] ?></div>
            </div>
        </div>
    </div>
</div>