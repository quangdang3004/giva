<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\YeuCauXacThuc */
?>
<div class="yeu-cau-xac-thuc-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
