<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\YeuCauXacThuc */

?>
<div class="yeu-cau-xac-thuc-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
