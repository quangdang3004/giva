<?php
/** @var $model \backend\models\YeuCauXacThuc */

/** @var $link_anh \backend\models\CauHinh */

use backend\models\YeuCauXacThuc;
use backend\models\CauHinh;
use \yii\helpers\VarDumper;

?>
<?php $link = $link_anh->value; ?>
<div class="form-control" style="height: auto; border: none ">
    <a type="button" class="" data-toggle="collapse" data-target="#anh-yeu-cau">
        <h4 class="text-primary">Ảnh giấy tờ</h4>
    </a>
    <div class="collapse in" id="anh-yeu-cau">
        <div class="anh-yeu-cau-xac-thuc row">
            <?php foreach ($model->giayToLienKets as $item) : ?>
                <div class="col-md-2" style="width: 20%">
                    <a href="<?= $link . $item->link; ?>"><img width="150" src="<?= $link . $item->link; ?>"></a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

    <div class="text-primary">
        <h4>Thông tin doanh nghiệp</h4>
    </div>
    <div class="row">
        <div class="col-md-4">
            <span><strong>Tên doanh nghiệp: </strong><?= $model->doanhNghiep->name ?></span>
        </div>
        <div class="col-md-4">
            <span><strong>Người đại diện: </strong><?= $model->doanhNghiep->nguoi_dai_dien ?></span>
        </div>
        <div class="col-md-4">
            <span><strong>Số điện thoại: </strong><?= $model->doanhNghiep->dien_thoai ?> </span>
        </div>
        <div class="col-md-4">
            <span><strong>Mã số thuế: </strong><?= $model->doanhNghiep->ma_so_thue ?></span>
        </div>
        <div class="col-md-4">
            <span><strong>Email: </strong><?= $model->doanhNghiep->email ?></span>
        </div>

        <div class="col-md-4">
            <span><strong>Trạng thái: </strong>
            <?php if ($model->doanhNghiep->trang_thai == 'Chưa xác minh') : ?>
                <span class="text-danger"><?= $model->doanhNghiep->trang_thai ?></span>
            <?php endif; ?>

                <?php if ($model->doanhNghiep->trang_thai == 'Đang xác minh') : ?>
                    <span style="color: orange"><?= $model->doanhNghiep->trang_thai ?></span>
                <?php endif; ?>

                <?php if ($model->doanhNghiep->trang_thai == 'Đã xác minh') : ?>
                    <span class="text-success"><?= $model->doanhNghiep->trang_thai ?></span>
                <?php endif; ?>
            </span>
        </div>
        <div class="col-md-4">
            <span><strong>Địa chỉ: </strong><?= $model->doanhNghiep->dia_chi ?></span>
        </div>
    </div>

</div>
