<?php

use backend\models\QuanLyYeuCauXacThuc;
use yii\helpers\Url;

/* @var $searchModel backend\models\search\QuanLyYeuCauXacThucSearch */
/* @var $data backend\models\QuanLyYeuCauXacThuc */
/* @var $doituong */
return [
//    [
//        'class' => 'kartik\grid\CheckboxColumn',
//        'width' => '1%',
//    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '1%',
        'header' => 'STT',
        'headerOptions' => ['class' => 'text-primary']
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'tu_khoa',
        'header' => 'Tên',
        'headerOptions' => ['class' => 'text-primary text-nowrap'],
        'value' => function ($data) {
            /** @var $data \backend\models\search\QuanLyYeuCauXacThucSearch */
            return $data->type == \backend\models\YeuCauXacThuc::CA_NHAN ? $data->ten_user : $data->ten_doanh_nghiep;
        },
        'filter' => \yii\helpers\Html::activeInput('text', $searchModel, (($doituong == 'ca_nhan') ? 'ten_user' : 'ten_doanh_nghiep'), ['class' => 'form-control']),
        'format' => 'raw'
    ],

    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'type',
        'width' => '1%',
        'header' => 'Đối tượng xác thực',
        'headerOptions' => ['class' => 'text-primary text-nowrap'],

    ],

//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'user_id',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'cong_ty_id',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'doanh_nghiep_id',
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'trang_thai',
        'header' => 'Trạng thái',
        'width' => '10%',
        'headerOptions' => ['class' => 'text-primary text-nowrap  '],
        'contentOptions' => ['class' => 'text-nowrap'],
        'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'trang_thai', QuanLyYeuCauXacThuc::getListTrangThai(),
            ['class' => 'form-control', 'prompt' => '-- Chọn --']),
        'value' => function ($data) {
            /** @var $data \backend\models\QuanLyYeuCauXacThuc */
            return $data->trang_thai ? '<b>'.QuanLyYeuCauXacThuc::getListTrangThaiDisplay()[$data->trang_thai].'</b>'
                : '<i>Đang cập nhật</i>';
        },
        'format' => 'raw'
    ],
    [//
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'created',
        'header' => 'Ngày yêu cầu',
        'width' => '1%',
        'headerOptions' => ['class' => 'text-primary text-nowrap  '],
        'contentOptions' => ['class' => 'text-nowrap'],
        'filter' => \common\models\myAPI::activeDateFieldNoLabel($searchModel, 'created', '', ['class' => 'date form-control created-date-field', 'autocomplete' => 'off']),
        'value' => function ($data) {
            return date('d/m/Y H:i:s', strtotime($data->created));
        },
    ],

    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'ghi_chu',
        'header' => 'Ghi chú',
        'width' => '1%',
        'headerOptions' => ['class' => 'text-primary   text-nowrap'],
        'contentOptions' => ['class' => 'text-nowrap'],

    ],
    [
        'value' => function ($data) {
            /** @var $data \backend\models\search\QuanLyYeuCauXacThucSearch */
            return \yii\bootstrap\Html::button(($data->trang_thai == QuanLyYeuCauXacThuc::CHUA_XAC_MINH || $data->trang_thai == QuanLyYeuCauXacThuc::DANG_XAC_MINH)? '<i class="fa fa-spinner" aria-hidden="true"></i>' : '<i class="fa fa-eye"><span class="badge hidden"> </span></i>',
                ['class' => ($data->trang_thai == QuanLyYeuCauXacThuc::CHUA_XAC_MINH || $data->trang_thai == QuanLyYeuCauXacThuc::DANG_XAC_MINH)? 'text-gray btn btn-warning btn-xem-chi-tiet' : 'text-gray btn btn-primary btn-xem-chi-tiet', 'type' => 'button',
                    'data-value' => $data->id, 'data-active' => $data->trang_thai]);
        },
        'header' => 'Xem',
        'format' => 'raw',
        'contentOptions' => ['class' => ' ', 'style' => 'width:3%;'],
        'headerOptions' => ['width' => '1%', 'class' => 'text-center  text-primary btn-sua', 'rowspan' => 2,
            'style' => 'vertical-align: inherit;'], //
    ],

    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'updated',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'user_created_id',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'active',
    // ],
//    [
//        'value' => function($data){
//            return $data->trang_thai != \backend\models\YeuCauXacThuc::DA_XAC_MINH && $data->trang_thai != \backend\models\YeuCauXacThuc::TU_CHOI ? \yii\helpers\Html::a('<i class="fa fa-check-square-o" aria-hidden="true"></i>','' , ['class' => 'text-gray btn-xac-minh-yeu-cau', 'data-value'=>$data->id]) : '' ;
//        },
//        'label' => 'Đồng ý',
//        'format' => 'raw',
//        'contentOptions' => ['class' => ' ','style'=>'width:3%;'],
//        'headerOptions' => ['class' => '  text-primary text-nowrap','style'=>'width:3%;']
//    ],
//    [
//        'value' => function($data){
//            return $data->trang_thai != \backend\models\YeuCauXacThuc::TU_CHOI && $data->trang_thai != \backend\models\YeuCauXacThuc::DA_XAC_MINH ? \yii\helpers\Html::a('<i class="fa fa-window-close-o" aria-hidden="true"></i>',
//                '' , ['class' => 'text-gray btn-huy-yeu-cau text-danger', 'data-value'=>$data->id]) : '';
//        },
//        'label' => 'Hủy',
//        'format' => 'raw',
//        'contentOptions' => ['class' => ' ','style'=>'width:3%;'],
//        'headerOptions' => ['class' => '  text-primary','style'=>'width:3%;']
//    ],

];   