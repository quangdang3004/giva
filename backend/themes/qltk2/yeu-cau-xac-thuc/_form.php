<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\YeuCauXacThuc */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="yeu-cau-xac-thuc-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'type')->dropDownList([ 'Doanh nghiệp' => 'Doanh nghiệp', 'Ứng viên' => 'Ứng viên', 'Công ty' => 'Công ty', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'cong_ty_id')->textInput() ?>

    <?= $form->field($model, 'doanh_nghiep_id')->textInput() ?>

    <?= $form->field($model, 'trang_thai')->dropDownList([ 'Đang xác minh' => 'Đang xác minh', 'Đã xác minh' => 'Đã xác minh', 'Từ chối' => 'Từ chối', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'ghi_chu')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created')->textInput() ?>

    <?= $form->field($model, 'updated')->textInput() ?>

    <?= $form->field($model, 'user_created_id')->textInput() ?>

    <?= $form->field($model, 'active')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
