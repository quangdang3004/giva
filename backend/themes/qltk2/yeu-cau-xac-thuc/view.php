<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\YeuCauXacThuc */
?>
<div class="yeu-cau-xac-thuc-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'type',
            'user_id',
            'cong_ty_id',
            'doanh_nghiep_id',
            'trang_thai',
            'ghi_chu',
            'created',
            'updated',
            'user_created_id',
            'active',
        ],
    ]) ?>

</div>
