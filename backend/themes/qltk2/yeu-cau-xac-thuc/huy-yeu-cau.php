<?php
/** @var $model \backend\models\YeuCauXacThuc */

?>

<div class="form-control" style="min-height: 200px; border: none">
    <form id="form-huy-yeu-cau">
        <h4 class="text-primary">Lí do từ chối</h4>
        <textarea name="ghi_chu_value" id="ghi_chu" cols="30" rows="6" class="form-control"></textarea>
    </form>
</div>
