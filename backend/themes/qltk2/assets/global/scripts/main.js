/**
 * Home Land by HungLuongHien on 6/23/2016.
 */
//
function getMessage(str) {
    return str.replace('Internal Server Error (#500):','');
}
function createTypeHead(target, action, callbackAfterSelect){
    $(target).typeahead({
        source: function (query, process) {
            var states = [];
            return $.get('index.php?r=autocomplete/'+action, { query: query }, function (data) {
                $.each(data, function (i, state) {
                    states.push(state.name);
                });
                return process(states);
            }, 'json');
        },
        afterSelect: function (item) {
            if(typeof callbackAfterSelect != 'undefined')
                callbackAfterSelect(item);
            /*$.ajax({
             url: 'index.php?r=khachhang/getdiachi',
             data: {name: item},
             type: 'post',1
             dataType: 'json',
             success: function (data) {
             $("#diachikhachhang").val(data);
             }
             })*/
        }
    });
}
function setDatePicker() {
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy',
        language: 'vi',
        todayBtn: true,
        todayHighlight: true,
    });
}
function uniqId() {
    return Math.round(new Date().getTime() + (Math.random() * 100));
}

function SaveObjectUploadFile($url_controller_action, $dataInput, callbackSuccess, columnClass){
    if(typeof columnClass == "undefined")
        columnClass = 's';
    // data = new FormData($(modalForm)[0]);
    $.dialog({
        columnClass: columnClass,
        content: function () {
            var self = this;
            return $.ajax({
                url: 'index.php?r=' + $url_controller_action,
                type: 'post',
                dataType: 'json',
                data: $dataInput,
                // async: false,
                contentType: false,
                // cache: false,
                processData: false
            }).success(function (data) {
                self.setContent(data.content);
                self.setTitle(data.title);
                self.setType('blue');
                callbackSuccess(data);
            }).error(function (r1, r2) {
                self.setContent(getMessage(r1.responseText));
                self.setTitle('Lỗi');
                self.setType('red');
                return false;
            });
        }
    })
}


function SaveObject($url_controller_action, $dataInput, callbackSuccess, columnClass){
    if(typeof columnClass == "undefined")
        columnClass = 's';

    $.dialog({
        columnClass: columnClass,
        content: function () {
            var self = this;
            return $.ajax({
                url: 'index.php?r=' + $url_controller_action,
                type: 'post',
                dataType: 'json',
                data: $dataInput,
            }).success(function (data) {
                self.setContent(data.content);
                self.setTitle(data.title);
                self.setType('blue');
                callbackSuccess(data);
            }).error(function (r1, r2) {
                self.setContent(getMessage(r1.responseText));
                self.setTitle('Lỗi');
                self.setType('red');
                return false;
            });
        }
    })
}
function SaveObjectTrangThai($url_controller_action, $dataInput, callbackSuccess, columnClass){
    if(typeof columnClass == "undefined")
        columnClass = 's';

    $.dialog({
        columnClass: columnClass,
        content: function () {
            var self = this;
            return $.ajax({
                url: 'index.php?r=' + $url_controller_action,
                type: 'post',
                dataType: 'json',
                data: $dataInput,
            }).success(function (data) {
                callbackSuccess(data);
            }).error(function (r1, r2) {
                return false;
            });
        }
    })
}
function loadForm($dataInput, $size = 'm', callbackSuccess, callbackSave) {
    $.confirm({
        content: function () {
            var self = this;
            return $.ajax({
                url: 'index.php?r=site/loadform',
                data: $dataInput,
                type: 'post',
                dataType: 'json'
            }).success(function (data) {
                self.setContent(data.content);
                self.setTitle(data.title);
                self.setType('blue');
                callbackSuccess(data);
            }).error(function (r1, r2) {
                self.setContent(getMessage(r1.responseText));
                self.setTitle('Lỗi');
                self.setType('red');
                self.$$btnSave.prop('disabled', true);
                return false;
            });
        },
        columnClass: $size,
        buttons: {
            btnClose: {
                text: '<i class="fa fa-close"></i> Đóng lại',
                btnClass: 'btn-secondary '
            },
        }
    });
}
function loadFormNew($dataInput, $size = 'm', callbackSuccess, callbackSave) {
    $.confirm({
        content: function () {
            var self = this;
            return $.ajax({
                url: 'index.php?r=site/loadform',
                data: $dataInput,
                type: 'post',
                dataType: 'json'
            }).success(function (data) {
                self.setContent(data.content);
                self.setTitle(data.title);
                self.setType('blue');
                callbackSuccess(data);
            }).error(function (r1, r2) {
                self.setContent(getMessage(r1.responseText));
                self.setTitle('Lỗi');
                self.setType('red');
                self.$$btnSave.prop('disabled', true);
                return false;
            });
        },
        columnClass: $size,
        buttons: {
            confirm:  {
                text: '<i class="fa fa-check"></i> Đồng ý ',
                btnClass: 'btn-primary',
                action: function () {
                    $.ajax({
                        url: 'index.php?r=yeu-cau-xac-thuc/xac-minh-yeu-cau',
                        data: { id: $dataInput['id'] },
                        dataType: 'json',
                        type: 'post',
                        beforeSend: function () {
                        },
                        success: function (data) {

                            $.alert(data.content)

                            setTimeout( function (){
                                window.location.reload()
                            },2*1000)
                        },
                        error: function (r1, r2) {
                            $('.thongbao').html(r1.responseText)
                        },
                    })
                }
            },

            somethingElse: {
                text: '<i class="fa fa-ban" aria-hidden="true"></i> Hủy ',
                btnClass: 'btn-danger',
                action: function () {
                    const id = $dataInput['id'];
                    loadForm2({type: 'huy-yeu-cau-xac-thuc', id: $dataInput['id']}, 'm', function (data){

                    }, function (){
                        $.ajax({
                            url: 'index.php?r=yeu-cau-xac-thuc/huy-yeu-cau-xac-thuc',
                            data: {
                                ghi_chu: $('#form-huy-yeu-cau').serializeArray(),
                                id: id,
                            },
                            dataType: 'json',
                            type: 'post',
                            beforeSend: function () {
                            },
                            success: function (data) {

                                $.alert(data.content)

                                setTimeout( function (){
                                    window.location.reload()
                                },1000)
                            },
                            error: function (r1, r2) {
                                $('.thongbao').html(r1.responseText)
                            },
                        })
                    })
                }
            },
            // btnAccept: {
            //     text: '<i class="fa fa-check"></i> Đồng ý ',
            //     btnClass: 'btn-primary',
            //
            // },
            // btnRefuse: {
            //     text: '<i class="fa fa-ban" aria-hidden="true"></i> Hủy ',
            //     btnClass: 'btn-danger',
            //
            // },
            btnClose: {
                text: '<i class="fa fa-close"></i> Đóng lại',
                btnClass: 'btn-secondary '
            },
        }
    });
}
function loadForm4($dataInput, $size = 'm', callbackSuccess, callbackSave,callbackClose) {
    $.confirm({
        content: function () {
            var self = this;
            return $.ajax({
                url: 'index.php?r=site/loadform',
                data: $dataInput,
                type: 'post',
                dataType: 'json'
            }).success(function (data) {
                self.setContent(data.content);
                self.setTitle(data.title);
                self.setType('blue');
                callbackSuccess(data);
            }).error(function (r1, r2) {
                self.setContent(getMessage(r1.responseText));
                self.setTitle('Lỗi');
                self.setType('red');
                self.$$btnSave.prop('disabled', true);
                return false;
            });
        },
        columnClass: $size,
        buttons: {
            btnSave: {
                text: '<i class="fa fa-save"></i> Lưu lại',
                btnClass: 'btn-primary',
                action: function () {
                    if(typeof callbackSave != "undefined") return callbackSave();
                }
            },
            btnClose: {
                text: '<i class="fa fa-close"></i> Đóng lại',
                action: function () {
                    if(typeof callbackClose != "undefined") return callbackClose();
                }
            }
        }
    });
}
function loadForm1($dataInput, $size = 'm', callbackSuccess, callbackSave) {
    $.confirm({
        content: function () {
            var self = this;
            return $.ajax({
                url: 'index.php?r=site/loadform',
                data: $dataInput,
                type: 'post',
                dataType: 'json'
            }).success(function (data) {
                self.setContent(data.content);
                self.setTitle(data.title);
                self.setType('blue');
                callbackSuccess(data);
            }).error(function (r1, r2) {
                self.setContent(getMessage(r1.responseText));
                self.setTitle('Lỗi');
                self.setType('red');
                self.$$btnSave.prop('disabled', true);
                return false;
            });
        },
        columnClass: $size,
        buttons: {
            btnSave: {
                text: '<i class="fa fa-save"></i> Lưu',
                btnClass: 'btn-primary',
                action: function () {
                    if(typeof callbackSave != "undefined") return callbackSave();
                }

            },
            btnClose: {
                text: '<i class="fa fa-close"></i> Đóng lại'
            }
        }
    });
}

function loadFormDanhMuc($dataInput, $size = 'm', callbackSuccess, callbackSave) {
    $.confirm({
        content: function () {
            var self = this;
            return $.ajax({
                url: 'index.php?r=site/loadform',
                data: $dataInput,
                type: 'post',
                dataType: 'json'
            }).success(function (data) {
                self.setContent(data.content);
                self.setTitle(data.title);
                self.setType('blue');
                callbackSuccess(data);
            }).error(function (r1, r2) {
                self.setContent(getMessage(r1.responseText));
                self.setTitle('Lỗi');
                self.setType('red');
                self.$$btnSave.prop('disabled', true);
                return false;
            });
        },
        columnClass: $size,
        buttons: {
            btnSave: {
                text: '<i class="fa fa-save"></i> Lưu lại',
                btnClass: 'btn-primary btn-them-moi',
                isDisabled: true,
                action: function () {
                    if(typeof callbackSave != "undefined") return callbackSave();
                }

            },
            btnClose: {
                btnClass: 'btn btn-gray',
                text: '<i class="fa fa-close"></i> Đóng lại'
            }
        }
    });
}

function loadFormDoanhNghiep($dataInput, $size = 'm', callbackSuccess, callbackSave) {
    $.confirm({
        content: function () {
            var self = this;
            return $.ajax({
                url: 'index.php?r=site/loadform',
                data: $dataInput,
                type: 'post',
                dataType: 'json'
            }).success(function (data) {
                self.setContent(data.content);
                self.setTitle(data.title);
                self.setType('blue');
                callbackSuccess(data);
            }).error(function (r1, r2) {
                self.setContent(getMessage(r1.responseText));
                self.setTitle('Lỗi');
                self.setType('red');
                self.$$btnSave.prop('disabled', true);
                return false;
            });
        },
        columnClass: $size,
        buttons: {
            btnSave: {
                text: '<i class="fa fa-save"> Lưu lại</i>',
                btnClass: 'btn-primary btn-them-doanh-nghiep',
                action: function () {
                    if(typeof callbackSave != "undefined") return callbackSave();
                }

            },
            btnClose: {
                text: '<i class="fa fa-close"></i> Đóng lại'
            }
        }
    });
}
function loadFormBanner($dataInput, $size = 'm', callbackSuccess, callbackSave, textBtnAccept = '<i class="fa fa-save"></i> Lưu lại') {

    $.confirm({

        content: function () {

            var self = this;

            return $.ajax({

                url: 'index.php?r=site/loadform',

                data: $dataInput,

                type: 'post',

                dataType: 'json'

            }).success(function (data) {

                self.setContent(data.content);

                self.setTitle(data.title);

                self.setType('blue');

                callbackSuccess(data);

            }).error(function (r1, r2) {

                self.setContent(getMessage(r1.responseText));

                self.setTitle('Lỗi');

                self.setType('red');

                self.$$btnSave.prop('disabled', true);

                return false;

            });

        },

        columnClass: $size,

        buttons: {

            btnSave: {

                text: textBtnAccept,

                btnClass: 'btn-primary btn-them-banner',
                isDisabled: true,

                action: function () {

                    if(typeof callbackSave != "undefined") return callbackSave();

                }

            },

            btnClose: {

                text: '<i class="fa fa-close"></i> Đóng lại'

            }

        }

    });

}
function loadForm2($dataInput, $size = 'm', callbackSuccess, callbackSave, textBtnAccept = '<i class="fa fa-save"></i> Lưu lại') {

    $.confirm({

        content: function () {

            var self = this;

            return $.ajax({

                url: 'index.php?r=site/loadform',

                data: $dataInput,

                type: 'post',

                dataType: 'json'

            }).success(function (data) {

                self.setContent(data.content);

                self.setTitle(data.title);

                self.setType('blue');

                callbackSuccess(data);

            }).error(function (r1, r2) {

                self.setContent(getMessage(r1.responseText));

                self.setTitle('Lỗi');

                self.setType('red');

                self.$$btnSave.prop('disabled', true);

                return false;

            });

        },

        columnClass: $size,

        buttons: {

            btnSave: {

                text: textBtnAccept,

                btnClass: 'btn-primary',

                action: function () {

                    if(typeof callbackSave != "undefined") return callbackSave();

                }

            },

            btnClose: {

                text: '<i class="fa fa-close"></i> Đóng lại'

            }

        }

    });

}

function loadFormNguoiPhuTrach($dataInput, $size = 'm', callbackSuccess, callbackSave) {
    $.confirm({
        content: function () {
            var self = this;
            return $.ajax({
                url: 'index.php?r=site/loadformnguoiphutrach',
                data: $dataInput,
                type: 'post',
                dataType: 'json'
            }).success(function (data) {
                self.setContent(data.content);
                self.setTitle(data.title);
                self.setType('blue');
                callbackSuccess(data);
            }).error(function (r1, r2) {
                self.setContent(getMessage(r1.responseText));
                self.setTitle('Lỗi');
                self.setType('red');
                self.$$btnSave.prop('disabled', true);
                return false;
            });
        },
        columnClass: $size,
        buttons: {
            btnSave: {
                text: '<i class="fa fa-save"></i> Lưu lại',
                btnClass: 'btn-primary',
                action: function () {
                    if(typeof callbackSave != "undefined") return callbackSave();
                }
            },
            btnClose: {
                text: '<i class="fa fa-close"></i> Đóng lại'
            }
        }
    });
}

function loadFormTrangThai($dataInput, $size = 'm', callbackSuccess, callbackSave) {
    $.confirm({
        content: function () {
            var self = this;
            return $.ajax({
                url: 'index.php?r=site/loadformtrangthai',
                data: $dataInput,
                type: 'post',
                dataType: 'json'
            }).success(function (data) {
                self.setContent(data.content);
                self.setTitle(data.title);
                self.setType('blue');
                callbackSuccess(data);
            }).error(function (r1, r2) {
                self.setContent(getMessage(r1.responseText));
                self.setTitle('Lỗi');
                self.setType('red');
                self.$$btnSave.prop('disabled', true);
                return false;
            });
        },
        columnClass: $size,
        buttons: {
            btnSave: {
                text: '<i class="fa fa-save"></i> Lưu lại',
                btnClass: 'btn-primary',
                action: function () {
                    if(typeof callbackSave != "undefined") return callbackSave();
                }
            },
            btnClose: {
                text: '<i class="fa fa-close"></i> Đóng lại'
            }
        }
    });
}

function loadFormThuPhi($dataInput, $size = 'm', callbackSuccess, callbackSave) {
    $.confirm({
        content: function () {
            var self = this;
            return $.ajax({
                url: 'index.php?r=site/loadformthuphi',
                data: $dataInput,
                type: 'post',
                dataType: 'json'
            }).success(function (data) {
                self.setContent(data.content);
                self.setTitle(data.title);
                self.setType('blue');
                callbackSuccess(data);
            }).error(function (r1, r2) {
                self.setContent(getMessage(r1.responseText));
                self.setTitle('Lỗi');
                self.setType('red');
                self.$$btnSave.prop('disabled', true);
                return false;
            });
        },
        columnClass: $size,
        buttons: {
            btnSave: {
                text: '<i class="fa fa-save"></i> Lưu lại',
                btnClass: 'btn-primary',
                action: function () {
                    if(typeof callbackSave != "undefined") return callbackSave();
                }
            },
            btnClose: {
                text: '<i class="fa fa-close"></i> Đóng lại'
            }
        }
    });
}
function loadFormFromUrl($dataInput, $controller_action, $size = 'm', callbackSuccess, callbackSave) {
    $.confirm({
        content: function () {
            var self = this;
            return $.ajax({
                url: 'index.php?r=' + $controller_action,
                data: $dataInput,
                type: 'post',
                dataType: 'json'
            }).success(function (data) {
                self.setContent(data.content);
                self.setTitle(data.title);
                self.setType('blue');
                callbackSuccess(data);
            }).error(function (r1, r2) {
                self.setContent(getMessage(r1.responseText));
                self.setTitle('Lỗi');
                self.setType('red');
                self.$$btnSave.prop('disabled', true);
                return false;
            });
        },
        columnClass: $size,
        buttons: {
            btnSave: {
                text: '<i class="fa fa-save"></i> Lưu lại',
                btnClass: 'btn-primary',
                action: function () {
                    if(typeof callbackSave != "undefined") return callbackSave();
                }
            },
            btnClose: {
                text: '<i class="fa fa-close"></i> Đóng lại'
            }
        }
    });
}
function loadFormThayTheNguoiPhuTrach($dataInput, $size = 'm', callbackSuccess, callbackSave) {
    $.confirm({
        content: function () {
            var self = this;
            return $.ajax({
                url: 'index.php?r=site/loadformthaythenguoiphutrach',
                data: $dataInput,
                type: 'post',
                dataType: 'json'
            }).success(function (data) {
                self.setContent(data.content);
                self.setTitle(data.title);
                self.setType('blue');
                callbackSuccess(data);
            }).error(function (r1, r2) {
                self.setContent(getMessage(r1.responseText));
                self.setTitle('Lỗi');
                self.setType('red');
                self.$$btnSave.prop('disabled', true);
                return false;
            });
        },
        columnClass: $size,
        buttons: {
            btnSave: {
                text: '<i class="fa fa-save"></i> Lưu lại',
                btnClass: 'btn-primary',
                action: function () {
                    if(typeof callbackSave != "undefined") return callbackSave();
                }
            },
            btnClose: {
                text: '<i class="fa fa-close"></i> Đóng lại'
            }
        }
    });
}
function loadFormKinhDoanhHoiVien($dataInput, $size = 'm', callbackSuccess, callbackSave) {
    $.confirm({
        content: function () {
            var self = this;
            return $.ajax({
                url: 'index.php?r=site/loadformkinhdoanhhoivien',
                data: $dataInput,
                type: 'post',
                dataType: 'json'
            }).success(function (data) {
                self.setContent(data.content);
                self.setTitle(data.title);
                self.setType('blue');
                callbackSuccess(data);
            }).error(function (r1, r2) {
                self.setContent(getMessage(r1.responseText));
                self.setTitle('Lỗi');
                self.setType('red');
                self.$$btnSave.prop('disabled', true);
                return false;
            });
        },
        columnClass: $size,
        buttons: {
            btnSave: {
                text: '<i class="fa fa-save"></i> Lưu lại',
                btnClass: 'btn-primary',
                action: function () {
                    if(typeof callbackSave != "undefined") return callbackSave();
                }
            },
            btnClose: {
                text: '<i class="fa fa-close"></i> Đóng lại'
            }
        }
    });
}
function loadFormCongXu($dataInput, $size = 'm', callbackSuccess, callbackSave) {
    $.confirm({
        content: function () {
            var self = this;
            return $.ajax({
                url: 'index.php?r=site/loadform',
                data: $dataInput,
                type: 'post',
                dataType: 'json'
            }).success(function (data) {
                self.setContent(data.content);
                self.setTitle(data.title);
                self.setType('blue');
                callbackSuccess(data);
            }).error(function (r1, r2) {
                self.setContent(getMessage(r1.responseText));
                self.setTitle('Lỗi');
                self.setType('red');
                self.$$btnSave.prop('disabled', true);

                return false;
            });

        },

        columnClass: $size,

        buttons: {
            somethingElse: {
                text: '<i class="fa fa-plus"></i> Cộng xu',
                btnClass: 'btn-primary btn-cong-xu',
                action: function () {
                    const id = $dataInput['id'];
                    loadForm1({type: 'cong-xu-nha-tuyen-dung', id: $dataInput['id']}, 'm', function (data){
                    }, function (){
                        $.ajax({
                            url: 'index.php?r=user/cong-xu',
                            data: {
                                so_xu: $('#so-xu-nhap-vao').val(),
                                id: id,
                            },
                            dataType: 'json',
                            type: 'post',
                            beforeSend: function () {
                            },
                            success: function (data) {

                                alertify.success(data.content)
                                setTimeout( function (){
                                    window.location.reload()
                                },1000)

                            },
                            error: function (r1, r2) {
                                console.log(r1)
                               alertify.error(r1.responseText)
                            },
                        })
                    })
                },
            },

            btnClose: {
                text: '<i class="fa fa-close"></i> Đóng lại'
            }

        },
        onContentReady: function () {
            // bind to events
            var jc = this;
            this.$content.find('somethingElse').on('click', function (e) {
                // if the user submits the form by pressing enter in the field.
                e.preventDefault();
                jc.$$formSubmit.trigger('click'); // reference the button and click it
            });
        }

    });

}

function taiFileExcel($controller_action, $data){
    $.ajax({
        url: 'index.php?r='+$controller_action,
        data: $data,
        dataType: 'json',
        type: 'post',
        beforeSend: function () {
            $('.thongbao').html('');
            Metronic.blockUI();
        },
        success: function (data) {
            $.dialog({
                title: data.title,
                content: data.link_file,
            });
        },
        complete: function () {
            Metronic.unblockUI();
        },
        error: function (r1, r2) {
            $('.thongbao').html(r1.responseText);
        }
    });
}
function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}
function viewData($controller_action, $dataInput, $size, callbackSuccess){
    $.confirm({
        content: function () {
            var self = this;
            return $.ajax({
                url: 'index.php?r=' + $controller_action,
                data: $dataInput,
                type: 'post',
                dataType: 'json'
            }).success(function (data) {
                self.setContent(data.content);
                self.setTitle(data.title);
                self.setType('blue');
                if(typeof callbackSuccess != "undefined")
                    callbackSuccess(data);
            }).error(function (r1, r2) {
                self.setContent(getMessage(r1.responseText));
                self.setTitle('Lỗi');
                self.setType('red');
                return false;
            });
        },
        columnClass: $size,
        buttons: {
            btnClose: {
                text: '<i class="fa fa-close"></i> Đóng lại'
            }
        }
    });
}
function viewDataGet($controller_action, $dataInput, $size, callbackSuccess){
    $.confirm({
        content: function () {
            var self = this;
            return $.ajax({
                url: 'index.php?r=' + $controller_action + '&id='+ $dataInput,
                data: $dataInput,
                type: 'get',
                dataType: 'json'
            }).success(function (data) {
                self.setContent(data.content);
                self.setTitle(data.title);
                self.setType('blue');
                if(typeof callbackSuccess != "undefined")
                    callbackSuccess(data);
            }).error(function (r1, r2) {
                self.setContent(getMessage(r1.responseText));
                self.setTitle('Lỗi');
                self.setType('red');
                return false;
            });
        },
        columnClass: $size,
        buttons: {
            btnClose: {
                text: '<i class="fa fa-close"></i> Đóng lại'
            }
        }
    });
}

function viewDataNew($controller_action, $dataInput, $size, callbackSuccess){
    $.confirm({
        content: function () {
            var self = this;
            return $.ajax({
                url: 'index.php?r=' + $controller_action,
                data: $dataInput,
                type: 'post',
                dataType: 'json'
            }).success(function (data) {
                self.setContent(data.content);
                self.setTitle(data.title);
                self.setType('blue');
                if(typeof callbackSuccess != "undefined")
                    callbackSuccess(data);
            }).error(function (r1, r2) {
                self.setContent(getMessage(r1.responseText));
                self.setTitle('Lỗi');
                self.setType('red');
                return false;
            });
        },
        columnClass: $size,
        buttons: {
            confirm: {
                text: '<i class="fa fa-edit"></i> Cập nhật',
                isDisabled: true,
                action: function(){
                    $.ajax({
                        url: 'index.php?r=doanh-nghiep/update-chi-tiet-doanh-nghiep',
                        data: $('#form-update-doanh-nghiep').serializeArray(),
                        type: 'post',
                        dataType: 'json',
                        beforeSend: function () {
                        },
                        success: function (data) {

                           alertify.success(data.content)

                            setTimeout(function () {
                                window.location.reload()
                            },  1000)
                        },
                        error: function (r1, r2) {
                            console.log(r1)
                          alertify.error(r1.responseText)
                        },
                    })
                },
                btnClass: 'btn-primary btn-luu-doanh-nghiep'
            },
            cancel: {
                text: '<i class="fa fa-close"></i> Đóng lại',
            },
        }
    });
}
function viewDataDel($controller_action, $dataInput, $size, callbackSuccess){
    $.confirm({
        title:"Thông báo",
        // text: 'Bạn chắc chắn muốn xóa',
        content: function () {
            var self = this;
            return 'Bạn chắc chắn muốn xóa nội dung này?';
            // return $.ajax({
            //     url: 'index.php?r=' + $controller_action,
            //     data: $dataInput,
            //     type: 'post',
            //     dataType: 'json'
            // }).success(function (data) {
            //     self.setContent(data.content);
            //     self.setTitle(data.title);
            //     self.setType('blue');
            //     if(typeof callbackSuccess != "undefined")
            //         callbackSuccess(data);
            // }).error(function (r1, r2) {
            //     self.setContent(getMessage(r1.responseText));
            //     self.setTitle('Lỗi');
            //     self.setType('red');
            //     return false;
            // });
        },
        columnClass: $size,
        buttons: {
            confirm: {
                text: '<i class="fa fa-check"></i> Đồng ý',
                btnClass: 'btn-primary',
                action: function(){
                    return $.ajax({
                        url: 'index.php?r='+ $controller_action,
                        data: $dataInput,
                        type: 'post',
                        dataType: 'json',
                        beforeSend: function () {
                            // $('.thongbao').html('');
                            Metronic.blockUI();
                        },
                        success: function (data) {
                            alertify.success(data.content)

                            setTimeout(function () {
                                window.location.reload()
                            }, 1000)
                        },
                        error: function (r1, r2) {
                            $('.thongbao').html(r1.responseText)
                        },
                    })
                },
            },
            cancel: {
                text: '<i class="fa fa-close"></i> Hủy',
            },
        }
    });
}
function viewDataCreate($controller_action, $dataInput, $size, callbackSuccess){
    $.confirm({
        content: function () {
            var self = this;
            return $.ajax({
                url: 'index.php?r=' + $controller_action,
                data: $dataInput,
                type: 'post',
                dataType: 'json'
            }).success(function (data) {
                self.setContent(data.content);
                self.setTitle(data.title);
                self.setType('blue');
                if(typeof callbackSuccess != "undefined")
                    callbackSuccess(data);
            }).error(function (r1, r2) {
                self.setContent(getMessage(r1.responseText));
                self.setTitle('Lỗi');
                self.setType('red');
                return false;
            });
        },
        columnClass: $size,
        buttons: {
            confirm: {
                text: '<i class="fa fa-edit"></i> Lưu lại',
                btnClass: 'btn-primary',
                action: function(){
                    $.ajax({
                        url: 'index.php?r=' + $controller_action,
                        data: $('#form-tao-moi-danh-muc').serializeArray(),
                        type: 'post',
                        dataType: 'json',
                        beforeSend: function () {
                            // $('.thongbao').html('');
                            Metronic.blockUI();
                        },
                        success: function (data) {
                            $.alert({
                                title: 'Thông báo!',
                                content: data.content,
                            });

                            setTimeout(function () {
                                window.location.reload()
                            }, 2 * 1000)
                        },
                        error: function (r1, r2) {
                            $('.thongbao').html(r1.responseText)
                        },
                    })
                },
            },
            cancel: {
                text: '<i class="fa fa-close"></i> Đóng lại',
            },
        }
    });
}
function showAlert($message){
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-top-right",
        "onclick": function () {
            // if($link != '')
            //     window.location = $link
        },
        "showDuration": "10000",
        "hideDuration": "1000",
        "timeOut": "10000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    toastr['info']($message, 'Thông báo');
}
function tinhDiemMoiCaNhan($idNhanVienPhongBan){
    var $tongDiem = 0;
    $("tr.tr-nhan-vien-"+$idNhanVienPhongBan).each(function () {
        var $myParentTr = $(this).parent().parent().parent().parent();
        var $myCheckBox = $myParentTr.find('.checkChonCVQuy ');
        if($myCheckBox.is(":checked")){
            var $myInputDiem = $(this).find('td.td-diem-nhan-vien input').val();
            var $thucHien = $(this).find('td.td-thuc-hien select').val();
            $myInputDiem = ($myInputDiem == '' ? 0 : parseFloat($myInputDiem));
            if($thucHien == 1)
                $tongDiem += $myInputDiem;
        }

    });
    $("#diem-nvien-" + $idNhanVienPhongBan).text($tongDiem);
}
function tinhTongDiemDonVi(){
    var $tongDiem = 0;
    $(".diem-so-don-vi input").each(function () {
        var $diemSo = $(this).val();
        $tongDiem += ($diemSo == '' ? 0 : parseFloat($diemSo));
    });
    $("span#tong-diem").text($tongDiem);
}

jQuery(document).ready(function() {
    Metronic.init(); // init metronic core components
    Layout.init(); // init current layout
    QuickSidebar.init(); // init quick sidebar
    // $(".tien-te").maskMoney({thousands:",", allowZero:true, /*suffix: " VNĐ",*/precision:3});
    // Hiển thị thông báo các công việc đã hoàn thành nhưng chưa duyệt
    $(document).on('click', 'ul li a.hover-initialized', function (e) {
        e.preventDefault();

        var $parent = $(this).parent();
        if($parent.find('ul').length > 0){
            $parent.addClass('open');
            $(this).attr('aria-expanded', 'true');
        }
    });
});
