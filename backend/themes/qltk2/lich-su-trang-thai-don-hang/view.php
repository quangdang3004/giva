<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\LichSuTrangThaiDonHang */
?>
<div class="lich-su-trang-thai-don-hang-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'don_hang_id',
            'trang_thai',
            'so_luong',
            'created',
            'user_createad_id',
        ],
    ]) ?>

</div>
