<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\LichSuTrangThaiDonHang */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lich-su-trang-thai-don-hang-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'don_hang_id')->textInput() ?>

    <?= $form->field($model, 'trang_thai')->dropDownList([ 'Đang tuyển' => 'Đang tuyển', 'Đã đủ' => 'Đã đủ', 'Hết hạn' => 'Hết hạn', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'so_luong')->textInput() ?>

    <?= $form->field($model, 'created')->textInput() ?>

    <?= $form->field($model, 'user_createad_id')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
