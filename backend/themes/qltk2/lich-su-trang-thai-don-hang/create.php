<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\LichSuTrangThaiDonHang */

?>
<div class="lich-su-trang-thai-don-hang-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
