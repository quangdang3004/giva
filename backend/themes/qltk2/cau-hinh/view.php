<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\CauHinh */
?>
<div class="cau-hinh-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'value',
            'code',
            'active',
            'created',
        ],
    ]) ?>

</div>
