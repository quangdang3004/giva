<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CauHinh */
?>
<div class="cau-hinh-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
