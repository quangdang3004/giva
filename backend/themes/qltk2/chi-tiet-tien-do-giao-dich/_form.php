<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ChiTietTienDoGiaoDich */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="chi-tiet-tien-do-giao-dich-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'giao_dich_id')->textInput() ?>

    <?= $form->field($model, 'type_tien_do')->dropDownList([ '' => '', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'created')->textInput() ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
