<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\ChiTietTienDoGiaoDich */
?>
<div class="chi-tiet-tien-do-giao-dich-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'giao_dich_id',
            'type_tien_do',
            'created',
            'user_id',
        ],
    ]) ?>

</div>
