<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\ChiTietTienDoGiaoDich */

?>
<div class="chi-tiet-tien-do-giao-dich-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
