<?php

/** @var $congTy CongTy */

use backend\models\CongTy;

?>

<div class="row">
    <div class="col-md-4"><strong>Tên công ty: </strong><?=$congTy->name?></div>
    <div class="col-md-4"><strong>Email: </strong><?= $congTy->email?></div>
    <div class="col-md-4"><strong>Điện thoại: </strong><?=$congTy->dien_thoai?></div>
</div>
<div class="row">
    <div class="col-md-4"><strong>Logo: </strong><?=$congTy->logo != '' ? $congTy->logo : '<i>Đang cập nhật</i>'?></div>
    <div class="col-md-4"><strong>Ngày tạo công ty: </strong><?=$congTy->created != ''
            ? date('d/m/Y',strtotime($congTy->created)) : '<i>Đang cập nhật</i>'?></div>
    <div class="col-md-4"><strong>Trạng thái: </strong>
        <?= $congTy->trang_thai!='' ? CongTy::getListTrangThaiCongTyDisplay()[$congTy->trang_thai] : '<i>Đang cập nhật</i>'?>
    </div>
</div>
<div class="row">
    <div class="col-md-12"><strong>Địa chỉ: </strong><?= $congTy->dia_chi != '' ? $congTy->dia_chi : '<i>Đang cập nhật</i>'?></div>
</div>

