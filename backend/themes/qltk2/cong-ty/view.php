<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\CongTy */
?>
<div class="cong-ty-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name:ntext',
            'dia_chi:ntext',
            'dien_thoai',
            [
                'attribute' => 'logo',
                'value'=>function($data){
                    /** @var $data \backend\models\CongTy */
                    return is_null($data->logo)? '<i>Đang cập nhật</i>' : $data->logo ;
                },
                'format' => 'raw'
            ],
            'email:email',
            [
                'attribute' => 'trang_thai',
                'value'=>function($data){
                    /** @var $data \backend\models\CongTy */
                    return is_null($data->trang_thai)? '<i>Đang cập nhật</i>' : $data->trang_thai ;
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'created',
                'value'=>function($data){
                    /** @var $data \backend\models\CongTy */
                    return is_null($data->created)? '<i>Đang cập nhật</i>' : date('d/m/Y', strtotime($data->created)) ;
                },
                'format' => 'raw'
            ],

        ],
    ]) ?>

</div>
