<?php
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'dia_chi',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'dien_thoai',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'logo',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'email',
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'active',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'trang_thai',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'user_created_id',
    // ],
//    [
//        'value' => function($data){
//            return \yii\bootstrap\Html::a('<i class="fa fa-eye"></i>',Url::toRoute(['cong-ty/view','id'=>$data->id]), ['class' => 'text-gray','role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip']);
//        },
//        'label' => 'Xem',
//        'format' => 'raw',
//        'contentOptions' => ['class' => 'text-center','style'=>'width:3%;'],
//        'headerOptions' => ['class' => 'text-center','style'=>'width:3%;']
//    ],
    [
        'value' => function($data){
            /** @var $data \backend\models\CongTy */
            return \yii\bootstrap\Html::a('<i class="fa fa-eye"></i>','#', ['class' => 'text-gray btn-xem-cong-ty-2',
                'title'=>'View', 'data-value' => $data->id]);
        },
        'label' => 'Xem ',
        'format' => 'raw',
        'contentOptions' => ['class' => 'text-center','style'=>'width:3%;'],
        'headerOptions' => ['class' => 'text-center','style'=>'width:3%;']
    ],
//    [
//        'value' => function($data){
//            return \yii\bootstrap\Html::a('<i class="fa fa-edit"></i>',Url::toRoute(['cong-ty/update', 'id'=>$data->id]), ['class' => 'text-gray','role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip']);
//        },
//        'label' => 'Sửa',
//        'format' => 'raw',
//        'contentOptions' => ['class' => 'text-center','style'=>'width:3%;'],
//        'headerOptions' => ['class' => 'text-center','style'=>'width:3%;']
//    ],
    [
        'value' => function($data){
            return \yii\bootstrap\Html::a('<i class="fa fa-edit"></i>','#', ['class' => 'text-gray btn-update-2',
                'title'=>'Update', 'data-value' => $data->id]);
        },
        'label' => 'Sửa',
        'format' => 'raw',
        'contentOptions' => ['class' => 'text-center','style'=>'width:3%;'],
        'headerOptions' => ['class' => 'text-center','style'=>'width:3%;']
    ],
    [
        'value' => function($data){
            return \yii\bootstrap\Html::a('<i class="fa fa-trash"></i>',Url::toRoute(['cong-ty/delete', 'id'=>$data->id]),
                ['class' => 'text-gray','role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip']);
        },
        'label' => 'Xóa',
        'format' => 'raw',
        'contentOptions' => ['class' => 'text-center','style'=>'width:3%;'],
        'headerOptions' => ['class' => 'text-center','style'=>'width:3%;']
    ],
];   