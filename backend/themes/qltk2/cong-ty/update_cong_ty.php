<?php

use common\models\myAPI;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CongTy */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class='form'>

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['rows' => 6]) ?>

        <?= $form->field($model, 'dia_chi')->textInput(['rows' => 6]) ?>

        <?= $form->field($model, 'logo')->fileInput([]) ?>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'dien_thoai')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <?= $form->field($model, 'trang_thai')->dropDownList([ 'Chưa xác minh' => 'Chưa xác minh',
            'Đã xác minh' => 'Đã xác minh', 'Từ chối' => 'Từ chối', ], ['prompt' => '']) ?>

        <?= myAPI::activeDateField($form, $model, 'created', 'Ngày tạo')?>

        <?php if (!Yii::$app->request->isAjax){ ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>

    </div>
<?php
