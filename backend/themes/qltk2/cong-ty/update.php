<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CongTy */
?>
<div class="cong-ty-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
