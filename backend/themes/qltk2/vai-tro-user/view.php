<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Vaitrouser */
?>
<div class="vaitrouser-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'vaitro_id',
        ],
    ]) ?>

</div>
