<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Vaitrouser */

?>
<div class="vaitrouser-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
