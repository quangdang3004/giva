<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Vaitrouser */
?>
<div class="vaitrouser-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
