<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\AnhLienKetChiTietTienDo */
?>
<div class="anh-lien-ket-chi-tiet-tien-do-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
