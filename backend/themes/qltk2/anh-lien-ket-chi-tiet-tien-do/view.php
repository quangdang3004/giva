<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\AnhLienKetChiTietTienDo */
?>
<div class="anh-lien-ket-chi-tiet-tien-do-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'tien_do_id',
            'link_anh',
            'created',
        ],
    ]) ?>

</div>
