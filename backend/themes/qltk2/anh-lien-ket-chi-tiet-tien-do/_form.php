<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\AnhLienKetChiTietTienDo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="anh-lien-ket-chi-tiet-tien-do-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tien_do_id')->textInput() ?>

    <?= $form->field($model, 'link_anh')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
