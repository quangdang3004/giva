<?php

use backend\models\search\UserSearch;
use yii\helpers\Url;
use \backend\models\User;

/** @var $searchModel \backend\models\search\UserVaiTroSearch */

return [
//    [
//        'class' => 'kartik\grid\CheckboxColumn',
//        'width' => '1%',
//    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '1%',
        'header' => 'STT',
        'headerOptions' => ['class' => 'text-primary']
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'avatar',
//        'header' => 'Ảnh đại diện',
//        'width' => '1%',
//        'headerOptions' => ['class' => 'text-primary text-nowrap'],
//        'value' => function ($data){
//            return \yii\helpers\Html::img(User::getLinkAnh().$data->avatar, ['style' => 'width: 40px']);
//        },
//        'format' => 'raw',
//        'contentOptions' => ['class' => 'text-center']
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'username',
        'header' => 'Tên người dùng',
        'headerOptions' => ['class' => 'text-primary text-nowrap'],
        'value' => function ($data) {
            return  '<span class="badge badge-primary">#' . $data->id . ' </span> '. $data->username;
        },
        'format' => 'raw'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'hoten',
        'header' => 'Họ tên',
        'headerOptions' => ['class' => 'text-primary text-nowrap'],
        'contentOptions' => ['class' => 'text-nowrap'],
        'width' => '1%',
        'format' => 'raw',
    ],

//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'ten_vai_tro',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'password_hash',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'exp_token_reset',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'email',
        'width' => '1%',
        'header' => 'Email',
        'headerOptions' => ['class' => 'text-primary text-nowrap']
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'dien_thoai',
        'width' => '1%',
        'header' => 'Số điện thoại',
        'headerOptions' => ['class' => 'text-primary text-nowrap'],
        'contentOptions' => ['class' => 'text-right']
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'auth_key',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'exp_auth_key',
    // ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'so_du',
//        'width' => '1%',
//        'header' => 'Số dư tài khoản',
//        'headerOptions' => ['class' => 'text-primary text-nowrap', 'rowspan' =>2, 'style'=>'vertical-align: inherit;'],
//        'contentOptions' => ['class' => 'text-right']
//    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'created_at',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'updated_at',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'ten_vai_tro',
        'header' => 'Vai trò',
        'headerOptions' => ['class' => 'text-primary'],
        'contentOptions' => ['class' => 'text-nowrap'],
        'width' => '1%'
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'status',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'doanh_nghiep_id',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'password_reset_token',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'password',
    // ],
//    [
//        'value' => function($data){
//            /** @var $data \common\models\User  */
//            return \yii\bootstrap\Html::a('<i class="fa fa-eye"></i>',Url::toRoute(['user/view', 'id' => $data->id]),
//                ['class' => 'text-gray','role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip']);
//        },
//        'label' => 'Xem',
//        'format' => 'raw',
//        'contentOptions' => ['class' => 'text-center','style'=>'width:3%;'],
//        'headerOptions' => ['class' => 'text-center','style'=>'width:3%;']
//    ],
//    [
//        'value' => function($data){
//            /** @var $data \common\models\User  */
//            /**  */
//            return \yii\bootstrap\Html::a('<i class="fa fa-eye"></i>','#',
//                ['class' => 'text-gray btn-xem-chi-tiet-nha-tuyen-dung', 'data-value' =>$data->id]);
//        },
//        'header' => 'Xem',
//        'format' => 'raw',
//        'contentOptions' => ['class' => 'text-center','style'=>'width:3%;'],
//        'width' => '1%',
//        'headerOptions' => ['class' => 'text-center text-primary text-nowrap', 'rowspan' =>2, 'style'=>'vertical-align: inherit;']
//    ],
    [
        'value' => function($data){
            /** @var $data \common\models\User  */
            return \yii\bootstrap\Html::a('<i class="fa fa-edit"></i>',Url::toRoute(['user/update', 'id'=>$data->id]),
                ['role' => 'modal-remote', 'data-toggle' => 'tooltip','class' => 'text-gray btn-update']);
        },
        'label' => 'Sửa',
        'format' => 'raw',
        'contentOptions' => ['class' => 'text-center','style'=>'width:3%;'],
        'headerOptions' => ['class' => 'text-center text-primary text-nowrap','style'=>'width:3%; vertical-align: inherit;', 'rowspan' =>2]
    ],
    [
        'value' => function($data){
            return \yii\bootstrap\Html::a('<i class="fa fa-trash"></i>','#'
                , ['class' => 'text-danger btn-xoa-thanh-vien','data-value' => $data->id,'title'=>'Xóa', 'data-toggle'=>'tooltip',]);
        },
        'label' => 'Xóa',
        'format' => 'raw',
        'contentOptions' => ['class' => 'text-center','style'=>'width:3%;'],
        'headerOptions' => ['class' => 'text-center text-primary text-nowrap','style'=>'width:3%; vertical-align: inherit;', 'rowspan' =>2],

    ],
];