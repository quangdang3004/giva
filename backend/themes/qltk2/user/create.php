<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\User */
$this->title = 'Thêm tài khoản';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">
    <?= $this->render('_form', [
        'model' => $model,
        'vaitros' => $vaitros,
        'vaitrouser' => $vaitrouser,
    ]) ?>
</div>
