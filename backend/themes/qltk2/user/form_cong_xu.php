<?php
/** @var $model \backend\models\User */
?>
<div class="form-control" style="height: auto; border: none">
    <form id="form-cong-xu">
        <div class="row">
            <div class="col-md-12">
                <h4 style="display: inline-block; padding-right: 25px;" class="text-primary">Số xu hiện tại: </h4><?= $model->so_du ?>
            </div>
            <div class="col-md-3">
                <h4 class="text-primary">Nhập số xu: </h4>
            </div>
            <div class="col-md-9">
                <input id="so-xu-nhap-vao" class="form-control" type="number" autofocus>
            </div>
        </div>

    </form>
</div>

