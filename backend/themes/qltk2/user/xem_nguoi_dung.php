<?php
/** @var $model \backend\models\User */
/** @var $anh_chi_tiet_nguoi_dung \backend\models\AnhChiTietNguoiDung */
/** @var $giao_dich \backend\models\GiaoDich */

///** @var $congtac \backend\models\NhaTuyenDungCongTac */

use backend\models\User;

?>
<div class="form-control" style="height: auto; border: none">
    <h4 class="text-primary">Thông tin nhà tuyển dụng</h4>
    <div class="row">
        <div class="col-md-1">
            <img src="<?= User::getLinkAnh() . $model->avatar ?>" alt="Ảnh đại diện" width="100px">
        </div>
        <div class="col-md-11">
            <div class="row" style="margin-bottom: 10px;">
                <div class="col-md-6">
                    <strong>Tên người dùng: </strong> <?= $model->hoten ?>
                </div>
                <div class="col-md-6">
                    <strong>Email: </strong> <?= $model->email ?>
                </div>
                <div class="col-md-6 ">
                    <strong>Điện thoại: </strong> <?= $model->dien_thoai ?>
                </div>
                <div class="col-md-6">
                    <strong>Trạng
                        thái: </strong> <?= $model->trang_thai ? User::getListUserDisplay()[$model->trang_thai]
                        : '<i>Đang cập nhật</i>'; ?>
                </div>
                <div class="col-md-6 ">
                    <strong>Ngày tạo: </strong> <?= is_null($model->created_at) ? '<i>Đang cập nhật</i>'
                        : date('d/m/Y', strtotime($model->created_at)); ?>
                </div>
                <div class="col-md-6 ">
                    <strong>Số dư: </strong> <?= $model->so_du ?>
                </div>
            </div>
        </div>

        <div class="tabbale-line">
            <ul class="nav nav-tabs ">
                <li class="nav-item active">
                    <a href="#tab_15_1" data-toggle="tab" aria-expanded="true" style="margin-left: 3px;">ẢNH THÔNG
                        TIN</a>
                </li>
                <li class="nav-item">
                    <a href="#tab_15_2" data-toggle="tab" aria-expanded="false">LỊCH SỬ GIAO DỊCH</a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="tab_15_1">
                    <?php foreach ($anh_chi_tiet_nguoi_dung as $item) : ?>
                        <div class="col-md-3 anh-thong-tin-nguoi-dung">
                            <a href="<?= User::getLinkAnh() . $item->link ?>"><img
                                        src="<?= User::getLinkAnh() . $item->link ?>" alt="Ảnh thông tin người dùng"
                                        width="200px"></a>
                        </div>
                    <?php endforeach; ?>
                </div>

                <div class="tab-pane" id="tab_15_2">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th class="text-nowrap" width="1%">STT</th>
                            <th class="text-nowrap" width="1%">Ngày</th>
                            <th class="text-nowrap">Tên công ty</th>
                            <th class="text-nowrap" width="1%">Người cung cấp</th>
                            <th class="text-nowrap" width="1%">Loại</th>
                            <th class="text-nowrap" width="1%">Trạng thái</th>
                            <th class="text-nowrap" width="1%">Tiến độ</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (count($giao_dich) > 0): ?>
                            <?php foreach ($giao_dich as $index => $item): ?>
                                <tr>
                                    <td class="text-center"><?= $index + 1 ?></td>
                                    <td class="text-center text-nowrap">
                                        <?= $item->created ?>
                                    </td>
                                    <td class="text-left">
                                        <?= $item->donHang->congTy->name ?>
                                    </td>
                                    <td class="text-left"><?= $item->nguoiCungCap->hoten ?></td>
                                    <td class="text-left text-nowrap"><?= $item->type ?></td>
                                    <td class="text-left"><?= $item->trang_thai ?></td>
                                    <td class="text-left text-nowrap"><?= $item->tien_do ?></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

