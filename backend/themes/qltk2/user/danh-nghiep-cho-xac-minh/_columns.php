<?php
use yii\helpers\Url;
/* @var $searchModel Backend\models\search\UserSearch */
/* @var $model \backend\models\QuanLyDoanhNghiep**/
return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'header' => 'STT',
        'headerOptions' => ['class' => 'text-primary text-center', 'width' => '1%']
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'hoten',
        'label' => 'Họ tên',
        'headerOptions' => ['class'=>'text-primary  text-nowrap', ],
        'value'=> function($data ){
            return '<strong>Họ tên: </strong>'.$data->hoten.'<br/>'.
                '<strong>Ngày đăng ký: </strong>'.$data->updated_at.'<br/>';
        },
        'format' => 'raw',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'ngay_sinh',
        'label' => 'Ngày sinh',
        'headerOptions' => ['class'=>'text-primary  text-nowrap', 'width'=>'1%'],

        'format' => 'raw',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'dien_thoai',
        'label' => 'Điện thoại',
        'headerOptions' => ['class'=>'text-primary  text-nowrap','width'=>'1%' ],
        'format' => 'raw',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'email',
        'label' => 'Email',
        'headerOptions' => ['class'=>'text-primary  text-nowrap','width'=>'1%' ],
        'format' => 'raw',
    ],
    [
        'header' => 'Xem',
        'value' => function($data) {
            return \yii\bootstrap\Html::a('<i class="fa fa-eye"></i>','', ['class'=>'btn-xem-chi-tiet-doanh-nghiep','data-value'=>$data->id]);
        },
        'format' => 'raw',
        'headerOptions' => ['width' => '1%', 'class' => ' text-nowrap text-center text-primary'],
        'contentOptions' => ['class' => 'text-center']
    ],

    [
        'header' => 'Sửa',
        'value' => function($data) {
            return \yii\bootstrap\Html::a('<i class="fa fa-edit"></i>',Url::toRoute(['user/update', 'id' => $data->id]), ['role' => 'modal-remote', 'data-toggle' => 'tooltip','id'=>'select2']);
        },
        'format' => 'raw',
        'headerOptions' => ['width' => '1%', 'class' => 'text-center  text-nowrap text-primary'],
        'contentOptions' => ['class' => 'text-center']
    ],

    [
        'header' => 'Xoá',
        'value' => function($data) {
            return \yii\bootstrap\Html::a('<i class="fa fa-trash"></i>','#', ['class'=>'xoa-doanh-nghiep text-danger','data-value'=>$data->id]);
        },
        'format' => 'raw',
        'headerOptions' => ['width' => '1%', 'class' => '  text-nowrap text-center text-primary'],
        'contentOptions' => ['class' => 'text-center']
    ],
];
?>

