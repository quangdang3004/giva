<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\User */
?>
<div class="user-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'username',
            'hoten',
            'email:email',
            'dien_thoai',
            'so_du',
            [
                'attribute' => 'created_at',
                'value' => function($data){
                    return date('d/m/Y', strtotime($data-> created_at));
                },
            ],
            [
                'attribute' => 'updated_at',
                'value' => function($data){
                    return is_null($data->updated_at) ? '<i>Đang cập nhật</i>'
                        : date('d/m/Y', strtotime($data-> updated_at));
                },
                'format'=>'raw',
            ],
            [
                'label' => 'Doanh nghiệp được quản lý',
                'value' => function($data){
                    /** @var $data \backend\models\User */
                    return $data->doanhNghiep ? $data->doanhNghiep->name : '<i>Đang cập nhật</i>';
                },
                'format'=>'raw',

            ],
            'trang_thai',
        ],
    ]) ?>

</div>
