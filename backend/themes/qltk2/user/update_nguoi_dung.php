
<?php

use common\models\myAPI;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hoten')->textInput(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'dien_thoai')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <?= $form->field($model, 'avatar')->fileInput(['rows' => 6]) ?>

    <?= $form->field($model, 'so_du')->textInput(['maxlength' => true]) ?>

    <?= myAPI::activeDateField($form, $model, 'created_at', 'Ngày tạo')?>

    <?= myAPI::activeDateField($form, $model, 'updated_at', 'Ngày cập nhật')?>

    <?= $form->field($model, 'type')->dropDownList([ 'Quản trị viên' => 'Quản trị viên', 'Nhà tuyển dụng' => 'Nhà tuyển dụng', 'Cá nhân' => 'Cá nhân', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'trang_thai')->dropDownList([ 'Chưa xác minh' => 'Chưa xác minh', 'Đang xác minh' => 'Đang xác minh', 'Đã xác minh' => 'Đã xác minh', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>


    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>

