<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\myAPI;
use backend\models\VaiTro;
use yii\helpers\ArrayHelper;
/* @var $anh_doang_nghiep[]**/
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>
    <h4 class="text-primary">Thông tin tài khoản</h4>
    <?=Html::activeHiddenInput($model, 'nhom');?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'hoten')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'username')->textInput()->label('Tài khoản')?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'password_hash')->textInput()->label('Mật khẩu') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'dien_thoai')->textInput(['type'=>'number'])->label('Điện thoại');?>
        </div>
        <div class="col-md-4">
            <?= myAPI::activeDateField2($form, $model, 'ngay_sinh', 'Ngày sinh', (date("Y") - 50).':'.date("Y")) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'email')->textInput()->label('email');?>
        </div>
    </div>
    <h4 class="text-primary">Thông tin doanh nghiệp</h4>
    <?=Html::activeHiddenInput($model, 'nhom');?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'ten_doanh_nghiep')->textInput([])->label('Tên doanh nghiệp') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'ma_so_thue')->textInput()->label('Mã số thuế')?>
        </div>
        <div class="col-md-8">
            <?= $form->field($model, 'anh_doanh_nghiep[]')->fileInput(['multiple' => 'multiple','class'=>'form-control'])->label('Ảnh sản phẩm') ?>
        </div>
    </div>
    <?php if(!$model->isNewRecord): ?>
        <div class='row'>
            <?php foreach ($anh_doanh_nghiep as $item) :?>
                <div class='col-md-2'>
                    <?=Html::img('images/'.$item->file_name, ['class' => 'img-responsive']) ?>
                    <p><?=Html::a('<i class="glyphicon glyphicon-trash"></i> Xóa','#',['class' => 'btn btn-sm btn-danger xoa-anh-doanh-nghiep','data-value'=>$item->id]) ?></p>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
