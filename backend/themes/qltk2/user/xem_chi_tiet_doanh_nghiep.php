
<?php

use backend\models\ChiaSeKhachHang;
use backend\models\LichSuDoanhThu;
use backend\models\SanPham;
use backend\models\ThongTinBanHang;
use backend\models\TrangThaiChiaSeKhachHang;
use backend\models\TrangThaiSanPham;
use common\models\User;
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\VarDumper;

/** @var $doanh_nghiep */
/** @var $anh_doanh_nghiep[] */
/** @var $trang_thai_doanh_nghiep[] */

?>

<div class="tabbale-line">
    <ul class="nav nav-tabs ">
        <li class="active">
            <a href="#tab_15_1" data-toggle="tab">THÔNG TIN CHUNG</a>
        </li>
        <li>
            <a href="#tab_15_2" data-toggle="tab">DANH SÁCH ĐƠN HÀNG</a>
        </li>
        <li>
            <a href="#tab_15_3" data-toggle="tab">DANH SÁCH ỨNG VIÊN</a>
        </li>
        <li>
            <a href="#tab_15_4" data-toggle="tab">LỊCH SỬ TRẠNG THÁI</a>
        </li>


    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_15_1">
            <h4 class="text-primary">Thông tin tài khoản </h4>
            <hr/>
            <div class="row">
                <!--                Họ tên-->
                <div class="col-md-1 col-xs-6"><p><strong>Họ tên</strong></p></div>
                <div class="col-md-5 col-xs-6"><p><?= $doanh_nghiep->hoten ?></p></div>
                <!--                Số điện thoại-->
                <div class="col-md-1 col-xs-6"><p><strong>Điện thoại</strong></p></div>
                <div class="col-md-5 col-xs-6"><p><?= $doanh_nghiep->dien_thoai ?></p></div>
            </div>
            <div class="row">
                <!--                Email-->
                <div class="col-md-1 col-xs-6"><p><strong>Email</strong></p></div>
                <div class="col-md-5 col-xs-6"><p><?= $doanh_nghiep->email ?></p></div>
                <!--                Ngày sinh-->
                <div class="col-md-1 col-xs-6"><p><strong>Ngày sinh</strong></p></div>
                <div class="col-md-11    col-xs-6"><p><?= $doanh_nghiep->ngay_sinh ?></p></div>
            </div>
            <div class="row">
                <!--                Địa chỉ-->
                <div class="col-md-1 col-xs-6"><p><strong>Địa chỉ</strong></p></div>
                <div class="col-md-5 col-xs-6"><p><?= $doanh_nghiep->dia_chi ?></p></div>
            </div>
            <h4 class="text-primary">Thông tin doanh nghiệp </h4>
            <hr/>
            <div class="row">
                <!--                Họ tên-->
                <div class="col-md-1 col-xs-6"><p><strong>Họ tên</strong></p></div>
                <div class="col-md-5 col-xs-6"><p><?= $doanh_nghiep->ten_doanh_nghiep ?></p></div>
                <!--                Số điện thoại-->
                <div class="col-md-1 col-xs-6"><p><strong>Điện thoại</strong></p></div>
                <div class="col-md-5 col-xs-6"><p><?= $doanh_nghiep->ma_so_thue ?></p></div>
            </div>
            <div class="row">
                <!--                Email-->
                <div class="col-md-1 col-xs-6"><p><strong>Email</strong></p></div>
                <!--                Ngày sinh-->
                <div class="col-md-1 col-xs-6"><p><strong>Ngày sinh</strong></p></div>
            </div>
            <h4 class="text-primary">Ảnh liên quan </h4>
            <hr/>
            <div class='row'>
                <?php foreach ($anh_doanh_nghiep as $item) :?>
                    <div class='col-md-2'>
                        <?=Html::img('images/'.$item->file_name, ['class' => 'img-responsive']) ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>

        <div class="tab-pane " id="tab_15_2">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th width="1%" class="text-nowrap">
                        STT
                    </th>
                    <th  class="text-nowrap ">
                       Thông tin đơn hàng
                    </th>
                    <th width="1%" class="text-nowrap ">
                        Yêu cầu ứng viên
                    </th>
                    <th width="1%" class="text-nowrap">
                        Mức lương
                    </th>
                    <th width="1%" class="text-nowrap">
                        Số lương
                    </th>
                </tr>
                </thead>
            </table>
        </div>
        <div class="tab-pane " id="tab_15_3">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th width="1%" class="text-nowrap">
                        STT
                    </th>
                    <th  class="text-nowrap ">
                        Thông tin ứng viên
                    </th>
                    <th width="1%" class="text-nowrap">
                        Ứng viên vị trí
                    </th>
                </tr>
                </thead>

            </table>
        </div>
        <div class="tab-pane " id="tab_15_4">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th width="1%" class="text-nowrap">
                        STT
                    </th>
                    <th  class="text-nowrap ">
                        Trạng thái
                    </th>
                    <th width="1%" class="text-nowrap">
                        Ngày cập nhật
                    </th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

</div>
<style>

    table {
        width: auto;
    }

    td {
        padding-right: 10px;
        white-space: nowrap;
        padding-top: 7px;
        padding-bottom: 7px;
    }

    .btn-click {
        display: inline-block;
        width: 100%;
    }


</style>

<?php $this->registerCssFile(Yii::$app->request->baseUrl . '/backend/assets/plugins/lightbox/src/css/lightbox.css',
    ['depends' => ['backend\assets\Qltk2Asset'], 'position' => View::POS_END]); ?>
<?php $this->registerJsFile(Yii::$app->request->baseUrl . '/backend/assets/plugins/lightbox/src/js/lightbox.js',
    ['depends' => ['backend\assets\Qltk2Asset'], 'position' => View::POS_END]); ?>
<?php $this->registerJsFile(Yii::$app->request->baseUrl . '/backend/assets/js-view/xem-chi-tiet-san-pham1.js',
    ['depends' => ['backend\assets\Qltk2Asset'], 'position' => View::POS_END]); ?>

