<?php

use backend\models\VaiTro;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'hoten')->textInput()->label('Họ tên')?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'username')->textInput()->label('Tài khoản') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'password_hash')->passwordInput()->label('Mật khẩu')?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'dien_thoai')->textInput()->label('Điện thoại')?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'email')->textInput()->label('Email')?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'dia_chi')->textInput()->label('Địa chỉ')?>
        </div>
    </div>

    <h4>Vai trò</h4><hr/>
    <?= Html::checkboxList('Vaitrouser[]', $vaitros, ArrayHelper::map(VaiTro::find()->all(), 'id', 'name'),
        ['prompt' => '', 'class' => 'list-block']) ?>

    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
