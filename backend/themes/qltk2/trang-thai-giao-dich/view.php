<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\TrangThaiGiaoDich */
?>
<div class="trang-thai-giao-dich-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'giao_dich_id',
            'trang_thai',
            'created',
            'user_id',
        ],
    ]) ?>

</div>
