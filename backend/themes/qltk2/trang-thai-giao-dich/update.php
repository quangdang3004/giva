<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TrangThaiGiaoDich */
?>
<div class="trang-thai-giao-dich-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
