<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TrangThaiTienDoGiaoDich */
?>
<div class="trang-thai-tien-do-giao-dich-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
