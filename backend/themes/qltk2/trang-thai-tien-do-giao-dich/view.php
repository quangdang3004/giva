<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\TrangThaiTienDoGiaoDich */
?>
<div class="trang-thai-tien-do-giao-dich-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'giao_dich_id',
            'trang_thai',
            'created',
        ],
    ]) ?>

</div>
