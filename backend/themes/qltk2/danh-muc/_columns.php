<?php

use backend\models\DanhMuc;
use yii\helpers\Url;

/* @var $searchModel backend\models\search\DanhMucSearch */
/** @var $doi_tuong  string */
//\yii\helpers\VarDumper::dump(($doi_tuong ), 10, true); exit();
return [
//    [
//        'class' => 'kartik\grid\CheckboxColumn',
//        'width' => '1%',
//    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '1%',
        'header' => 'STT',
        'headerOptions' => ['class' => 'text-primary'],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
//    [
//        'headerOptions' => ['class' => 'text-primary',],
//        'class' => '\kartik\grid\DataColumn',
//        'attribute' => 'type',
////        'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'type', \backend\models\DanhMuc::getDanhMuc(),
////            ['class' => 'form-control', 'prompt' => '--Chọn--']),
//    ],
    [
        'value' => function($data){
            return \yii\bootstrap\Html::a('<i class="fa fa-eye"></i>','#',
                ['class' => 'text-gray btn-xem-khu-vuc','title'=>'Xem', 'data-toggle'=>'tooltip',
                    'data-value' => $data->id]);
        },
        'label' => 'Xem',
        'format' => 'raw',
        'headerOptions' => ['class' => 'text-center text-primary','style'=>'width:1%; vertical-align: inherit;', 'rowspan' => 2],
        'contentOptions' => ['class' => 'text-center'],
        'visible' => ($doi_tuong == 'khu_vuc') ? true : false,
    ],
    [
        'value' => function($data){
            return \yii\bootstrap\Html::a('<i class="fa fa-edit"></i>',['danh-muc/update'],
                ['class' => 'text-gray btn-update-danh-muc','title'=>'Cập nhật', 'data-toggle'=>'tooltip',
                    'data-value' => $data->id]);
        },
        'label' => 'Sửa',
        'format' => 'raw',
        'headerOptions' => ['class' => 'text-center text-primary','style'=>'width:1%; vertical-align: inherit;', 'rowspan' => 2],
        'contentOptions' => ['class' => 'text-center'],
        'visible' => ($doi_tuong != 'khu_vuc') ? true : false,
    ],
    [
        'header' => 'Xóa',
        'headerOptions' => ['class' => 'text-center text-primary test', 'width' => '1%', 'rowspan' => 2, 'style' => 'vertical-align: inherit'],
        'contentOptions' => ['class' => 'text-center'],
        'value' => function($data){
            $model = DanhMuc::findOne($data->id);
//            \yii\helpers\VarDumper::dump($model->active, 10, true); exit();
            if($model->active == 1)
                return \common\models\myAPI::createDeleteBtnInGrid(Url::toRoute(['danh-muc/delete', 'id' => $data->id]));
        },
        'format' => 'raw',
        'visible' => ($doi_tuong != 'khu_vuc') ? true : false,
    ]
//    [
//        'header' => 'Khôi phục',
//        'headerOptions' => ['class' => 'text-center text-primary', 'width' => '1%'],
//        'contentOptions' => ['class' => 'text-center'],
//        'value' => function($data){
//            $model = \backend\models\DanhMuc::findOne($data->id);
//            if($model->active == 0)
//                return \yii\bootstrap\Html::a('<i class="fa fa-repeat"></i>',Url::toRoute(['danh-muc/back-up', 'id' => $data->id]), ['class' => 'text-gray','role'=>'modal-remote','title'=>'Khôi phục', 'data-toggle'=>'tooltip']);
//        },
//        'format' => 'raw'
//    ]

];   
