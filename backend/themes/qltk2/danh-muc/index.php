<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\DanhMucSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/** @var $doi_tuong  string */

$this->title = 'Quản lý '.($doi_tuong == 'khu_vuc' ? 'khu vực' :'loại ngành ');
$this->params['breadcrumbs'][] = $this->title;
// \yii\helpers\VarDumper::dump($doi_tuong, 10, true); exit();
CrudAsset::register($this);

?>
<div class="nhommau-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'summary' => "Hiển thị {begin} - {end} Trên tổng số {totalCount}",
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'emptyText' => 'Chưa có dữ liệu',
            'columns' => require(__DIR__.'/_columns.php'),
            'toolbar'=> [
                ['content'=>
                    $doi_tuong == 'khu_vuc' ? Html::a('<i class="glyphicon glyphicon-repeat"></i> Khôi phục lưới',
                        ['danh-muc/index', 'doi_tuong'  => $doi_tuong],
                        ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Khôi phục lưới'])

                    : Html::a('<i class="glyphicon glyphicon-plus"></i> Thêm loại ngành', '',
                        ['title'=> 'Thêm mới chức năng',
                            'class'=>'btn btn-primary btn-them-moi-danh-muc', 'data-value' => $doi_tuong]).
                    Html::a('<i class="glyphicon glyphicon-repeat"></i> Khôi phục lưới',
                        ['danh-muc/index', 'doi_tuong'  => $doi_tuong],
                        [ 'class'=>'btn btn-default', 'title'=>'Khôi phục lưới']),

                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
//            'responsiveWrap' => false,
//            'tableOptions' => ['class' => 'table table-bordered table-striped text-nowrap table-responsive'],
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="glyphicon glyphicon-list"></i> Danh sách '.($doi_tuong == 'khu_vuc' ? 'khu vực' :'loại ngành '),
//                'after' => false,
//                'showFooter' => false,
            ]
        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
<?php $this->registerCssFile(Yii::$app->request->baseUrl.'/backend/assets/css/table.css',[ 'depends' => ['backend\assets\Qltk2Asset'], 'position' => \yii\web\View::POS_END ]); ?>

<?php $this->registerJsFile(Yii::$app->request->baseUrl.'/backend/assets/js-view/user.js',[ 'depends' => ['backend\assets\Qltk2Asset'], 'position' => \yii\web\View::POS_END ]); ?>


