<?php
use yii\widgets\ActiveForm;
/* @var $model backend\models\DanhMuc */
?>

<div class="form-control" style="height: auto; border: none">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?php ActiveForm::end(); ?>
</div>

