<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\AnhLienKetDonHang */

?>
<div class="anh-lien-ket-don-hang-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
