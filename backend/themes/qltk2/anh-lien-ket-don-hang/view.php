<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\AnhLienKetDonHang */
?>
<div class="anh-lien-ket-don-hang-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'link',
            'don_hang_id',
            'created',
            'active',
        ],
    ]) ?>

</div>
