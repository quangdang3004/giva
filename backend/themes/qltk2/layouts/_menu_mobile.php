<?php

use common\models\User;
use common\models\VietHungAPI;
use yii\helpers\Html;
use common\models\myAPI;
use yii\helpers\Url;
?>
<ul class="page-sidebar-menu" data-slide-speed="200" data-auto-scroll="true">
    <?php if (myAPI::isAccess2('DoanhNghiep', 'index')): ?>
        <li>
            <?= Html::a('<i class="fa fa-users"></i> Quản lý doanh nghiệp', Url::to(['doanh-nghiep/index'])) ?>
        </li>
    <?php endif; ?>


    <?php if (myAPI::isAccess2('User', 'index')): ?>
        <li>
            <?= Html::a('<i class="fa fa-user"></i> Nhà tuyển dụng',
                Yii::$app->urlManager->createUrl(['user/index', 'doi_tuong' => 'nha_tuyen_dung'])) ?>
        </li>
    <?php endif; ?>

    <li class="classic-menu-dropdown">
        <a href="javascript:;">
            <i class="fa fa-check-circle"></i></i> Quản lý xác thực <span class="arrow">
        </a>
        <ul class="sub-menu">
            <?php if (myAPI::isAccess2('XacThuc', 'index')): ?>
                <li>
                    <?= Html::a('<i class="fa fa-user"></i> Xác thực cá nhân',
                        Yii::$app->urlManager->createUrl(['yeu-cau-xac-thuc/index', 'doituong' => 'ca_nhan'])) ?>
                </li>
                <li>
                    <?= Html::a('<i class="fa fa-handshake-o"></i> Xác thực doanh nghiệp',
                        Yii::$app->urlManager->createUrl(['yeu-cau-xac-thuc/index', 'doituong' => 'doanh_nghiep'])) ?>
                </li>
            <?php endif; ?>

        </ul>
    </li>
    <li class="classic-menu-dropdown">
        <a href="javascript:;">
            <i class="fa fa-bars"></i> Quản lý danh mục <span class="arrow">
        </a>
        <ul class="sub-menu">
            <?php if (myAPI::isAccess2('BannerApp', 'index')): ?>
                <li>
                    <?= Html::a('<i class="fa fa-picture-o" aria-hidden="true"></i> Quản lý Banners',
                        Yii::$app->urlManager->createUrl(['banner-app/index'])) ?>
                </li>
            <?php endif; ?>
            <?php if (myAPI::isAccess2('DanhMuc', 'index')): ?>
                <li>
                    <?= Html::a('<i class="fa fa-user"></i> Quản lý loại ngành',
                        Yii::$app->urlManager->createUrl(['danh-muc/index', 'doi_tuong' => 'loai_nganh'])) ?>
                </li>
                <li>
                    <?= Html::a('<i class="fa fa-location-arrow"></i> Quản lý khu vực',
                        Yii::$app->urlManager->createUrl(['danh-muc/index', 'doi_tuong' => 'khu_vuc'])) ?>
                </li>
            <?php endif; ?>

        </ul>
    </li>
    <li>
        <a href="javascript:;">
            <i class="fa fa-bars"></i> Giao dịch <span class="arrow">
        </a>
        <ul class="sub-menu">
            <?php if (myAPI::isAccess2('GiaoDich', 'index')): ?>
                <li>
                    <?= Html::a('<i class="fa fa-exchange" aria-hidden="true"></i>&ensp;Quản lý giao dịch', Url::to(['giao-dich/index'])) ?>
                </li>
            <?php endif; ?>
            <?php if (myAPI::isAccess2('DonHang', 'index')): ?>
                <li>
                    <?= Html::a('<i class="fa fa-address-book-o" aria-hidden="true"></i> Quản lý đơn hàng',
                        Yii::$app->urlManager->createUrl(['don-hang/index'])) ?>
                </li>
            <?php endif; ?>

        </ul>
    </li>
    <?php if(
        myAPI::isAccess2('Cauhinh', 'index') ||
        myAPI::isAccess2('VaiTro', 'index') ||
        myAPI::isAccess2('ChucNang', 'index') ||
        myAPI::isAccess2('User', 'index') ||
        myAPI::isAccess2('PhanQuyen', 'index')
    ): ?>
        <li>
            <a href="javascript:;">
                <i class="fa fa-cog"></i> Hệ thống <span class="arrow">
            </a>
            <ul class="sub-menu">
                <?php if(myAPI::isAccess2('Cauhinh', 'index')): ?>
                    <li>
                        <?=Html::a('<i class="fa fa-cogs"></i> Cấu hình', Yii::$app->urlManager->createUrl(['cauhinh']))?>
                    </li>
                <?php endif; ?>
                <?php if(myAPI::isAccess2('User', 'index')): ?>
                    <li>
                        <?=Html::a('<i class="fa fa-users"></i> Thành viên', Url::to(['user/index']))?>
                    </li>
                <?php endif; ?>
                <?php if(myAPI::isAccess2('VaiTro', 'index')): ?>
                    <li>
                        <?=Html::a('<i class="fa fa-users"></i> Vai trò', Yii::$app->urlManager->createUrl(['vai-tro']))?>
                    </li>
                <?php endif; ?>

                <?php if(myAPI::isAccess2('ChucNang', 'index')): ?>
                    <li>
                        <?=Html::a('<i class="fa fa-bars"></i> Chức năng', Yii::$app->urlManager->createUrl(['chuc-nang']))?>
                    </li>
                <?php endif; ?>

                <?php if(myAPI::isAccess2('PhanQuyen', 'index')): ?>
                    <li>
                        <?=Html::a('<i class="fa fa-users"></i> Phân quyền', Yii::$app->urlManager->createUrl(['phan-quyen']))?>
                    </li>
                <?php endif; ?>
            </ul>
        </li>
    <?php endif; ?>
</ul>
