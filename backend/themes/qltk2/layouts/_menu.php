<?php

use backend\models\DoanhNghiep;
use backend\models\QuanLyYeuCauXacThuc;
use common\models\numberAPI;
use common\models\User;
use common\models\VietHungAPI;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use common\models\myAPI;
use yii\helpers\Url;

//$numberAPI = new numberAPI();
//$chuaXacMinh = $numberAPI->getNumberByTitleColumn(QuanLyYeuCauXacThuc::CHUA_XAC_MINH);
//$choXacMinh = $numberAPI->getNumberByTitleColumn(QuanLyYeuCauXacThuc::DANG_XAC_MINH);
?>
<ul class="nav navbar-nav">

    <?php if (myAPI::isAccess2('DoanhNghiep', 'index')): ?>
        <li>
            <?= Html::a('<i class="fa fa-users"></i> Quản lý doanh nghiệp', Url::to(['doanh-nghiep/index'])) ?>
        </li>
    <?php endif; ?>


    <?php if (myAPI::isAccess2('User', 'index')): ?>
        <li>
            <?= Html::a('<i class="fa fa-user"></i> Nhà tuyển dụng',
                Yii::$app->urlManager->createUrl(['user/index', 'doi_tuong' => 'nha_tuyen_dung'])) ?>
        </li>
    <?php endif; ?>

    <li class="classic-menu-dropdown">
        <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true">
            <i class="fa fa-check-circle"></i> Quản lý xác thực <i class="fa fa-angle-down"></i></a>
        <ul class="dropdown-menu pull-left">
            <?php if (myAPI::isAccess2('YeuCauXacThuc', 'index')): ?>
                <li>
                    <?= Html::a('<i class="fa fa-user"></i> Xác thực cá nhân',
                        Yii::$app->urlManager->createUrl(['yeu-cau-xac-thuc/index', 'doituong' => 'ca_nhan'])) ?>
                </li>
                <li>
                    <?= Html::a('<i class="fa fa-handshake-o"></i> Xác thực doanh nghiệp',
                        Yii::$app->urlManager->createUrl(['yeu-cau-xac-thuc/index', 'doituong' => 'doanh_nghiep'])) ?>
                </li>
            <?php endif; ?>

        </ul>
    </li>
    <li class="classic-menu-dropdown">
        <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true">
            <i class="fa fa-bars"></i> Quản lý danh mục <i class="fa fa-angle-down"></i></a>
        <ul class="dropdown-menu pull-left">
            <?php if (myAPI::isAccess2('BannerApp', 'index')): ?>
                <li>
                    <?= Html::a('<i class="fa fa-picture-o" aria-hidden="true"></i> Quản lý Banners',
                        Yii::$app->urlManager->createUrl(['banner-app/index'])) ?>
                </li>
            <?php endif; ?>
            <?php if (myAPI::isAccess2('DanhMuc', 'index')): ?>
                <li>
                    <?= Html::a('<i class="fa fa-user"></i> Quản lý loại ngành',
                        Yii::$app->urlManager->createUrl(['danh-muc/index', 'doi_tuong' => 'loai_nganh'])) ?>
                </li>
                <li>
                    <?= Html::a('<i class="fa fa-location-arrow"></i> Quản lý khu vực',
                        Yii::$app->urlManager->createUrl(['danh-muc/index', 'doi_tuong' => 'khu_vuc'])) ?>
                </li>
            <?php endif; ?>

        </ul>
    </li>
    <li>
        <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true">
            <i class="fa fa-globe" aria-hidden="true"></i> Giao dịch <i class="fa fa-angle-down"></i></a>
        <ul class="dropdown-menu pull-left">
            <?php if (myAPI::isAccess2('GiaoDich', 'index')): ?>
                <li>
                    <?= Html::a('<i class="fa fa-exchange" aria-hidden="true"></i>&ensp;Quản lý giao dịch', Url::to(['giao-dich/index'])) ?>
                </li>
            <?php endif; ?>
            <?php if (myAPI::isAccess2('DonHang', 'index')): ?>
                <li>
                    <?= Html::a('<i class="fa fa-address-book-o" aria-hidden="true"></i> Quản lý đơn hàng',
                        Yii::$app->urlManager->createUrl(['don-hang/index'])) ?>
                </li>
            <?php endif; ?>

        </ul>
    </li>


    <div class="modal fade" id="modal-them-thanh-vien" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-full">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <div class="thongbao"></div>
                    <div class="content-body"></div>
                </div>
            </div>
        </div>
    </div>

    <?php if (myAPI::isAccess2('Cauhinh', 'index') ||
        myAPI::isAccess2('VaiTro', 'index') ||
        myAPI::isAccess2('ChucNang', 'index') ||
        myAPI::isAccess2('User', 'index') ||
        myAPI::isAccess2('DanhMuc', 'Index') ||
        myAPI::isAccess2('PhanQuyen', 'index')): ?>
        <li class="classic-menu-dropdown">
            <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true">
                <i class="fa fa-cog"></i> Hệ thống <i class="fa fa-angle-down"></i></a>
            <ul class="dropdown-menu pull-left">
                <?php if (myAPI::isAccess2('User', 'index')): ?>
                    <li>
                        <?= Html::a('<i class="fa fa-users"></i> Thành viên', Yii::$app->urlManager->createUrl(['user/index'])) ?>
                    </li>
                <?php endif; ?>
                <?php if (myAPI::isAccess2('VaiTro', 'index')): ?>
                    <li>
                        <?= Html::a('<i class="fa fa-users"></i> Vai trò', Yii::$app->urlManager->createUrl(['vai-tro'])) ?>
                    </li>
                <?php endif; ?>
                <?php if (myAPI::isAccess2('ChucNang', 'index')): ?>
                    <li>
                        <?= Html::a('<i class="fa fa-bars"></i> Chức năng', Yii::$app->urlManager->createUrl(['chuc-nang'])) ?>
                    </li>
                <?php endif; ?>
                <?php if (myAPI::isAccess2('PhanQuyen', 'index')): ?>
                    <li>
                        <?= Html::a('<i class="fa fa-users"></i> Phân quyền', Yii::$app->urlManager->createUrl(['phan-quyen'])) ?>
                    </li>
                <?php endif; ?>
            </ul>
        </li>
    <?php endif; ?>
</ul>
