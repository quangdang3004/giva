<?php
/** @var $this View */
use yii\helpers\Html;
use yii\web\View;
use common\models\User;

//\yii\helpers\VarDumper::dump($slYeuCauChoChapNhanChiaSe,10,true); exit();
?>

<!-- BEGIN HEADER -->
<div class="page-header -i navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="<?=Yii::$app->urlManager->createUrl('site/index')?>" class="text-default">
            </a>
        </div>
        <!-- END LOGO -->

        <!-- BEGIN HORIZANTAL MENU -->
        <div class="hor-menu hidden-sm hidden-xs">
            <?=$this->render('_menu'); ?>
        </div>
        <!-- END HORIZANTAL MENU -->

        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"></a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav">
                <li>
                    <?=Html::a('<span class="badge badge-danger"></span>', '#',['id'=>'danh-sach-cho-chap-nhan-chia-se','data-value'=>Yii::$app->user->id])?>
                </li>
            </ul>
            <ul class="nav navbar-nav">
                <li>
                    <?=Html::a('<span class="badge badge-danger"></span>', '#',['id'=>'danh-sach-nhan-chia-se','data-value'=>Yii::$app->user->id])?>
                </li>
            </ul>
            <ul class="nav navbar-nav pull-right">
                <!-- BEGIN USER LOGIN DROPDOWN -->
                <li class="dropdown dropdown-user">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <img alt="" class="img-circle" src="<?=Yii::$app->request->baseUrl ?>/backend/themes/qltk2/assets/admin/layout/img/avatar3_small.jpg"/>
                        <span class="username username-hide-on-mobile">
                            <?=Yii::$app->user->isGuest ? '' : Yii::$app->user->identity->username; ?>
                        </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <?=Html::a('<i class="fa fa-user"></i> '.(Yii::$app->user->isGuest ? '' : Yii::$app->user->identity->username), '#', ['class' => 'btn-chitietnguoidung', 'data-value'=>Yii::$app->user->id])?>
                        </li>
                        <?php if (\common\models\myAPI::isAccess2('Site', 'index')): ?>
                            <li>
                                <?=Html::a('<i class="icon-key"></i> Đổi mật khẩu', '#', ['class' => 'btn-doimatkhau'])?>
                            </li>
                        <?php endif; ?>
                        <li>
                            <?=Html::a('<i class="glyphicon glyphicon-log-out"></i> Đăng xuất', Yii::$app->urlManager->createUrl('site/logout'))?>
                        </li>

                    </ul>
                </li>
                <!-- END USER LOGIN DROPDOWN -->
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix"></div>
<?php $this->registerJsFile(Yii::$app->request->baseUrl.'/backend/assets/js-view/menu.js',[ 'depends' => ['backend\assets\Qltk2Asset'], 'position' => \yii\web\View::POS_END ]); ?>

