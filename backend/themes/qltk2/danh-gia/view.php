<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\DanhGia */
?>
<div class="danh-gia-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'noi_dung:ntext',
            'active',
            'rate',
            'created',
            'updated',
            'doanh_nghiep_id',
            'user_created_id',
            'type',
        ],
    ]) ?>

</div>
