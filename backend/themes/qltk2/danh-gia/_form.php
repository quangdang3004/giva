<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\DanhGia */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="danh-gia-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'noi_dung')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'active')->textInput() ?>

    <?= $form->field($model, 'rate')->textInput() ?>

    <?= $form->field($model, 'created')->textInput() ?>

    <?= $form->field($model, 'updated')->textInput() ?>

    <?= $form->field($model, 'doanh_nghiep_id')->textInput() ?>

    <?= $form->field($model, 'user_created_id')->textInput() ?>

    <?= $form->field($model, 'type')->dropDownList([ 'Doanh nghiệp' => 'Doanh nghiệp', 'Ứng viên' => 'Ứng viên', ], ['prompt' => '']) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
