<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\DanhGia */

?>
<div class="danh-gia-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
