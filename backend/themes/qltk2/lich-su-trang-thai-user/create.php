<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\LichSuTrangThaiUser */

?>
<div class="lich-su-trang-thai-user-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
