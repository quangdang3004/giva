<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\LichSuTrangThaiUser */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lich-su-trang-thai-user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'trang_thai')->dropDownList([ 'Chờ xác minh' => 'Chờ xác minh', 'Đang xác minh' => 'Đang xác minh', 'Đã xác minh' => 'Đã xác minh', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'created')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
