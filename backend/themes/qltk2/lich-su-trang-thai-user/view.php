<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\LichSuTrangThaiUser */
?>
<div class="lich-su-trang-thai-user-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'trang_thai',
            'created',
        ],
    ]) ?>

</div>
