<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\NhaTuyenDungCongTac */
?>
<div class="nha-tuyen-dung-cong-tac-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'ten_cong_ty:ntext',
            'thoi_gian_vao',
            'thoi_gian_nghi',
            'trang_thai',
            'trang_thai_lam_viec',
            'created',
            'active',
        ],
    ]) ?>

</div>
