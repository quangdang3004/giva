<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\NhaTuyenDungCongTac */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nha-tuyen-dung-cong-tac-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'ten_cong_ty')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'thoi_gian_vao')->textInput() ?>

    <?= $form->field($model, 'thoi_gian_nghi')->textInput() ?>

    <?= $form->field($model, 'trang_thai')->dropDownList([ 'Chưa duyệt' => 'Chưa duyệt', 'Đã duyệt' => 'Đã duyệt', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'trang_thai_lam_viec')->dropDownList([ 'Đang công tác' => 'Đang công tác', 'Đã nghỉ' => 'Đã nghỉ', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'created')->textInput() ?>

    <?= $form->field($model, 'active')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
