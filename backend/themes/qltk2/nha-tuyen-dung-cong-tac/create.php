<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\NhaTuyenDungCongTac */

?>
<div class="nha-tuyen-dung-cong-tac-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
