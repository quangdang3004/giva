<?php
/** @var $model \backend\models\ChiTietGiaoDichDoanhNghiep */

use backend\models\GiaoDich;

?>
<div class="form-control" style="height: auto; border: none">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6" style="border-right: #0a6aa1 dashed 1px; padding-bottom: 10px">
                <h4 class="col-md-12 text-primary">Thông tin người gửi</h4>
                <div class="col-md-3"><img src="<?= \backend\models\User::getLinkAnh().$model->avatar_chu_don_hang ?>" alt="Avt chủ đơn hàng" width="100px"></div>
                <div class="col-md-9">
                    <div><strong>Tên chủ đơn hàng: </strong> <?= $model->ten_chu_don_hang  ?></div>
                    <div><strong>Điện thoại: </strong> <?= $model->dien_thoai_chu_don_hang  ?></div>
                    <div><strong>Email: </strong> <?= $model->email_chu_don_hang  ?></div>
                    <div><strong>Doanh nghiệp: </strong> <?= $model->ten_doanh_nghiep_chu_don_hang  ?></div>
                    <div><strong>Trạng thái doanh nghiệp: </strong> <?= \backend\models\DoanhNghiep::getListTrangThaiDisplay()[$model->trang_thai_doanh_nghiep]?></div>
                    <div><strong>Trạng thái đơn hàng: </strong> <?= $model->trang_thai_don_hang ?></div>
                </div>
            </div>
            <div class="col-md-6">
                <h4 class="col-md-12 text-primary">Thông tin người nhận</h4>
                <div class="col-md-3"><img src="<?= \backend\models\User::getLinkAnh().$model->avatar ?>" alt="Avt người nhận" width="100px"></div>
                <div class="col-md-9">
                    <div><strong>Tên người nhận: </strong> <?= $model->ho_ten_nguoi_yeu_cau  ?></div>
                    <div><strong>Điện thoại: </strong> <?= $model->dien_thoai_nguoi_yeu_cau  ?></div>
                    <div><strong>Email: </strong> <?= $model->email_nguoi_yeu_cau  ?></div>
                    <div><strong>Trạng thái: </strong> <?= \backend\models\DoanhNghiep::getListTrangThaiDisplay()[$model->trang_thai_nguoi_yeu_cau]?></div>
                    <div><strong>Địa chỉ: </strong> <?= $model->dia_chi_nguoi_yeu_cau ?></div>
                </div>
            </div>
        </div>

        <div class="col-md-12" style="border-top: #0a6aa1 dashed 1px;">
            <div class="col-md-12 text-primary"><h4>Thông tin giao dịch</h4></div>
            <div class="col-md-4"><strong>Loại giao dịch: </strong> <?=$model->type?></div>
            <div class="col-md-4"><strong>Số lượng: </strong> <?=$model->so_luong?></div>
            <div class="col-md-4"><strong>Đơn vị: </strong> <?=$model->don_vi?></div>
            <div class="col-md-4"><strong>Tên tỉnh thành: </strong> <?= $model->ten_tinh_thanh ?></div>
            <div class="col-md-4"><strong>Nghề nghiệp: </strong> <?= $model->ten_nghe_nghiep ?></div>
            <div class="col-md-4"><strong>Tên công ty: </strong> <?= $model->ten_cong_ty ?></div>

            <div class="col-md-4"><strong>Trạng thái: </strong> <?= $model->trang_thai ? GiaoDich::getListTTGDDisplay()[$model->trang_thai]
                    : '<i>Đang cập nhật</i>'; ?></div>
            <div class="col-md-4"><strong>Ngày tạo: </strong> <?= $model->created?></div>
            <div class="col-md-4"><strong>Tiến độ: </strong> <?= $model->tien_do ? GiaoDich::getListTDGDDisplay()[$model->tien_do]
                    : '<i>Đang cập nhật</i>'; ?></div>
        </div>

    </div>
</div>
