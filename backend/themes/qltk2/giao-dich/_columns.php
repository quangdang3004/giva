<?php

use backend\models\ChiTietGiaoDichDoanhNghiep;
use backend\models\GiaoDich;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $searchModel \backend\models\search\ChiTietGiaoDichDoanhNghiepSearch */

return [
//    [
//        'class' => 'kartik\grid\CheckboxColumn',
//        'width' => '1%',
//    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'header' =>'STT',
        'headerOptions' => ['class' => 'text-primary'],
        'width' => '1%',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'user_created',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'ten_nghe_nghiep',
        'header' => 'Tên đơn hàng',
        'headerOptions' => ['class' => 'text-primary'],
        'value' => function ($data){
            return '<span class="badge badge-primary">#' . $data->don_hang_id . ' </span> '.$data->ten_nghe_nghiep ;
        },
        'format' => 'raw'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'ten_cong_ty',
        'header' => 'Tên công ty',
        'headerOptions' => ['class' => 'text-primary text-nowrap'],
        'contentOptions' => ['class' => 'text-nowrap'],
        'width' => '1%',

    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'ten_chu_don_hang',
        'header' => 'Tên người cung cấp',
        'headerOptions' => ['class' => 'text-primary text-nowrap'],
        'width' => '1%'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'ho_ten_nguoi_yeu_cau',
        'header' => 'Tên người nhận',
        'headerOptions' => ['class' => 'text-primary text-nowrap'],
        'contentOptions' => ['class' => 'text-nowrap'],
        'width' => '1%'
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'type',
//        'header' => 'Loại giao dịch',
//        'headerOptions' => ['class' => 'text-primary text-nowrap'],
//        'contentOptions' => ['class' => 'text-nowrap'],
//        'width' => '1%',
//        'value' => function($data){
//            /** @var $data GiaoDich */
//            return $data->type ? ChiTietGiaoDichDoanhNghiep::getListLoaiGiaoDich()[$data->type]
//                : '<i>Đang cập nhật</i>';
//        },
//        'filter' => Html::activeDropDownList($searchModel, 'type',ChiTietGiaoDichDoanhNghiep::getListLoaiGiaoDich(), [
//            'class' => 'form-control text-nowrap', 'prompt' => '-- Chọn --'
//        ]),
//
//    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'so_luong',
         'contentOptions' => ['class' => 'text-nowrap text-right'],
         'header' => 'Số lượng',
         'headerOptions' => ['class' => 'text-primary text-nowrap'],
         'width' =>'1%'

     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'don_vi',
         'header' => 'Đơn vị',
         'headerOptions' => ['class' => 'text-primary text-nowrap'],
         'contentOptions' => ['class' => 'text-nowrap'],
         'width' =>'10%',
         'value' => function($data){
             /** @var $data GiaoDich */
             return $data->don_vi ? ChiTietGiaoDichDoanhNghiep::getListDonViGiaoDich()[$data->don_vi]
                 : '<i>Đang cập nhật</i>';
         },
         'filter' => Html::activeDropDownList($searchModel, 'don_vi',ChiTietGiaoDichDoanhNghiep::getListDonViGiaoDich(), [
             'class' => 'form-control text-nowrap', 'prompt' => '-- Chọn --'
         ]),
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'trang_thai',
         'filter' => Html::activeDropDownList($searchModel, 'trang_thai', GiaoDich::getListTTGD(), [
             'class' => 'form-control text-nowrap', 'prompt' => '-- Chọn --'
         ]),
         'value' => function($data){
             /** @var $data GiaoDich */
             return $data->trang_thai ? '<b>'.GiaoDich::getListTTGDDisplay()[$data->trang_thai].'</b>'
                 : '<i>Đang cập nhật</i>';
         },
         'format' => 'raw',
         'header' => 'Trạng thái',
         'headerOptions' => ['class' => 'text-primary text-nowrap'],
         'width' =>'10%'
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'tien_do',
         'filter' => Html::activeDropDownList($searchModel, 'tien_do', GiaoDich::getListTDGD(), [
             'class' => 'form-control text-nowrap', 'prompt' => '-- Chọn --'
         ]),
         'width' => '10%',
         'value' => function($data){
             /** @var $data GiaoDich */
             return $data->tien_do ? '<b>'.GiaoDich::getListTDGDDisplay()[$data->tien_do].'</b>'
                 : '<i>Đang cập nhật</i>';
         },
         'format' => 'raw',
         'header' => 'Từ chối đi',
         'headerOptions' => ['class' => 'text-primary text-nowrap'],
     ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'active',
    // ],
    [
        'value' => function($data){
            return \yii\bootstrap\Html::a('<i class="fa fa-eye"></i>','#',
                ['class' => 'text-primary btn-xem-giao-dich', 'title'=>'View','data-toggle'=>'tooltip', 'data-value' =>$data->id]);
        },
        'label' => 'Xem',
        'format' => 'raw',
        'contentOptions' => ['class' => 'text-center','style'=>'width:3%;'],
        'headerOptions' => ['class' => 'text-center text-primary','style'=>'width:3%; vertical-align: inherit;', 'rowspan' =>2]
    ],
//    [
//        'value' => function($data){
//            return \yii\bootstrap\Html::a('<i class="fa fa-edit"></i>','#', ['class' => 'text-gray','role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip']);
//        },
//        'label' => 'Sửa',
//        'format' => 'raw',
//        'contentOptions' => ['class' => 'text-center','style'=>'width:3%;'],
//        'headerOptions' => ['class' => 'text-center','style'=>'width:3%;']
//    ],
//    [
//        'value' => function($data){
//            return \yii\bootstrap\Html::a('<i class="fa fa-trash"></i>','#', ['class' => 'text-gray','role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip']);
//        },
//        'label' => 'Xóa',
//        'format' => 'raw',
//        'contentOptions' => ['class' => 'text-center','style'=>'width:3%;'],
//        'headerOptions' => ['class' => 'text-center','style'=>'width:3%;']
//    ],

];   