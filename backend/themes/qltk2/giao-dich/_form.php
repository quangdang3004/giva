<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\GiaoDich */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="giao-dich-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_created')->textInput() ?>

    <?= $form->field($model, 'don_hang_id')->textInput() ?>

    <?= $form->field($model, 'nguoi_cung_cap_id')->textInput() ?>

    <?= $form->field($model, 'nguoi_nhan_id')->textInput() ?>

    <?= $form->field($model, 'type')->dropDownList([ 'Cung cấp ứng viên' => 'Cung cấp ứng viên', 'Trừ tiền' => 'Trừ tiền', 'Cộng tiền' => 'Cộng tiền', 'Chuyển tiền' => 'Chuyển tiền', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'so_luong')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'don_vi')->dropDownList([ 'Xu' => 'Xu', 'Úng viên' => 'Úng viên', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'trang_thai')->dropDownList([ 'Yêu cầu' => 'Yêu cầu', 'Xác nhận' => 'Xác nhận', 'Từ chối' => 'Từ chối', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'tien_do')->dropDownList([ 'Chưa hoàn thành' => 'Chưa hoàn thành', 'Đã hoàn thành' => 'Đã hoàn thành', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'active')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
