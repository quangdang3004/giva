<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\GiaoDich */
?>
<div class="giao-dich-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_created',
            'don_hang_id',
            'nguoi_cung_cap_id',
            'nguoi_nhan_id',
            'type',
            'so_luong',
            'don_vi',
            'trang_thai',
            'tien_do',
            'active',
        ],
    ]) ?>

</div>
