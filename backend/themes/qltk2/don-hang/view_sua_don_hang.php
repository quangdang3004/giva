<?php
/** @var $donHang QuanLyDonHang */

use backend\models\DanhMuc;
use backend\models\QuanLyDonHang;
use kartik\form\ActiveForm;
use yii\helpers\Html;

?>
<?php $form =  ActiveForm::begin(['options' => ['id' => 'form-sua-don-hang']]) ?>
<?= Html::hiddenInput('don_hang_id', $donHang->id, ['id' => 'don-hang-id']) ?>
<?= Html::hiddenInput('nganh_nghe_id', $donHang->nganh_nghe_id) ?>
<h4 class="text-primary"><strong>THÔNG TIN ĐƠN HÀNG</strong></h4>
<div class="row">
    <div class="col-md-2"><strong>Id đơn hàng: </strong><?=$donHang->id?></div>
    <div class="col-md-3"><strong>Ngày tạo: </strong><?=$donHang->created ? date('d/m/y', strtotime( $donHang->created))
            : '<i class="text-muted">Chưa có thông tin</i>'?></div>
    <div class="col-md-4"><strong>Ngày hết hạn: </strong><?=$donHang->updated ? date('d/m/y', strtotime( $donHang->updated))
            : '<i class="text-muted">Chưa có thông tin</i>'?></div>
</div>
<h5 class="text-primary"><strong>YÊU CẦU ĐƠN HÀNG</strong></h5>
<div class="row">
    <div class="col-md-3">
        <strong><?=Html::label('Trạng thái')?> </strong>
        <?= Html::activeDropDownList($donHang,'trang_thai', QuanLyDonHang::getListTrangThaiDonHang(), ['class' => 'form-control custom-input']) ?>
    </div>
    <div class="col-md-3">
        <strong> <?=Html::label('Nghề nghiệp')?></strong>
        <?= Html::activeDropDownList($donHang,'nganh_nghe_id', DanhMuc::getListNgheNghiep(), ['class' => 'form-control custom-input']) ?>
    </div>
    <div class="col-md-3">
        <strong><?=Html::label('Trình độ')?> </strong>
        <?= Html::activeTextInput($donHang,'trinh_do', ['class' => 'form-control custom-input']) ?>
    </div>
    <div class="col-md-3">
        <strong><?=Html::label('Tên thành phố, khu vực')?> </strong>
        <?= Html::activeDropDownList($donHang,'tinh_thanh_id', DanhMuc::getListThanhPho(), ['class' => 'form-control custom-input']) ?>
    </div>

</div>
<div class="row">
    <div class="col-md-3">
        <strong><?=Html::label('Số lượng cần tuyển')?> </strong>
        <?= Html::activeTextInput($donHang,'so_luong_can_tuyen', ['class' => 'form-control custom-input']) ?>
    </div>
    <div class="col-md-3">
        <strong><?=Html::label('Số lượng đã tuyển')?> </strong>
        <?= Html::activeTextInput($donHang,'so_luong_da_tuyen', ['class' => 'form-control custom-input']) ?>
    </div>
    <div class="col-md-3">
        <strong><?=Html::label('Tuổi từ')?> </strong>
        <?= Html::activeTextInput($donHang,'tuoi_tu', ['class' => 'form-control custom-input']) ?>
    </div>
    <div class="col-md-3">
        <strong><?= Html::label('đến')?>  </strong>
        <?= Html::activeTextInput($donHang,'tuoi_den', ['class' => 'form-control custom-input']) ?>
    </div>
</div>
<div class="row">

    <div class="col-md-3" style="margin: 0 !important;">
        <?= \common\models\myAPI::activeDateField4($form, $donHang, 'han_tuyen', 'Hạn tuyển'
            , '1950:2050',  ['class' => 'form-control custom-input']) ?>
    </div>
    <div class="col-md-9">
        <strong><?=Html::label('Ghi chú')?> </strong>
        <?= Html::activeTextarea($donHang,'ghi_chu_cho_don', ['class' => 'form-control custom-input']) ?>
    </div>
</div>

<h5 class="text-primary"><strong>YÊU CẦU ỨNG VIÊN</strong></h5>
<div class="row">
    <div class="col-md-3"><strong><?=Html::label('Xăm trổ')?> </strong>
        <?= Html::activeDropDownList($donHang,'xam_tro', [ 0 => 'Không', 1=>'Có' ],['class' => 'form-control custom-input']) ?>
    </div>
    <div class="col-md-3"><strong><?=Html::label('Viêm gan B')?></strong>
        <?= Html::activeDropDownList($donHang,'viem_gan_b',[ 0 => 'Không', 1=>'Có' ], ['class' => 'form-control custom-input']) ?>
    </div>
    <div class="col-md-3"><strong><?=Html::label('Chi phí')?></strong>
        <?= Html::activeTextInput($donHang,'phi_don_hang', ['class' => 'form-control custom-input']) ?>
    </div>
    <div class="col-md-3"><strong><?=Html::label('Tiền hỗ trợ')?> </strong>
        <?= Html::activeTextInput($donHang,'tien_ho_tro', ['class' => 'form-control custom-input']) ?>
    </div>
</div>
<div class="row">

    <div class="col-md-3" style="margin: 0 !important;">
        <?= \common\models\myAPI::activeDateField4($form, $donHang, 'ngay_thi', 'Ngày thi'
            , '1950:2050',  ['class' => 'form-control custom-input']) ?>
    </div>


    <div class="col-md-9"><strong><?=Html::label('Ghi chú')?> </strong>
        <?= Html::activeTextarea($donHang,'ghi_chu', ['class' => 'form-control custom-input']) ?>
    </div>
</div>
<div class="form-group text-right">
    <?= Html::a('<i class="fa fa-save"></i> Lưu lại', '#', ['class' => 'btn btn-primary', 'id' => 'luu-chi-tiet-don-hang']) ?>
</div>
<?php ActiveForm::end(); ?>
<style>
    .custom-input{
        margin-bottom: 10px;
    }
    .field-quanlydonhang-han_tuyen, .field-quanlydonhang-ngay_thi{
        margin: 0;
    }
</style>