<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\DonHang */
?>
<div class="don-hang-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
