<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\DonHang */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="don-hang-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cong_ty_id')->textInput() ?>

    <?= $form->field($model, 'nganh_nghe_id')->textInput() ?>

    <?= $form->field($model, 'bao_gia')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gioi_tinh')->dropDownList([ 'Nam' => 'Nam', 'Nữ' => 'Nữ', 'Nam/Nữ' => 'Nam/Nữ', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'trinh_do')->dropDownList([ 'N1' => 'N1', 'N2' => 'N2', 'N3' => 'N3', 'N4' => 'N4', 'N5' => 'N5', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'ghi_chu')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'so_luong_can_tuyen')->textInput() ?>

    <?= $form->field($model, 'so_luong_da_tuyen')->textInput() ?>

    <?= $form->field($model, 'created')->textInput() ?>

    <?= $form->field($model, 'updated')->textInput() ?>

    <?= $form->field($model, 'han_tuyen')->textInput() ?>

    <?= $form->field($model, 'parent_id')->textInput() ?>

    <?= $form->field($model, 'trang_thai')->dropDownList([ 'Đang tuyển' => 'Đang tuyển', 'Đã đủ' => 'Đã đủ', 'Hết hạn' => 'Hết hạn', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'active')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
