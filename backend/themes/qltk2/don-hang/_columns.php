<?php
/** @var $searchModel \backend\models\search\QuanLyDonHangSearch */

use backend\models\CongTy;
use backend\models\QuanLyDonHang;
use common\models\myAPI;
use yii\helpers\Html;
use yii\helpers\Url;

return [
//    [
//        'class' => 'kartik\grid\CheckboxColumn',
//        'width' => '1%',
//    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '1%',
        'header' => 'STT',
        'headerOptions' => ['class' => 'text-center text-primary']
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created',
        'label' => 'Ngày tạo',
        'width' => '1%',
        'headerOptions' => ['class' => 'text-center'],
        'contentOptions' => ['class' => 'text-center text-nowrap'],
        'value' => function($data){
            /** @var $data QuanLyDonHang */
            return $data->created!='' ? date('d/m/Y', strtotime($data->created))
                : "<i class='text-muted'>Chưa có thông tin</i>";
        },
        'format' => 'raw',
        'filter' => myAPI::activeDateFieldNoLabel($searchModel, 'created', (date('Y') - 3).':'.(date('Y') + 1),
            ['class' => 'form-control created-date-field','autocomplete' => 'off', 'placeholder' => 'Ngày tạo'])
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'han_tuyen',
        'label' => 'Ngày hết hạn',
        'width' => '1%',

        'headerOptions' => ['class' => 'text-center text-nowrap'],
        'contentOptions' => ['class' => 'text-center text-nowrap'],
        'value' => function($data){
            /** @var $data QuanLyDonHang */
            return $data->han_tuyen!='' ? date('d/m/Y', strtotime($data->han_tuyen))
                : "<i class='text-muted'>Chưa có thông tin</i>";
        },
        'format' => 'raw',
        'filter' =>   myAPI::activeDateFieldNoLabel($searchModel, 'han_tuyen', (date('Y') - 3).':'.(date('Y') + 1),
            ['class' => 'form-control  created-date-field','autocomplete' => 'off','placeholder' => 'Hạn tuyển'])
    ],

    //  YÊU CẦU ĐƠN HÀNG
    [
        'label' => 'Yêu cầu đơn hàng',
//        'width'=>'1%',
        'class'=>'\kartik\grid\DataColumn',
        'headerOptions' => ['class' => 'text-primary text-center'],
        'contentOptions' => ['class' => 'text-nowrap'],
        'value' => function($data){
            /** @var $data QuanLyDonHang */
            return implode('<br />', [
                "<strong>Trạng thái: </strong> ".QuanLyDonHang::getListTrangThaiDonHangDisplay()[$data->trang_thai],
                "<strong>Nghề nghiệp: </strong> ".($data->ten_nghe_nghiep!=''? $data->ten_nghe_nghiep
                        : "<i class='text-muted'>Chưa có thông tin</i>"),
                "<strong>Trình độ: </strong> ". ($data->trinh_do!=''? $data->trinh_do
                        : "<i class='text-muted'>Chưa có thông tin</i>"),
                "<strong>Thành phố: </strong> ".($data->ten_tinh_thanh!=''? $data->ten_tinh_thanh
                        : "<i class='text-muted'>Chưa có thông tin</i>"),
                "<strong>Khu vực: </strong> ".($data->ten_khu_vuc!=''? $data->ten_khu_vuc
                        : "<i class='text-muted'>Chưa có thông tin</i>"),
                ])
                ."</br>". "<strong>Tuổi:&nbsp;</strong>".$data->tuoi_tu."&ensp;-&ensp;".$data->tuoi_den
                    .'</br>'
                . implode('<br/>',[
                    "<strong>Số lượng cần tuyển: </strong> ".($data->so_luong_can_tuyen!=''? $data->so_luong_can_tuyen
                        : "<i class='text-muted'>Chưa có thông tin</i>"),
                    "<strong>Số lượng đã tuyển: </strong> ".($data->so_luong_da_tuyen!=''? $data->so_luong_da_tuyen
                        : "<i class='text-muted'>Chưa có thông tin</i>"),
                    "<strong>Ghi chú: </strong> ".($data->ghi_chu_cho_don!=''? $data->ghi_chu_cho_don
                        : "<i class='text-muted'>Chưa có thông tin</i>")]
                     );
        },
        'format' => 'raw',
        'filter' => Html::activeDropDownList($searchModel, 'trang_thai', QuanLyDonHang::getListTrangThaiDonHang(),
                ['class' => 'form-control', 'placeholder' => 'Trạng thái', 'prompt' => '-- Chọn trạng thái --']).
            Html::activeDropDownList($searchModel, 'ten_nghe_nghiep', \backend\models\DanhMuc::getNgheNghiep(), ['prompt' => '-- Chọn nghề nghiệp --', 'class' => 'form-control']).
            Html::activeDropDownList($searchModel, 'trinh_do', \backend\models\DonHang::getListTrinhDo(), ['prompt' => '-- Chọn trình độ --', 'class' => 'form-control']).
            Html::activeDropDownList($searchModel, 'ten_khu_vuc', \backend\models\DanhMuc::getListKhuVuc(), ['prompt' => '-- Chọn khu vực --', 'class' => 'form-control']).
            Html::activeDropDownList($searchModel, 'ten_tinh_thanh', \backend\models\DanhMuc::getThanhPho(), ['prompt' => '-- Chọn thành phố --', 'class' => 'form-control ten-tinh-thanh-dropdown']).
            Html::activeTextInput($searchModel, 'tuoi_tu', ['class' => 'form-control', 'placeholder' => 'Tuổi từ']).
            Html::activeTextInput($searchModel, 'tuoi_den', ['class' => 'form-control', 'placeholder' => 'Đến'])
    ],

    // THÔNG TIN ỨNG VIÊN
    [
        'label' => 'Yêu cầu ứng viên',
        'class'=>'\kartik\grid\DataColumn',
//        'width' => '10%',
        'headerOptions' => ['class' => 'text-primary text-center'],
        'value' => function($data){
            /** @var $data QuanLyDonHang */
            return implode('<br />', [
                    "<strong>Viêm gan B: </strong> ".($data->viem_gan_b == 1 ? 'Có' : 'Không'),
                    "<strong>Xăm trổ </strong> ".($data->xam_tro == 1 ? 'Có' : 'Không'),
                    "<strong>Chi phí: </strong> ". ($data->phi_don_hang!=''? number_format($data->phi_don_hang, 0, '.', ',')
                        : "<i class='text-muted'>Chưa có thông tin</i>"),
                    "<strong>Tiền hỗ trợ được nhận: </strong> ".($data->tien_ho_tro!=''? $data->tien_ho_tro
                        : "<i class='text-muted'>Chưa có thông tin</i>"),
                    "<strong>Ngày thi: </strong> ".($data->ngay_thi!=''? date('d/m/Y', strtotime($data->ngay_thi))
                        : "<i class='text-muted'>Chưa có thông tin</i>"),
                    "<strong>Ghi chú: </strong> ".($data->ghi_chu!=''? $data->ghi_chu
                        : "<i class='text-muted'>Chưa có thông tin</i>"),
                ]);
        },
        'format' => 'raw',
        'filter' => myAPI::activeDateFieldNoLabel($searchModel, 'ngay_thi', (date('Y') - 3).':'.(date('Y') + 1),
            ['class' => 'form-control  created-date-field','autocomplete' => 'off', 'placeholder' => 'Ngày thi']).
            Html::activeDropDownList($searchModel, 'viem_gan_b', [1 => 'Viêm gan B: Có', 0 => 'Viêm gan B: Không'],
                ['class' => 'form-control', 'prompt' => '-- Viêm gan b --']).
            Html::activeDropDownList($searchModel, 'xam_tro', [1 => 'Xăm trổ: Có', 0 => 'Xăm trổ: Không'],
                ['class' => 'form-control', 'prompt' => '-- Xăm trổ --']).
            Html::activeTextInput($searchModel, 'phi_don_hang',
                ['class' => 'form-control', 'placeholder' => 'Chi phí từ']).
            Html::activeTextInput($searchModel, 'phi_don_hang_den',
                ['class' => 'form-control', 'placeholder' => 'đến'])
    ],


    // NHÀ TUYỂN DỤNG
    /** ten_nha_tuyen_dung
    ten_doanh_nghiep
    trang_thai_nha_tuyen_dung
    dien_thoai_nha_tuyen_dung
    email_nha_tuyen_dung
    avatar_nha_tuyen_dung
     */
    [
        'label' => 'Thông tin nhà tuyển dụng',
        'class'=>'\kartik\grid\DataColumn',
//        'width' => '10%',
        'headerOptions' => ['class' => 'text-primary text-center'],
        'value' => function($data){
            /** @var $data QuanLyDonHang */
            return implode('<br />', [
                "<strong>Tên nhà tuyển dụng: </strong> ".($data->ten_nha_tuyen_dung!='' ? $data->ten_nha_tuyen_dung
                    : "<i class='text-muted'>Chưa có thông tin</i>"),
                "<strong>Tên doanh nghiệp: </strong> ".($data->ten_doanh_nghiep!='' ? $data->ten_doanh_nghiep
                    : "<i class='text-muted'>Chưa có thông tin</i>"),
                "<strong>Trạng thái: </strong> ".($data->trang_thai_nha_tuyen_dung!='' ? \backend\models\User::getListUserDisplay()[$data->trang_thai_nha_tuyen_dung]
                    : "<i class='text-muted'>Chưa có thông tin</i>"),
                "<strong>Điện thoại: </strong> ".($data->dien_thoai_nha_tuyen_dung!='' ? $data->dien_thoai_nha_tuyen_dung
                    : "<i class='text-muted'>Chưa có thông tin</i>"),
                "<strong>Email: </strong> ".($data->email_nha_tuyen_dung!='' ? $data->email_nha_tuyen_dung
                    : "<i class='text-muted'>Chưa có thông tin</i>"),

            ]);
        },
        'format' => 'raw',
        'filter' => Html::activeTextInput($searchModel, 'ten_nha_tuyen_dung', ['class' => 'form-control',
            'placeholder' => 'Tên nhà tuyển dụng']).
            Html::activeTextInput($searchModel, 'ten_doanh_nghiep', ['class' => 'form-control',
                'placeholder' => 'Tên doanh nghiệp']).
             Html::activeTextInput($searchModel, 'dien_thoai_nha_tuyen_dung', ['class' => 'form-control',
                'placeholder' => 'Điện thoại nhà tuyển dụng']).
            Html::activeTextInput($searchModel, 'email_nha_tuyen_dung', ['class' => 'form-control',
                'placeholder' => 'Email nhà tuyển dụng'])

    ],

    // DOANH NGHIỆP
//    /**
//     * ten_doanh_nghiep
//    trang_thai_doanh_nghiep
//    dia_chi_doanh_nghiep
//    dien_thoai_doanh_nghiep
//    email_doanh_nghiep
//     */
//    [
//        'label' => 'Thông tin doanh nghiệp',
//        'class'=>'\kartik\grid\DataColumn',
//        'width' => '10%',
//        'headerOptions' => ['class' => 'text-primary text-center'],
//        'value' => function($data){
//            /** @var $data QuanLyDonHang */
//            return implode('<br />', [
//                "<strong>Tên doanh nghiệp: </strong> ".($data->ten_doanh_nghiep!='' ? $data->ten_doanh_nghiep
//                    : "<i class='text-muted'>Chưa có thông tin</i>"),
//                "<strong>Trạng thái: </strong> ".($data->trang_thai_doanh_nghiep!='' ? \backend\models\DoanhNghiep::getListTrangThaiDisplay()[$data->trang_thai_doanh_nghiep]
//                    : "<i class='text-muted'>Chưa có thông tin</i>"),
//                "<strong>Địa chỉ: </strong> ".($data->dia_chi_doanh_nghiep!='' ? $data->dia_chi_doanh_nghiep
//                    : "<i class='text-muted'>Chưa có thông tin</i>"),
//                "<strong>Điện thoại: </strong> ".($data->dien_thoai_doanh_nghiep!='' ? $data->dien_thoai_doanh_nghiep
//                    : "<i class='text-muted'>Chưa có thông tin</i>"),
//                "<strong>Email: </strong> ".($data->email_doanh_nghiep!='' ? $data->email_doanh_nghiep
//                    : "<i class='text-muted'>Chưa có thông tin</i>"),
//            ]);
//        },
//        'format' => 'raw',
//        'filter' => Html::activeTextInput($searchModel, 'ten_doanh_nghiep', ['class' => 'form-control',
//                'placeholder' => 'Tên doanh nghiệp']).
//            Html::activeDropDownList($searchModel, 'trang_thai_doanh_nghiep', \backend\models\User::getListTrangThai() ,
//                ['class' => 'form-control', 'placeholder' => 'Trạng thái doanh nghiệp', 'prompt' => '-- Chọn trạng thái --']).
//            Html::activeTextInput($searchModel, 'dia_chi_doanh_nghiep', ['class' => 'form-control',
//                'placeholder' => 'Địa chỉ doanh nghiệp']).
//            Html::activeTextInput($searchModel, 'dien_thoai_doanh_nghiep', ['class' => 'form-control',
//                'placeholder' => 'Điện thoại doanh nghiệp']).
//            Html::activeTextInput($searchModel, 'email_doanh_nghiep', ['class' => 'form-control',
//                'placeholder' => 'Email doanh nghiệp'])
//
//    ],

    // CÔNG TY
    /**
     * ten_cong_ty
    trang_thai_cong_ty
    dien_thoai_cong_ty
    email_cong_ty
    dia_chi_cong_ty
    logo_cong_ty
     */
    [
        'label' => 'Công ty phái cử',
        'class'=>'\kartik\grid\DataColumn',
//        'width' => '10%',
        'headerOptions' => ['class' => 'text-primary text-center'],
        'value' => function($data){
            /** @var $data QuanLyDonHang */
            return implode('<br />', [
                "<strong>Tên công ty: </strong> ".($data->ten_cong_ty!='' ? $data->ten_cong_ty
                    : "<i class='text-muted'>Chưa có thông tin</i>"),
                "<strong>Trạng thái: </strong> ".($data->trang_thai_cong_ty!='' ? CongTy::getListTrangThaiCongTyDisplay()[$data->trang_thai_cong_ty]
                    : "<i class='text-muted'>Chưa có thông tin</i>"),
                "<strong>Địa chỉ: </strong> ".($data->dia_chi_cong_ty!='' ? $data->dia_chi_cong_ty
                    : "<i class='text-muted'>Chưa có thông tin</i>"),
                "<strong>Điện thoại: </strong> ".($data->dien_thoai_cong_ty!='' ? $data->dien_thoai_cong_ty
                    : "<i class='text-muted'>Chưa có thông tin</i>"),
                "<strong>Email: </strong> ".($data->email_cong_ty!='' ? $data->email_cong_ty
                    : "<i class='text-muted'>Chưa có thông tin</i>"),
            ]);
        },
        'format' => 'raw',
        'filter' => Html::activeTextInput($searchModel, 'ten_cong_ty', ['class' => 'form-control',
                'placeholder' => 'Tên công ty']).
            Html::activeDropDownList($searchModel, 'trang_thai_cong_ty', \backend\models\User::getListTrangThai() ,
                ['class' => 'form-control', 'placeholder' => 'Trạng thái công ty', 'prompt' => '-- Chọn trạng thái --']).
            Html::activeTextInput($searchModel, 'dia_chi_cong_ty', ['class' => 'form-control',
                'placeholder' => 'Địa chỉ công ty']).
            Html::activeTextInput($searchModel, 'dien_thoai_cong_ty', ['class' => 'form-control',
                'placeholder' => 'Điện thoại công ty']).
            Html::activeTextInput($searchModel, 'email_cong_ty', ['class' => 'form-control',
                'placeholder' => 'Email công ty'])

    ],

    // SỬA
    [
        'value' => function($data){
            return \yii\bootstrap\Html::a('<i class="fa fa-edit"></i>','#',
                ['class' => 'text-gray btn-sua-don-hang', 'title'=>'Sửa đơn hàng', 'data-value' => $data->id]);
        },
        'label' => 'Sửa',
        'format' => 'raw',
        'contentOptions' => ['class' => 'text-center'],
        'headerOptions' => ['class' => 'text-center text-primary','style'=>'width:3%;vertical-align: inherit;',  'rowspan'=>2]
    ],
//    [
//        'value' => function($data){
//            return \yii\bootstrap\Html::a('<i class="fa fa-edit"></i>','#', ['class' => 'text-gray','role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip']);
//        },
//        'label' => 'Sửa',
//        'format' => 'raw',
//        'contentOptions' => ['class' => 'text-center','style'=>'width:3%;'],
//        'headerOptions' => ['class' => 'text-center','style'=>'width:3%;',  'rowspan'=>2]
//    ],
//    [
//        'value' => function($data){
//            return \yii\bootstrap\Html::a('<i class="fa fa-trash"></i>','#', ['class' => 'text-gray','role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip']);
//        },
//        'label' => 'Xóa',
//        'format' => 'raw',
//        'contentOptions' => ['class' => 'text-center','style'=>'width:3%;'],
//        'headerOptions' => ['class' => 'text-center','style'=>'width:3%;', 'rowspan'=>2 ]
//    ],
];   