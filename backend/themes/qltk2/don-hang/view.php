<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\DonHang */
?>
<div class="don-hang-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'cong_ty_id',
            'nganh_nghe_id',
            'bao_gia',
            'gioi_tinh',
            'trinh_do',
            'ghi_chu:ntext',
            'so_luong_can_tuyen',
            'so_luong_da_tuyen',
            'created',
            'updated',
            'han_tuyen',
            'parent_id',
            'trang_thai',
            'user_id',
            'active',
        ],
    ]) ?>

</div>

