<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\myAPI;


/* @var $this yii\web\View */
/* @var $model backend\models\BannerApp */

?>
<div>
    <?php $form = ActiveForm::begin(['id' => 'them_banner_moi']); ?>
    <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>
    <div class="row">
        <?php if($model->id != ''): ?>
            <div class="col-md-12 bannerapps-sua-banner">
                <a href="<?= \backend\models\User::getLinkAnh().$model->link ?>"><img src="<?= \backend\models\User::getLinkAnh().$model->link ?>" alt="Ảnh banner" width="200px"></a>
            </div>
        <?php endif; ?>
        <div class="col-md-8">
            <label for="anh_banner_app">Chọn ảnh</label>
            <input type="file" id="anh_banner_app" class="form-control" name="anh_banner" value="<?= \backend\models\User::getLinkAnh().$model->link ?>">
        </div>
        <div class="col-md-4">
            <label>Hạn sử dụng</label>
            <?=myAPI::activeDateFieldNoLabel($model, 'exp', '1940:'.'2100', ['class' => 'form-control  created-date-field','autocomplete' => 'off']); ?>
        </div>
    </div>

    <?= $form->field($model, 'title')->textInput()?>
    <?= $form->field($model, 'url_chuyen_huong')->textInput()?>
    <?php ActiveForm::end(); ?>
</div>
