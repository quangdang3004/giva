<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\BannerApp */
?>
<div class="banner-app-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'active',
            'title',
            'link:ntext',
            'created',
            'exp',
            'url_chuyen_huong:ntext',
        ],
    ]) ?>

</div>
