<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\BannerApp */
?>
<div class="banner-app-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
