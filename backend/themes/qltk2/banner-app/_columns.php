<?php

use backend\models\search\BannerAppSearch;
use yii\helpers\Url;
use \backend\models\User;
/** @var $searchModel BannerAppSearch */

return [
//    [
//        'class' => 'kartik\grid\CheckboxColumn',
//        'width' => '1%',
//    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '1%',
        'header' => 'STT',
        'headerOptions' => ['class' => 'text-primary']
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'link',
        'width' => '1%',
        'value' => function ($data){
            return \yii\helpers\Html::img(User::getLinkAnh().$data->link, ['style' => 'width: 80px']);
        },
        'header' => 'Ảnh banner',
        'headerOptions' => ['class' => 'text-nowrap text-primary'],
        'filter' => false,
        'format' => 'raw'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'title',
        'contentOptions' => ['class' => 'text-nowrap']
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created',
        'width' => '1%',
        'header' => 'Ngày tạo',
        'headerOptions' => ['class' => 'text-nowrap text-primary'],
        'contentOptions' => ['class' => 'text-nowrap'],
        'filter' => \common\models\myAPI::activeDateFieldNoLabel($searchModel, 'created', (date('Y') - 3).':'.(date('Y') + 1), ['class' => 'created-date-field form-control', 'autocomplete' => 'off']),
        'value' => function ($data) {
            return date('d/m/Y H:i:s', strtotime($data->created));
        }, //
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'exp',
        'width' => '1%',
        'contentOptions' => ['class' => 'text-nowrap'],
        'header' => 'Hạn sử dụng',
        'headerOptions' => ['class' => 'text-primary text-nowrap'],
        'filter' => \common\models\myAPI::activeDateFieldNoLabel($searchModel, 'exp', (date('Y') - 3).':'.'2100', ['class' => 'created-date-field form-control', 'autocomplete' => 'off']),
        'value' => function ($data) {
            return date('d/m/Y H:i:s', strtotime($data->exp));
        },
    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'url_chuyen_huong',
         'width' => '1%',
         'contentOptions' => ['class' => 'text-nowrap'],
         'headerOptions' => ['class' => 'text-nowrap'],
         'value' => function ($data){
                return \yii\helpers\Html::a($data->url_chuyen_huong, $data->url_chuyen_huong, ['target' => 'blank']);
         },
         'format' => 'raw'
     ],
    [
        'value' => function($data){
            return \yii\bootstrap\Html::a('<i class="fa fa-edit"></i>','#',
                ['class' => 'text-gray btn-sua-banner', 'title'=>'Update', 'data-toggle'=>'tooltip','data-value' =>$data->id ]);
        },
        'label' => 'Sửa',
        'format' => 'raw',
        'width' => '1%',
        'contentOptions' => ['class' => 'text-center','style'=>'width:3%;'],
        'headerOptions' => ['class' => 'text-primary text-center','style'=>'width:3%; vertical-align: inherit;', 'rowspan' =>2]
    ],
    [
        'value' => function($data){
            $model = \backend\models\DoanhNghiep::findOne($data->id);
            return \yii\bootstrap\Html::a('<i class="fa fa-trash"></i>','#',
                ['class' => 'text-danger btn-xoa-banner', 'title'=>'Delete', 'data-toggle'=>'tooltip','data-value' =>$data->id ]);

//            \yii\helpers\VarDumper::dump($model->active, 10, true); exit();
//            if($model->status == 10)
//                return \common\models\myAPI::createDeleteBtnInGrid(Url::toRoute(['doanh-nghiep/delete', 'id' => $data->id]));
        },
        'label' => 'Xóa',
        'format' => 'raw',
        'width' => '1%',
        'contentOptions' => ['class' => 'text-center text-danger','style'=>'width:3%;'],
        'headerOptions' => ['class' => 'text-primary text-center','style'=>'width:3%; vertical-align: inherit;', 'rowspan' =>2]
    ],

];   