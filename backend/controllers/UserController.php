<?php

namespace backend\controllers;

use backend\models\search\UserVaiTroSearch;
use backend\models\UserVaiTro;
use backend\models\Vaitrouser;
use common\models\myAPI;
use common\widgets\Alert;
use HttpException;
use Yii;
use common\models\User;
use backend\models\search\UserSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    public function behaviors()
    {

        $arr_action = ['index', 'view', 'create', 'update', 'delete', 'bulk-delete',
            'xem-nguoi-dung', 'update-nguoi-dung', 'cong-xu', 'xoa-nguoi-dung'];
        $rules = [];
        foreach ($arr_action as $item) {
            $rules[] = [
                'actions' => [$item],
                'allow' => true,
                'matchCallback' => function ($rule, $action) {
                    $action_name = strtolower(str_replace('action', '', $action->id));
                    return \Yii::$app->user->id == 1 || myAPI::isAccess2('User', $action_name);
                }
            ];
        }

        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex($doi_tuong = null)
    {
        $searchModel = new UserVaiTroSearch();

        if (!is_null($doi_tuong)){
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $doi_tuong);
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'doi_tuong' => $doi_tuong,
            ]);
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index_thanh_vien', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $name = $this->findModel($id)->hoten;
            return [
                'title' => "Thông tin tài khoản" . " " . $name,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('<i class="fa fa-edit"></i> Cập nhật', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new User model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new User();
        $vaitros = [];
        $vaitrouser = new Vaitrouser();

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Thêm thành viên mới",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                        'vaitros' => $vaitros,
                        'vaitrouser' => $vaitrouser,
                    ]),
                    'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('<i class="fa fa-save"></i> Lưu lại', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else if ($model->load($request->post())) {
                if ($model->username == '') {
                    return [
                        'title' => "Thêm thành viên mới",
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                            'vaitros' => $vaitros,
                            'vaitrouser' => $vaitrouser
                        ]),
                        'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('<i class="fa fa-save"></i> Lưu lại', ['class' => 'btn btn-primary', 'type' => "submit"])

                    ];
                } else {
                    $user = User::findOne(['username' => $model->username]);
                    if (isset($user)) {
                        return [
                            'title' => 'Lỗi',
                            'content' => 'Username đã tồn tại!',
                            'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-right', 'data-dismiss' => "modal"])
                        ];
                    } else {
                        if ($model->save())
                            return [
                                'forceReload' => '#crud-datatable-pjax',
                                'title' => "Thêm thành viên mới",
                                'content' => '<span class="text-success">Thêm mới thành viên thành công</span>',
                                'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                    Html::a('<i class="glyphicon glyphicon-plus"></i> Tạo thêm', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                            ];
                        else
                            throw new \yii\web\HttpException(500, myAPI::getMessage('danger', Html::errorSummary($model)));
                    }
                }
            } else {
                return [
                    'title' => "Thêm thành viên mới",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                        'vaitros' => $vaitros,
                        'vaitrouser' => $vaitrouser
                    ]),
                    'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('<i class="fa fa-save"></i> Lưu lại', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing User model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        $vaitros = ArrayHelper::map(Vaitrouser::findAll(['user_id' => $id]), 'vaitro_id', 'vaitro_id');
        $vaitrouser = new Vaitrouser();
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Thông tin thành viên",
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                        'vaitros' => $vaitros,
                        'vaitrouser' => $vaitrouser,

                    ]),
                    'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('<i class="fa fa-save"></i> Lưu lại', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post())) {
                if ($model->id == 1) {
                    if (Yii::$app->user->id != 1) {
                        echo Json::encode(['message' => myAPI::getMessage('danger', 'Bạn không có quyền thực hiện chức năng này')]);
                        exit;
                    }
                }

                if ($model->save()) {
                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => "Thông tin thành viên",
                        'content' => $this->renderAjax('view', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('<i class="fa fa-edit"></i> Chỉnh sửa', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                    ];
                } else {
                    return [
                        'title' => "Cập nhật thông tin thành viên " . $model->hoten,
                        'content' => Html::errorSummary($model),
//                        'content'=>$this->renderAjax('update', [
//                            'model' => $model,
//                            'vaitros' => $vaitros,
//                            'vaitrouser' => $vaitrouser
//                        ]),
                        'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('<i class="fa fa-save"></i> Lưu lại', ['class' => 'btn btn-primary', 'type' => "submit"])
                    ];
                }
            } else {
                return [
                    'title' => "Chỉnh sửa thông tin thành viên",
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                        'vaitros' => $vaitros,
                        'vaitrouser' => $vaitrouser
                    ]),
                    'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('<i class="fa fa-save"></i> Lưu lại', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing User model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if ($id != 1 || ($id == 1 && Yii::$app->user->id == 1)) {
            $request = Yii::$app->request;
            $this->findModel($id)->delete();

            if ($request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
            } else {
                return $this->redirect(['index']);
            }
        } else {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        }


    }

    /**
     * Delete multiple existing User model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkdelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionXemNguoiDung()
    {
        if (Yii::$app->request->isAjax) {
            if (isset($_POST['id'])) {
                $user = User::findOne($_POST['id']);

                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'title' => '<h3 >Thông tin người dùng</h3>',
                    'content' => $this->renderAjax('xem_nguoi_dung', ['user' => $user]),
                ];
            } else {
                throw new HttpException(500, 'Không xác thực được dữ liệu gửi lên');
            }
        } else {
            throw new NotFoundHttpException('Đường dẫn sai cú pháp');
        }
    }

    public function actionUpdateNguoiDung()
    {
//        VarDumper::dump('123', 10, true);exit();
        if (Yii::$app->request->isAjax) {
            if (isset($_POST['id'])) {
                $user = User::findOne($_POST['id']);
                $model = $this->findModel($_POST['id']);
                $name = $model->hoten;
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'title' => 'Cập nhật thông tin người dùng ' . $name,
                    'content' => $this->renderAjax('update_nguoi_dung', ['model' => $user]),
                ];
            } else {
                throw new HttpException(500, 'Không xác thực được dữ liệu gửi lên');
            }
        } else {
            throw new NotFoundHttpException('Đường dẫn sai cú pháp');
        }
    }

    public function actionCongXu()
    {
        if (isset($_POST['so_xu'])){
            if ($_POST['so_xu'] == '' || $_POST['so_xu'] <= 0){
                throw new \yii\web\HttpException(500, 'Vui lòng nhập số xu lớn hơn 0');
            }
        }
        $user = User::findOne(['id' => $_POST['id']]);
        $soXu = abs($_POST['so_xu']);
        $user->updateAttributes(['so_du' => $user->so_du + $soXu]);

        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => "Cộng {$soXu} xu thành công",
        ];
    }

    //xoa-nguoi-dung
    public function actionXoaNguoiDung()
    {
        $user = User::findOne([$_POST['id']]);
        if (!is_null($user)) {
            if ($user->active != 0) {
                $user->updateAttributes(['active' => 0]);

                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'title' => 'Thông báo!',
                    'content' => "Xóa người dùng {$user->username} thành công"
                ];
            }
        }
        throw new \yii\web\HttpException(500, 'Người dùng không tồn tại');
    }

}
