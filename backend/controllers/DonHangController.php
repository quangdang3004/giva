<?php

namespace backend\controllers;

use backend\models\DanhMuc;
use backend\models\Hopdong;
use backend\models\QuanLyDonHang;
use backend\models\search\QuanLyDonHangSearch;
use common\models\myAPI;
use Yii;
use backend\models\DonHang;
use backend\models\search\DonHangSearch;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * DonHangController implements the CRUD actions for DonHang model.
 */
class DonHangController extends CoreController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $arr_action = ['index', 'view','create','update','delete', 'bulk-delete',
            'chi-tiet-don-hang', 'luu-chi-tiet-don-hang', 'sua-don-hang'];
        $rules = [];
        foreach ($arr_action as $item) {
            $rules[] = [
                'actions' => [$item],
                'allow' => true,
//                'matchCallback' => myAPI::isAccess2($controller, $item)
                'matchCallback' => function ($rule, $action) {
                    $action_name =  strtolower(str_replace('action', '', $action->id));
                    return \Yii::$app->user->id == 1 || myAPI::isAccess2('DonHang', $action_name);
                }
            ];
        }
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all DonHang models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new QuanLyDonHangSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single DonHang model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $name = $this->findModel($id)->id;
            return [
                    'title'=> "Thông tin đơn hàng"." ".$name,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('<i class="fa fa-close"></i> Đóng lại',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('<i class="fa fa-edit"></i> Cập nhật',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new DonHang model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new DonHang();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new DonHang",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Create new DonHang",
                    'content'=>'<span class="text-success">Create DonHang success</span>',
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Create new DonHang",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing DonHang model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update DonHang #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "DonHang #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Update DonHang #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing DonHang model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing DonHang model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkdelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the DonHang model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DonHang the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DonHang::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionChiTietDonHang()
    {
        if(Yii::$app->request->isAjax){
            if(isset($_POST['id'])){
                $user = DonHang::findOne($_POST['id']);

                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'title' => '<h3 >Thông tin đơn hàng</h3>',
                    'content' => $this->renderAjax('chi_tiet_don_hang', ['model' => $user]),
                ];
            }else{
                throw new \yii\web\HttpException(500, 'Không xác thực được dữ liệu gửi lên');
            }
        }else{
            throw new NotFoundHttpException('Đường dẫn sai cú pháp');
        }
    }

    public function actionLuuChiTietDonHang(){
        /**
         *
         * 'don_hang_id' => '44'
        'QuanLyDonHang' => [
            'trang_thai' => 'Đang tuyển'
            'ten_nghe_nghiep' => 'Hàn xì'
            'trinh_do' => 'Cấp 3'
            'ten_khu_vuc' => 'Chubu'
            'so_luong_can_tuyen' => '100'
            'so_luong_da_tuyen' => '0'
            'tuoi_tu' => '18'
            'tuoi_den' => '22'
            'han_tuyen' => 'Sep 1, 2022'
            'ghi_chu_cho_don' => ''
            'xam_tro' => '0'
            'viem_gan_b' => '0'
            'phi_don_hang' => '1'
            'tien_ho_tro' => '1'
            'ngay_thi' => 'Sep 12, 2022'
            'ghi_chu' => ''
        ]
         */
//        VarDumper::dump($_POST, 10, true);exit();
        if (Yii::$app->request->isAjax) {
            if (isset($_POST['don_hang_id'])) {
                $donHang = DonHang::findOne($_POST['don_hang_id']);
                $donHang->trang_thai  = $_POST['QuanLyDonHang']['trang_thai'];
//                $donHang->nganh_nghe_id  = $_POST['nganh_nghe_id'];
                // tìm nghe co id  = $_POST['nganh_nghe_id']
//                VarDumper::dump($donHang->nganh_nghe_id, 10, true);exit();

                $donHang->nganh_nghe_id = $_POST['QuanLyDonHang']['nganh_nghe_id'];

                $donHang->trinh_do  = $_POST['QuanLyDonHang']['trinh_do'];
                $donHang->tinh_thanh_id  = $_POST['QuanLyDonHang']['tinh_thanh_id'];
                $donHang->so_luong_can_tuyen  = $_POST['QuanLyDonHang']['so_luong_can_tuyen'];
                $donHang->so_luong_da_tuyen  = $_POST['QuanLyDonHang']['so_luong_da_tuyen'];
                $donHang->tuoi_tu  = $_POST['QuanLyDonHang']['tuoi_tu'];
                $donHang->tuoi_den  = $_POST['QuanLyDonHang']['tuoi_den'];
//                VarDumper::dump( $_POST['QuanLyDonHang']['han_tuyen'], 10, true);exit();
                $donHang->han_tuyen  = $_POST['QuanLyDonHang']['han_tuyen'];
//                $donHang->ghi_chu_cho_don  = $_POST['QuanLyDonHang']['ghi_chu_cho_don'];
                $donHang->xam_tro  = $_POST['QuanLyDonHang']['xam_tro'];
                $donHang->ngay_thi  = $_POST['QuanLyDonHang']['ngay_thi'];
                $donHang->viem_gan_b  = $_POST['QuanLyDonHang']['viem_gan_b'];
                $donHang->phi_don_hang  = $_POST['QuanLyDonHang']['phi_don_hang'];
                $donHang->tien_ho_tro  = $_POST['QuanLyDonHang']['tien_ho_tro'];
                $donHang->ghi_chu  = $_POST['QuanLyDonHang']['ghi_chu'];
                if (!$donHang->save()) {
                    throw new HttpException(500, Html::errorSummary($donHang));
                }else{
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return [
                        'title' => 'Lưu đơn hàng',
                        'content' => 'Lưu đơn hàng thành công',
                    ];
                }

            } else {
                throw new HttpException(500, 'Không xác thực được dữ liệu');
            }
        } else {
            throw new HttpException(500, 'Đường dẫn sai cú pháp');
        }
    }

    /** sua-don-hang */
    public function actionSuaDonHang(){
        $donHang = QuanLyDonHang::find()->andFilterWhere(['id' => $_POST['id_donhang']])->one();
//        VarDumper::dump($donHang, 10, true);exit();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => $this->renderAjax('view_sua_don_hang', [
                'donHang' => $donHang,
            ] ),
            'header' => 'Cập nhật thông tin đơn hàng'
        ];
    }
}
