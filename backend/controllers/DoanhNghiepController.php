<?php

namespace backend\controllers;

use backend\models\QuanLyDoanhNghiep;
use backend\models\search\QuanLyDoanhNghiepSearch;
use common\models\myAPI;
use HttpException;
use Yii;
use backend\models\DoanhNghiep;
use backend\models\search\DoanhNghiepSearch;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * DoanhNghiepController implements the CRUD actions for DoanhNghiep model.
 */
class DoanhNghiepController extends CoreController
{

    public function behaviors()
    {
        $arr_action = ['index', 'view', 'create', 'update', 'delete', 'doanh-nghiep-chua-xac-minh',
            'doanh-nghiep-cho-xac-minh',
            'doanh-nghiep-da-xac-minh', 'doanh-nghiep-tu-choi', 'xem-doanh-nghiep', 'tao-moi-doanh-nghiep',
            'update-chi-tiet-doanh-nghiep', 'update-doanh-nghiep'];
        $rules = [];
        foreach ($arr_action as $item) {
            $rules[] = [
                'actions' => [$item],
                'allow' => true,
                'matchCallback' => function ($rule, $action) {
                    $action_name =  strtolower(str_replace('action', '', $action->id));
                    return \Yii::$app->user->id == 1 || myAPI::isAccess2('DoanhNghiep', $action_name);
                }
            ];
        }

        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

//    public function behaviors()
//    {
//        return [
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'delete' => ['post'],
//                    'bulkdelete' => ['post'],
//                ],
//            ],
//        ];
//    }

    /**
     * Lists all DoanhNghiep models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new QuanLyDoanhNghiepSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /** view */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "DoanhNghiep #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Edit', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /** create */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new DoanhNghiep();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Create new DoanhNghiep",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Save', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Create new DoanhNghiep",
                    'content' => '<span class="text-success">Create DoanhNghiep success</span>',
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Create More', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                ];
            } else {
                return [
                    'title' => "Create new DoanhNghiep",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Save', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /** update */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Update DoanhNghiep #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Save', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "DoanhNghiep #" . $id,
                    'content' => $this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Edit', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => "Update DoanhNghiep #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Save', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /** delete */
    public function actionDelete()
    {
        $request = Yii::$app->request;
        $this->findModel($_POST['id'])->updateAttributes(['status' => 0]);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['title' => 'Thông báo',
                'content' => 'Xóa thành công doanh nghiệp'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /** bulk-delete */
    public function actionBulkdelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**  */
    protected function findModel($id)
    {
        if (($model = DoanhNghiep::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /** doanh-nghiep-chua-xac-minh */
    public function actionDoanhNghiepChuaXacMinh($type)
    {
//        VarDumper::dump($type, 10 , true); exit();
        $searchModel = new DoanhNghiepSearch();
        $dataProvider = $searchModel->searchDoanhNghiep(Yii::$app->request->queryParams, $type);
        return $this->render('doanh_nghiep_chua_xac_minh', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'type' => $type
        ]);
    }

    /** doanh-nghiep-cho-xac-minh */
    public function actionDoanhNghiepChoXacMinh($type)
    {
        $searchModel = new DoanhNghiepSearch();
        $dataProvider = $searchModel->searchDoanhNghiep(Yii::$app->request->queryParams, $type);

        return $this->render('doanh_nghiep_cho_xac_minh', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'type' => $type
        ]);
    }

    /** doanh-nghiep-da-xac-minh */
    public function actionDoanhNghiepDaXacMinh($type)
    {
        $searchModel = new DoanhNghiepSearch();
        $dataProvider = $searchModel->searchDoanhNghiep(Yii::$app->request->queryParams, $type);

        return $this->render('doanh-nghiep-da-xac-minh', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'type' => $type
        ]);
    }

    /** doanh-nghiep-tu-choi */
    public function actionDoanhNghiepTuChoi($type)
    {
        $searchModel = new DoanhNghiepSearch();
        $dataProvider = $searchModel->searchDoanhNghiep(Yii::$app->request->queryParams, $type);

        return $this->render('doanh-nghiep-tu-choi', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'type' => $type
        ]);
    }

    /** xem-doanh-nghiep */
    public function actionXemDoanhNghiep()
    {
        if (Yii::$app->request->isAjax) {
            if (isset($_POST['id'])) {
                $user = QuanLyDoanhNghiep::findOne(['id' => $_POST['id']]);

                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'title' => '<h3 >Thông tin doanh nghiệp</h3>',
                    'content' => $this->renderAjax('xem_doanh_nghiep', ['model' => $user]),
                ];
            } else {
                throw new \yii\web\HttpException(500, 'Không xác thực được dữ liệu gửi lên');
            }
        } else {
            throw new NotFoundHttpException('Đường dẫn sai cú pháp');
        }
    }

    /** update-doanh-nghiep */
    public function actionUpdateDoanhNghiep()
    {
        if (Yii::$app->request->isAjax) {
            if (isset($_POST['id'])) {
                $user = DoanhNghiep::findOne($_POST['id']);

                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'title' => '<h3 >Cập nhật doanh nghiệp</h3>',
                    'content' => $this->renderAjax('view_update_doanh_nghiep', ['model' => $user]),
                ];
            } else {
                throw new HttpException(500, 'Không xác thực được dữ liệu gửi lên');
            }
        } else {
            throw new NotFoundHttpException('Đường dẫn sai cú pháp');
        }
    }

    /** tao-moi-doanh-nghiep */
    public function actionTaoMoiDoanhNghiep()
    {
        if (isset($_POST['id'])) {
            $model = new DoanhNghiep();
            $model->created = date('Y-m-d H:i:s');
            $model->status = 1;
            $model->save();
            return [
                'title' => 'Thông báo',
                'content' => 'Thêm doanh nghiệp thành công'
            ];
        }
    }

    /** update-chi-tiet-doanh-nghiep */
    public function actionUpdateChiTietDoanhNghiep()
    {
        if (strlen($_POST['DoanhNghiep']['dien_thoai']) != 10){
            throw new \yii\web\HttpException(500, 'Số điện thoại không hợp lệ!');
        }
        if (strlen($_POST['DoanhNghiep']['ma_so_thue']) >= 32){
            throw new \yii\web\HttpException(500, 'Mã số thuế không hợp lệ!');
        }
        if (strlen($_POST['DoanhNghiep']['nguoi_dai_dien']) >= 32){
            throw new \yii\web\HttpException(500, 'Tên người đại diện không được quá 32 kí tự!');
        }
        $doanhNghiep = DoanhNghiep::findOne([$_POST['DoanhNghiep']['id']]);
        $arrUpdate = ['name', 'nguoi_dai_dien', 'dien_thoai', 'ma_so_thue', 'dia_chi', 'email', 'user_quan_li'];
        foreach ($arrUpdate as $item) {
            $doanhNghiep->$item = $_POST['DoanhNghiep'][$item];
        }
        if ($doanhNghiep->save()){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => 'Thông báo',
                'content' => 'Cập nhật doanh nghiệp thành công!'
            ];
        } else {
            throw new \yii\web\HttpException(500, Html::errorSummary($doanhNghiep));

        }


    }
}
