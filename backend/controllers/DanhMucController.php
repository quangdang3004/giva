<?php

namespace backend\controllers;

use common\models\myAPI;
use Yii;
use backend\models\DanhMuc;
use backend\models\search\DanhMucSearch;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use \yii\web\Response;
use yii\helpers\Html;

class DanhMucController extends CoreController{

    public function behaviors()
    {
        $arr_action = ['index', 'view','create','update','delete', 'get-khu-vuc', 'chi-tiet-khu-vuc',
            'update-danh-muc', 'them-moi', 'get-khu-vuc-chon-nhieu'];
        $rules = [];
        foreach ($arr_action as $item) {
            $rules[] = [
                'actions' => [$item],
                'allow' => true,
//                'matchCallback' => myAPI::isAccess2($controller, $item)
                'matchCallback' => function ($rule, $action) {
                    $action_name =  strtolower(str_replace('action', '', $action->id));
                    return \Yii::$app->user->id == 1 || myAPI::isAccess2('DanhMuc', $action_name);
                }
            ];
        }
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /** index */
    public function actionIndex($doi_tuong)
    {
        $searchModel = new DanhMucSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $doi_tuong);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'doi_tuong' => $doi_tuong
        ]);
    }


    /** view */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Danh mục ".$model->name,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('<i class="fa fa-close"></i> Đóng lại',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('<i class="fa fa-save"></i> Cập nhật',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /** create */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new DanhMuc();

        $quan_huyen = ArrayHelper::map(DanhMuc::find()->andFilterWhere(['type'=> DanhMuc::QUAN_HUYEN, 'active' => 1])->all(),'id','name');

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Tạo mới danh mục",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'quan_huyen' => $quan_huyen
                    ]),
                    'footer'=> Html::button('<i class="fa fa-close"></i> Đóng lại',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('<i class="fa fa-save"></i> Lưu lại',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Tạo mới danh mục",
                    'danhmuc' => $model,
                    'content'=>'<span class="text-success">Thêm danh mục thành công!</span>',
                    'footer'=> Html::button('<i class="fa fa-close"></i> Đóng lại',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('<i class="glyphicon glyphicon-plus"></i> Tạo thêm',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Tạo mới danh mục",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'quan_huyen' => $quan_huyen
                    ]),
                    'footer'=> Html::button('<i class="fa fa-close"></i> Đóng lại',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('<i class="fa fa-save"></i> Lưu lại',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['index']);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'quan_huyen' => $quan_huyen
                ]);
            }
        }
    }

    /** update */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $quan_huyen = ArrayHelper::map(DanhMuc::find()->andFilterWhere(['type'=> DanhMuc::QUAN_HUYEN, 'active' => 1])->all(),'id','name');

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Cập nhật danh mục ".$model->name,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                        'quan_huyen' => $quan_huyen
                    ]),
                    'footer'=> Html::button('<i class="fa fa-close"></i> Đóng lại',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('<i class="fa fa-save"></i> Lưu lại',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Thông tin {$model->name}",
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                        'quan_huyen' => $quan_huyen
                    ]),
                    'footer'=> Html::button('<i class="fa fa-close"></i> Đóng lại',['class'=>'btn btn-default pull-left btn-edit-after-save','data-dismiss'=>"modal"]).
                            Html::a('<i class="fa fa-edit"></i> Chỉnh sửa',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
            }else{
                 return [
                     'title'=> "Cập nhật danh mục ".$model->name,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                        'quan_huyen' => $quan_huyen
                    ]),
                    'footer'=> Html::button('<i class="fa fa-close"></i> Đóng lại',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('<i class="fa fa-save"></i> Lưu lại',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'quan_huyen' => $quan_huyen
                ]);
            }
        }
    }

    /** delete */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->active = 0;
        $model->save();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            return $this->redirect(['index']);
        }


    }
//
     /** bulk-deleye */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' ));
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk)->updateAttributes(['active' => 0]);
//            $model->delete();
        }

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            return $this->redirect(['index']);
        }

    }

    protected function findModel($id)
    {
        if (($model = DanhMuc::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Danh mục không tồn tại');
        }
    }

    //get-khu-vuc
    /** get-khu-vuc */
    public function actionGetKhuVuc(){
        if(!empty($_POST['value'])){
            $model = DanhMuc::findOne($_POST['value']);
            $data = DanhMuc::find()->select(['id', 'name'])
                ->andFilterWhere(['parent_id' => $model->id])
                ->andFilterWhere(['active' => 1])
                ->all();
        }else{
            $data = '';
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $data;
    }
    /** chi-tiet-khu-vuc */
    public function actionChiTietKhuVuc()
    {
        if(Yii::$app->request->isAjax){
            if(isset($_POST['id'])){
                $model = DanhMuc::findOne($_POST['id']);
                $name = $model->name;
                $listTinh = DanhMuc::find()
                    -> select(['name'])
                    -> andFilterWhere(['parent_id' => $_POST['id']])
                    -> all();
//                VarDumper::dump($listTinh, 10, true); exit();
                Yii::$app->response->format = Response::FORMAT_JSON;
                if($listTinh == []){
                    $title = "Thông tin chi tiết loại ngành ".$name;
                    return [
                        'title' => $title,
                        'content' => $this->renderAjax('view',  [
                            'model' => $model,
                        ]),
                    ];
                }
                else{
                    $title = "Danh sách tỉnh thành khu vực ".$name;
                    return [
                        'title' => $title,
                        'content' => $this->renderAjax('chi_tiet_khu_vuc',
                            ['listTinh' => $listTinh]),
                    ];
                }
//                VarDumper::dump($title, 10, true); exit();

            }else{
                throw new HttpException(500, 'Không xác thực được dữ liệu gửi lên');
            }
        }else{
            throw new NotFoundHttpException('Đường dẫn sai cú pháp');
        }
    }
    /** update-danh-muc */
    public function actionUpdateDanhMuc()
    {
        if(Yii::$app->request->isAjax){
            if(isset($_POST['id'])){
                $model = DanhMuc::findOne($_POST['id']);
                $name = $model->name;
                $listTinh = DanhMuc::find()
                    -> select(['name'])
                    -> andFilterWhere(['parent_id' => $_POST['id']])
                    -> all();
//                VarDumper::dump($listTinh, 10, true); exit();
                Yii::$app->response->format = Response::FORMAT_JSON;
                if($listTinh == []){
                    $title = "Cập nhật thông tin loại ngành ".$name;
                    return [
                        'title' => $title,
                        'content' => $this->renderAjax('update_danh_muc',  [
                            'model' => $model,
                        ]),
                    ];
                }
                else{
                    $title = "Cập nhật thông tin khu vực ".$name;
                    return [
                        'title' => $title,
                        'content' => $this->renderAjax('update_danh_muc',  [
                            'model' => $model,
                        ]),
                    ];
                }
//                VarDumper::dump($title, 10, true); exit();

            }else{
                throw new HttpException(500, 'Không xác thực được dữ liệu gửi lên');
            }
        }else{
            throw new NotFoundHttpException('Đường dẫn sai cú pháp');
        }
    }

    //thêm danh mục loại ngành
    /** them-moi */
    public function actionThemMoi()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if(isset($_POST['id'])){
            if ($_POST['id'] == '') {
                $model = new DanhMuc();
            } else {
                $model = DanhMuc::findOne(['id' => $_POST['id']]);
            }

            if (isset($_POST['name'])){
                if (trim($_POST['name']) == ''){
                    throw new HttpException(500, 'Vui lòng nhập loại ngành muốn thêm');
                }
            }

            $model->name = $_POST['name'];
            $model->type = 'Loại nghành';
            $model->active = 1;
//        VarDumper::dump($model->name,10, true); exit();
            $model->save();
            return [
                'title' => 'Thông báo',
                'content' => $_POST['id'] == '' ? 'Thêm loại ngành thành công' : 'Cập nhật loại ngành thành công'
            ];
        }
    }

    /** get-khu-vuc-chon-nhieu */
    public function actionGetKhuVucChonNhieu(){
        if (!empty($_POST['value']))
        {
            $model = ArrayHelper::map(DanhMuc::find()->andFilterWhere(['in', 'name', $_POST['value']])->all(), 'id', 'id');
            $data =DanhMuc::find()->select(['id', 'name'])
                ->andFilterWhere(['in', 'parent_id', $model])
                ->andFilterWhere(['active'=>1])
                ->andFilterWhere(['type' => $_POST['type']])
                ->all();
        }
        else
        {
            $data = '';
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $data;
    }

//    public function actionTaoMoiDanhMuc()
//    {
//        $doi_tuong = new DanhMuc();
//        $doi_tuong->name = $_POST['name'];
//        if ($doi_tuong->save()){
//            return [
//                'title' => 'Thông báo',
//                'content' => 'Tạo danh mục thành công!'
//            ];
//        }else{
//            throw new NotFoundHttpException('123');
//
//        }
//    }
}
