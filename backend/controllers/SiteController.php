<?php

namespace backend\controllers;
//
use backend\models\AnhChiTietNguoiDung;
use backend\models\AnhNguoiDung;
use backend\models\AnhTienChi;
use backend\models\BangChamCong;
use backend\models\BannerApp;
use backend\models\CanDoiTaiChinh;
use backend\models\CauHinh;
use backend\models\ChiaSeKhachHang;
use backend\models\ChiTietTienChi;
use backend\models\DanhMuc;
use backend\models\DoanhNghiep;
use backend\models\DonHang;
use backend\models\GhiChu;
use backend\models\GiaoDich;
use backend\models\LichSuChi;
use backend\models\LichSuDoanhThu;
use backend\models\LichSuUser;
use backend\models\QuanLyKhachHang;
use backend\models\QuanLySanPham;
use backend\models\SanPham;
use backend\models\TaiChinh;
use backend\models\ThongTinBanHang;
use backend\models\ThuongPhat;
use backend\models\TienChi;
use backend\models\TrangThaiSanPham;
use backend\models\UserVaiTro;
use backend\models\YeuCauXacThuc;
use common\models\myAPI;
use common\models\User;
use Yii;
use yii\base\Security;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\filters\AccessControl;
use common\models\LoginForm;
use yii\helpers\VarDumper;
use yii\web\HttpException;
use yii\web\Response;

class SiteController extends CoreController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'update-hotline'],
                        'allow' => true,
//                        'roles' => ['?']
                    ],
                    [
                        'actions' => ['error'],
                        'allow' => true
                    ],
                    [
                        'actions' => ['logout', 'loadform', 'load-form-modal', 'doimatkhau', 'index', 'danh-sach-khach-hang', 'update-san-pham', 'test'],
                        'allow' => true,
//                        'matchCallback' => function($rule, $action){
//                            return Yii::$app->user->identity->username == 'adamin';
//                        }
                        'roles' => ['@']
                    ],
                ],
//                'denyCallback' => function ($rule, $action) {
//                    throw new Exception('You are not allowed to access this page', 404);
//                }
            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /** index */
    public function actionIndex()
    {
        $this->checkIfDonHangIsExpried();
        $this->redirect(Url::toRoute(['user/index', 'doi_tuong' => 'nha_tuyen_dung']));
    }

    /** login */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $vai_tro = UserVaiTro::find()->select(['username', 'vai_tro_id'])
                ->andFilterWhere(['username' => $_POST['LoginForm']['username']])->one();
            if (!is_null($vai_tro)){
                if ($vai_tro->vai_tro_id != 1)
                    throw new HttpException(500, 'Bạn không thể truy cập vào hệ thống');

            }

            return $this->goBack();
        } else {
            return $this->renderPartial('login', [
                'model' => $model,
            ]);
        }
    }

    /** logout */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /** error */
    public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            if (Yii::$app->request->isAjax) {
                echo myFuncs::getMessage($exception->getMessage(), 'danger', "Lỗi!");
                exit;
            }
            return $this->render('error', ['exception' => $exception]);
        }
    }

    /** doimatkhau */
    public function actionDoimatkhau()
    {
        $sercurity = new Security();
        $user = User::findOne(Yii::$app->user->getId());
        if (!Yii::$app->security->validatePassword($_POST['matkhaucu'], $user->password_hash))
            throw new HttpException(500, myAPI::getMessage('danger', 'Mật khẩu cũ không đúng!'));
        else {
            $matkhaumoi = $sercurity->generatePasswordHash(trim($_POST['matkhaumoi']));
            User::updateAll(['password_hash' => $matkhaumoi], ['id' => Yii::$app->user->getId()]);
            echo Json::encode(['message' => myAPI::getMessage('success', 'Đã đổi mật khẩu thành công')]);
        }
    }

    /** loadform */
    public function actionLoadform()
    {
        $content = '';
        $title = '';

        if (isset($_POST['id'])) {
            if ($_POST['type'] == 'duyet_san_pham') {
                $title = "Duyệt sản phẩm";
                if (!is_null($_POST['san_pham_id'])) {
                    $san_pham = SanPham::findOne($_POST['san_pham_id']);
                    $dia_chi = implode(', ', [$san_pham->duong_pho_id != '' ? $san_pham->duongPho->name : "", $san_pham->quan->name]);
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return [
                        'content' => $this->renderAjax('../san-pham/form_duyet_san_pham', [
                            'model' => $san_pham,
                            'dia_chi' => $dia_chi
                        ]),
                        'title' => $title
                    ];
                }
            } else if ($_POST['type'] == 'tai-excel-kho-san-pham') {
                $content = $this->renderAjax('../san-pham/form-tai-excel', [
                ]);
                $title = 'Tải danh sách tìm kiếm';
            } elseif ($_POST['type'] == 'huy-yeu-cau-xac-thuc') {
                if (isset($_POST['id'])) {
                    $model = YeuCauXacThuc::findOne($_POST['id']);

                    if ($model->type == 'Cá nhân') {
                        $title = 'Hủy yêu cầu xác thực ' . $model->user->hoten;

                        $content = $this->renderAjax('../yeu-cau-xac-thuc/huy-yeu-cau', [
                            'model' => $model,
                        ]);
                    }

                    if ($model->type != 'Cá nhân') {
                        $title = 'Hủy yêu cầu xác thực ' . $model->doanhNghiep->name;

                        $content = $this->renderAjax('../yeu-cau-xac-thuc/huy-yeu-cau', [
                            'model' => $model,
                        ]);
                    }

                } else {
                    throw new HttpException(500, 'Không xác thực được dữ liệu');
                }
            } elseif ($_POST['type'] == 'xem-chi-tiet-xac-thuc') {
                if (isset($_POST['id'])) {
                    $model = YeuCauXacThuc::findOne($_POST['id']);
                    $link_anh = CauHinh::find()
                        ->andFilterWhere(['name' => 'images'])
                        ->one();

                    if ($model->type == 'Cá nhân') {
                        $title = 'Chi tiết yêu cầu xác thực ' . $model->user->hoten;

                        $content = $this->renderAjax('../yeu-cau-xac-thuc/xem-chi-tiet', [
                            'model' => $model,
                            'link_anh' => $link_anh
                        ]);
                    }

                    if ($model->type != 'Cá nhân') {
                        $title = 'Chi tiết yêu cầu xác thực' . $model->doanhNghiep->name;

                        $content = $this->renderAjax('../yeu-cau-xac-thuc/xem-chi-tiet-doanh-nghiep', [
                            'model' => $model,
                            'link_anh' => $link_anh
                        ]);
                    }

                } else {
                    throw new HttpException(500, 'Không xác thực được dữ liệu');
                }
            } else if ($_POST['type'] == 'xem-chi-tiet-nha-tuyen-dung') {
                $model = User::findOne([$_POST['id']]);
                $anh_chi_tiet_nguoi_dung = AnhChiTietNguoiDung::findAll(['user_id' => $model->id]);
                if ($model->type == 'Nhà tuyển dụng') {
                    $title = 'Chi tiết nhà tuyển dụng ' . $model->hoten;
                    $giao_dich = GiaoDich::findAll(['nguoi_cung_cap_id' => $model->id]);

                    $content = $this->renderAjax('../user/xem_nguoi_dung', [
                        'model' => $model,
                        'anh_chi_tiet_nguoi_dung' => $anh_chi_tiet_nguoi_dung,
                        'giao_dich' => $giao_dich
                    ]);
                }
                if ($model->type != 'Nhà tuyển dụng') {
                    $title = 'Chi tiết ' . $model->hoten;

                    $content = $this->renderAjax('../user/xem_nguoi_dung', [
                        'model' => $model,
                    ]);
                }
            } else if ($_POST['type'] == 'cong-xu-nha-tuyen-dung') {
                $model = User::findOne([$_POST['id']]);
                $title = 'Cộng xu người dùng ' . $model->hoten;

                $content = $this->renderAjax('../user/form_cong_xu', [
                    'model' => $model,
                ]);
            } else if ($_POST['type'] == 'them-banner-app') {
                if (isset($_POST['id'])) {
                    if ($_POST['id'] != '') {
                        $model = BannerApp::findOne(['id' => $_POST['id']]);

                        $title = 'Cập nhật Banner App';
                    } else {
                        $model = new BannerApp();

                        $title = 'Thêm Banner App';
                    }

                    $content = $this->renderAjax('../banner-app/create', [
                        'model' => $model
                    ]);
                }

            } else if ($_POST['type'] == 'them_moi_danh_muc') {
                if (isset($_POST['id'])) {
                    if ($_POST['id'] != '') {
                        $model = DanhMuc::findOne(['id' => $_POST['id']]);

                        $title = 'Cập nhật loại ngành';
                    } else {
                        $model = new DanhMuc();
                        $title = 'Thêm loại ngành';
                    }

                    $content = $this->renderAjax('../danh-muc/them_moi', [
                        'model' => $model
                    ]);
                }
//                VarDumper::dump($content, 10, true); exit();

            } else if ($_POST['type'] == 'update_doanh_nghiep') {
//                VarDumper::dump($_POST['type'], 10, true); exit();
                if (isset($_POST['id'])) {
                    if ($_POST['id'] == '')
                        $model = new DoanhNghiep();
                    $title = 'Thêm doanh nghiệp';
                    $content = $this->renderAjax('../doanh-nghiep/tao_moi_doanh_nghiep', [
                        'model' => $model
                    ]);
                }
            } else if ($_POST['type'] == 'them_thanh_vien') {
//                VarDumper::dump($_POST['type'], 10, true); exit();
                if (isset($_POST['id'])) {
                    if ($_POST['id'] == '')
                        $model = new User();
                    $title = 'Thêm thành viên';
                    $content = $this->renderAjax('../user/create', [
                        'model' => $model
                    ]);
                }
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return ([
                'content' => $content,
                'title' => $title
            ]);
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return ([
            'content' => $content,
            'title' => $title
        ]);
    }

    function checkIfDonHangIsExpried()
    {
        $donHang = DonHang::find()->all();

        foreach ($donHang as $item) {
            if (strtotime($item->han_tuyen) <= strtotime(date('Y-m-d H:i:s'))) {
                $item->updateAttributes(['trang_thai' => DonHang::HET_HAN]);
            }
        }
        return [
            'status' => '1',
        ];
    }
}
