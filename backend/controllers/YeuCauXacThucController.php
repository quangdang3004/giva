<?php

namespace backend\controllers;

use backend\models\CauHinh;
use backend\models\GiayToLienKet;
use backend\models\LichTrangThaiYeuCauXacThuc;
use backend\models\search\QuanLyYeuCauXacThucSearch;
use common\models\myAPI;
use common\models\User;
use http\Exception\BadConversionException;
use Yii;
use common\widgets\Alert;
use backend\models\YeuCauXacThuc;
use backend\models\search\YeuCauXacThucSearch;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * YeuCauXacThucController implements the CRUD actions for YeuCauXacThuc model.
 */
class YeuCauXacThucController extends CoreController
{
    public function behaviors()
    {
        $arr_action = ['index', 'create','update','delete','view',
            'xem-chi-tiet', 'xac-minh-yeu-cau', 'huy-yeu-cau-xac-thuc'];
        $rules = [];
        foreach ($arr_action as $item) {
            $rules[] = [
                'actions' => [$item],
                'allow' => true,
//                'matchCallback' => myAPI::isAccess2($controller, $item)
                'matchCallback' => function ($rule, $action) {
                    $action_name =  strtolower(str_replace('action', '', $action->id));
                    return \Yii::$app->user->id == 1 || myAPI::isAccess2('YeuCauXacThuc', $action_name);
                }
            ];
        }
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Lists all YeuCauXacThuc models.
     * @return mixed
     */
    public function actionIndex($doituong)
    {    
        $searchModel = new QuanLyYeuCauXacThucSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $doituong);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'doituong' => $doituong
        ]);
    }


    /**
     * Displays a single YeuCauXacThuc model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "YeuCauXacThuc #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new YeuCauXacThuc model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new YeuCauXacThuc();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new YeuCauXacThuc",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Create new YeuCauXacThuc",
                    'content'=>'<span class="text-success">Create YeuCauXacThuc success</span>',
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Create new YeuCauXacThuc",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing YeuCauXacThuc model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update YeuCauXacThuc #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "YeuCauXacThuc #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Update YeuCauXacThuc #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing YeuCauXacThuc model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing YeuCauXacThuc model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkdelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the YeuCauXacThuc model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return YeuCauXacThuc the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = YeuCauXacThuc::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /** xem-chi-tiet */
    public function actionXemChiTiet($id)
    {
        $model = YeuCauXacThuc::findOne($id);
        $link_anh = CauHinh::find()
            ->andFilterWhere(['name' => 'images'])
            ->one();
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;

            return [
                'title'=> "Yêu cầu xác thực ".$model->user->hoten,
                'content'=>$this->renderAjax('xem-chi-tiet.php', [
                    'model' => $model,
                    'link_anh' => $link_anh
                ]),
                'footer'=> Html::button('Close',['class'=>'btn btn-default pull-right','data-dismiss'=>"modal"])
            ];
        }else{
            return $this->render('xem-chi-tiet.php', [
                'model' => $model,
                'link_anh' => $link_anh
            ]);
        }
    }

    /** xac-minh-yeu-cau */
    public function actionXacMinhYeuCau(){
        $yeu_cau = YeuCauXacThuc::findOne($_POST['id']);
        if (is_null($yeu_cau))
            throw new HttpException(500, 'Yêu cầu xác minh không tồn tại.');

        if ($yeu_cau->trang_thai == YeuCauXacThuc::TU_CHOI){
            throw new HttpException(500, 'Yêu cầu xác minh đã bị từ chối.');
        }
        if ($yeu_cau->trang_thai != YeuCauXacThuc::DA_XAC_MINH){
            $lich_su = new LichTrangThaiYeuCauXacThuc();
            $lich_su->yeu_cau_id = $yeu_cau->id;
            $lich_su->trang_thai = YeuCauXacThuc::DA_XAC_MINH;
            $lich_su->ghi_chu = $yeu_cau->ghi_chu;
            $lich_su->created = date('Y-m-d H:i:s');
            if (!$lich_su->save()){
                throw new HttpException(500, Html::errorSummary($lich_su));
            }
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => 'Xác thực yêu cầu thành công'
        ];

    }

    /** huy-yeu-cau-xac-thuc */
    public function actionHuyYeuCauXacThuc(){
        $yeu_cau = YeuCauXacThuc::findOne([$_POST['id']]);
        if (is_null($yeu_cau))
            throw new HttpException(500, 'Yêu cầu không tồn tại.');

        if ($yeu_cau->trang_thai != YeuCauXacThuc::TU_CHOI){
            $yeu_cau->updateAttributes(['ghi_chu' => $_POST['ghi_chu'][0]['value']]);

            $lich_su = new LichTrangThaiYeuCauXacThuc();
            $lich_su->yeu_cau_id = $yeu_cau->id;
            $lich_su->trang_thai = YeuCauXacThuc::TU_CHOI;
            $lich_su->ghi_chu = $yeu_cau->ghi_chu;
            $lich_su->created = date('Y-m-d H:i:s');
            if (!$lich_su->save()){
                throw new HttpException(500, Html::errorSummary($lich_su));
            }
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => 'Hủy yêu cầu thành công'
        ];
    }
}
