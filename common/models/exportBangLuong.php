<?php
namespace common\models;
/**
 * @property \PHPExcel $objPHPExcel
 */



use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use yii\base\Exception;
use yii\helpers\Html;
use yii\helpers\VarDumper;

/**
 * @author Nikola Kostadinov
 * @license MIT License
 * @version 0.3
 * @link http://yiiframework.com/extension/eexcelview/
 *
 * @fork 0.33ab
 * @forkversion 1.1
 * @author A. Bennouna
 * @organization tellibus.com
 * @license MIT License
 * @link https://github.com/tellibus/tlbExcelView
 */

/* Usage :
  $this->widget('application.components.widgets.tlbExcelView', array(
    'id'                   => 'some-grid',
    'dataProvider'         => $model->search(),
    'grid_mode'            => $production, // Same usage as EExcelView v0.33
    //'template'           => "{summary}\n{items}\n{exportbuttons}\n{pager}",
    'title'                => 'Some title - ' . date('d-m-Y - H-i-s'),
    'creator'              => 'Your Name',
    'subject'              => mb_convert_encoding('Something important with a date in French: ' . utf8_encode(strftime('%e %B %Y')), 'ISO-8859-1', 'UTF-8'),
    'description'          => mb_convert_encoding('Etat de production généré à la demande par l\'administrateur (some text in French).', 'ISO-8859-1', 'UTF-8'),
    'lastModifiedBy'       => 'Some Name',
    'sheetTitle'           => 'Report on ' . date('m-d-Y H-i'),
    'keywords'             => '',
    'category'             => '',
    'landscapeDisplay'     => true, // Default: false
    'A4'                   => true, // Default: false - ie : Letter (PHPExcel default)
    'RTL'                  => false, // Default: false
    'pageFooterText'       => '&RThis is page no. &P of &N pages', // Default: '&RPage &P of &N'
    'automaticSum'         => true, // Default: false
    'decimalSeparator'     => ',', // Default: '.'
    'thousandsSeparator'   => '.', // Default: ','
    //'displayZeros'       => false,
    //'zeroPlaceholder'    => '-',
    'sumLabel'             => 'Column totals:', // Default: 'Totals'
    'borderColor'          => '00FF00', // Default: '000000'
    'bgColor'              => 'FFFF00', // Default: 'FFFFFF'
    'textColor'            => 'FF0000', // Default: '000000'
    'rowHeight'            => 45, // Default: 15
    'headerBorderColor'    => 'FF0000', // Default: '000000'
    'headerBgColor'        => 'CCCCCC', // Default: 'CCCCCC'
    'headerTextColor'      => '0000FF', // Default: '000000'
    'headerHeight'         => 10, // Default: 20
    'footerBorderColor'    => '0000FF', // Default: '000000'
    'footerBgColor'        => '00FFCC', // Default: 'FFFFCC'
    'footerTextColor'      => 'FF00FF', // Default: '0000FF'
    'footerHeight'         => 50, // Default: 20
    'columns'              => $grid // an array of your CGridColumns
)); */

class exportBangLuong
{
    public $data;
    public $year;
    public $month;
    public $path_file;
//    public $tienTraLai;
    //Document properties
    public $creator = 'MINHHIEN SETE CO.,Ltd';
    public $title = 'Bảng lương';
    public $subject = 'Bảng lương';
    public $description = '';
    public $category = '';
    public $lastModifiedBy = 'MinhHienSETE CO.,Ltd';
    public $keywords = '';
    public $sheetTitle = 'Bang luong';
    public $legal = 'Bang luong';
    public $landscapeDisplay = false;
    public $A4 = false;
    public $RTL = false;
    public $pageFooterText = '&RPage &P of &N';

    //config
    public $autoWidth = true;
    public $exportType = 'Excel2007';
    public $disablePaging = true;
    public $filename = null; //export FileName
    public $stream = true; //stream to browser
    public $grid_mode = 'export'; //Whether to display grid ot export it to selected format. Possible values(grid, export)
    public $grid_mode_var = 'grid_mode'; //GET var for the grid mode

    //options
    public $automaticSum = false;
    public $sumLabel = 'Totals';
    public $decimalSeparator = '.';
    public $thousandsSeparator = ',';
    public $displayZeros = false;
    public $zeroPlaceholder = '-';
    public $border_style;
    public $borderColor = '000000';
    public $bgColor = 'FFFFFF';
    public $textColor = '000000';
    public $rowHeight = 15;
    public $headerBorderColor = '000000';
    public $headerBgColor = 'CCCCCC';
    public $headerTextColor = '000000';
    public $headerHeight = 20;
    public $footerBorderColor = '000000';
    public $footerBgColor = 'FFFFCC';
    public $footerTextColor = '0000FF';
    public $footerHeight = 20;
    public static $fill_solid;
    public static $papersize_A4;
    public static $orientation_landscape;
    public static $horizontal_center;
    public static $horizontal_right;
    public static $vertical_center;
    public static $horizontal_left;
    public static $style = array();
    public static $headerStyle = array();
    public static $footerStyle = array();
    public static $summableColumns = array();

    public static $objPHPExcel;
    public static $activeSheet;

    //buttons config
    public $exportButtonsCSS = 'summary';
    public $exportButtons = array('Excel2007');
    public $exportText = 'Export to: ';

    //callbacks
    public $onRenderHeaderCell = null;
    public $onRenderDataCell = null;
    public $onRenderFooterCell = null;

    //mime types used for streaming
    public $mimeTypes = array(
        'Excel5'	=> array(
            'Content-type'=>'application/vnd.ms-excel',
            'extension'=>'xls',
            'caption'=>'Excel(*.xls)',
        ),
        'Excel2007'	=> array(
            'Content-type'=>'application/vnd.ms-excel',
            'extension'=>'xlsx',
            'caption'=>'Excel(*.xlsx)',
        ),
        'PDF'		=>array(
            'Content-type'=>'application/pdf',
            'extension'=>'pdf',
            'caption'=>'PDF(*.pdf)',
        ),
        'HTML'		=>array(
            'Content-type'=>'text/html',
            'extension'=>'html',
            'caption'=>'HTML(*.html)',
        ),
        'CSV'		=>array(
            'Content-type'=>'application/csv',
            'extension'=>'csv',
            'caption'=>'CSV(*.csv)',
        )
    );


    /**
     * @param $activeSheet Worksheet
     */
    public function renderBody($activeSheet){
        $data = $this->data;
        $year = $this->year;
        $month = $this->month;
        $activeSheet
            ->setCellValue("A2", '(Tháng '.$month.'/'.$year.')');
        $tong_ngay_cong = 0;
        $tong_he_so_luong = 0;
        $tong_luong_thuc_te =0;
        $tong_luong_co_ban = 0;
        $tong_tien_phat = 0;
        $tong_tien_thuong = 0;
        $tong_tien_hoa_hong = 0;
        $tong_tien_linh = 0;
        $tong_doanh_so_hh = 0;
        $tong_hh_ve_cong_ty = 0;
        $tong_hoa_hong_chua_lay = 0;
        $tong_so_can = 0;

        $dong =4;
        foreach ($data as $index => $item) {
            $tong_ngay_cong += $item['ngay_cong'];
            $tong_luong_thuc_te += $item['luong_thuc_te'];
            $tong_luong_co_ban += $item['luong_co_ban'];
            $tong_tien_phat += $item['tien_phat'];
            $tong_tien_thuong += $item['tien_thuong'];
            $tong_tien_linh += $item['thuc_linh'];
            $tong_tien_hoa_hong += $item['hoa_hong'];
            $tong_doanh_so_hh += $item['doanh_thu_hoa_hong'];
            $tong_hh_ve_cong_ty += $item['hoa_hong_ve_cong_ty'];
            $tong_hoa_hong_chua_lay += $item['hoa_hong_chua_lay'];
            $tong_so_can += $item['so_can'];
            $activeSheet
                ->setCellValue("A{$dong}", $index)
                ->setCellValue("B{$dong}", $item['ngay_cong'])
                ->setCellValue("C{$dong}", $item['he_so_luong'])
                ->setCellValue("D{$dong}", $item['luong_thuc_te'])
                ->setCellValue("E{$dong}", $item['luong_co_ban'])
                ->setCellValue("F{$dong}", $item['tien_thuong'])
                ->setCellValue("G{$dong}", $item['tien_phat'])
                ->setCellValue("H{$dong}", $item['hoa_hong'])
                ->setCellValue("I{$dong}", $item['thuc_linh'])
                ->setCellValue("J{$dong}", $item['san_pham_ban_duoc'])
                ->setCellValue("K{$dong}", $item['so_can'])
                ->setCellValue("L{$dong}", $item['doanh_thu_hoa_hong'])
                ->setCellValue("M{$dong}", $item['hoa_hong_ve_cong_ty']);

            $dong++;
        }
        $activeSheet
            ->setCellValue("A{$dong}", 'Tổng')
            ->setCellValue("B{$dong}", $tong_ngay_cong)
            ->setCellValue("D{$dong}", $tong_luong_thuc_te)
            ->setCellValue("E{$dong}", $tong_luong_co_ban)
            ->setCellValue("F{$dong}", $tong_tien_thuong)
            ->setCellValue("G{$dong}", $tong_tien_phat)
            ->setCellValue("H{$dong}", $tong_tien_hoa_hong)
            ->setCellValue("I{$dong}", $tong_tien_linh)
            ->setCellValue("K{$dong}", $tong_so_can)
            ->setCellValue("L{$dong}", $tong_doanh_so_hh)
            ->setCellValue("M{$dong}", $tong_hh_ve_cong_ty);
        $dong++;
        $activeSheet
            ->setCellValue("K{$dong}", 'Hoa hồng chưa lấy')
            ->setCellValue("L{$dong}", $tong_hoa_hong_chua_lay);
        formatExcel::setBorder($activeSheet, "A2:M{$dong}");
    }

    public function run()
    {
        $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load(dirname(dirname(__DIR__)).'/common/template/MAU_FILE_BANG_LUONG.xlsx');
        $activeSheet = $objPHPExcel->getActiveSheet();
        $this->renderBody($activeSheet);
        $this->filename = 'BANG_LUONG_'.time().'.xlsx';;
        $this->path_file.=$this->filename;
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Xlsx'); //\PHPExcel_IOFactory::createWriter($objPHPExcel, $this->exportType);
        $objWriter->setPreCalculateFormulas(true);

        if (!$this->stream) {
            $objWriter->save($this->path_file);
        } else {
            //output to browser
            if(!$this->filename) {
                $this->filename = $this->title;
            }
            $this->cleanOutput();
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-type: '.$this->mimeTypes[$this->exportType]['Content-type']);
            header('Content-Disposition: attachment; filename="' . $this->filename . '.' . $this->mimeTypes[$this->exportType]['extension'] . '"');
            header('Cache-Control: max-age=0');
            $objWriter->save('php://output');
            exit;
        }
        return $this->filename;
    }

    /**
     * Returns the corresponding Excel column.(Abdul Rehman from yii forum)
     *
     * @param int $index
     * @return string
     */
    public function columnName($index)
    {
        --$index;
        if (($index >= 0) && ($index < 26)) {
            return chr(ord('A') + $index);
        } else if ($index > 25) {
            return ($this->columnName($index / 26)) . ($this->columnName($index%26 + 1));
        } else {
            throw new Exception("Invalid Column # " . ($index + 1));
        }
    }

    /**
     * Performs cleaning on mutliple levels.
     *
     * From le_top @ yiiframework.com
     *
     */
    private static function cleanOutput()
    {
        for ($level = ob_get_level(); $level > 0; --$level) {
            @ob_end_clean();
        }
    }
}
