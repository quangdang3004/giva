<?php

namespace common\models;

use backend\models\AnhChiTietNguoiDung;
use backend\models\ChiTietTienDoGiaoDich;
use backend\models\DanhGia;
use backend\models\DoanhNghiep;
use backend\models\DonHang;
use backend\models\GiaoDich;
use backend\models\LichSuTrangThaiDonHang;
use backend\models\LichSuTrangThaiUser;
use backend\models\NhaTuyenDungCongTac;
use backend\models\Vaitrouser;
use backend\models\YeuCauXacThuc;
use Yii;
use yii\base\NotSupportedException;
use yii\bootstrap\Html;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\VarDumper;
use yii\web\HttpException;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "ad_user".
 *
 * @property int $id
 * @property string|null $username
 * @property string|null $password_hash
 * @property string|null $exp_token_reset
 * @property string|null $hoten
 * @property string|null $dia_chi
 * @property string|null $email
 * @property string|null $dien_thoai
 * @property string|null $cmnd
 * @property string|null $auth_key
 * @property string|null $exp_auth_key
 * @property string|null $avatar
 * @property float|null $so_du
 * @property string|null $gioi_thieu
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $type
 * @property string|null $trang_thai
 * @property int|null $status
 * @property int|null $doanh_nghiep_id
 * @property string|null $password_reset_token
 * @property string|null $password
 * @property int|null $active
 *
 * @property AnhChiTietNguoiDung[] $anhChiTietNguoiDungs
 * @property Chat[] $chats
 * @property ChatMember[] $chatMembers
 * @property ChiTietTienDoGiaoDich[] $chiTietTienDoGiaoDiches
 * @property DanhGia[] $danhGias
 * @property DanhGia[] $danhGias0
 * @property DanhGia[] $danhGias1
 * @property DoanhNghiep[] $doanhNghieps
 * @property DoanhNghiep[] $doanhNghieps0
 * @property DonHang[] $donHangs
 * @property GiaoDich[] $giaoDiches
 * @property GiaoDich[] $giaoDiches0
 * @property GiaoDich[] $giaoDiches1
 * @property LichSuTrangThaiDonHang[] $lichSuTrangThaiDonHangs
// * @property LichSUTrang[] $lichSuTrangThaiGiaoDiches
 * @property LichSuTrangThaiUser[] $lichSuTrangThaiUsers
 * @property NhaTuyenDungCongTac[] $nhaTuyenDungCongTacs
 * @property DoanhNghiep $doanhNghiep
 * @property Vaitrouser[] $vaitrousers
 * @property YeuCauXacThuc[] $yeuCauXacThucs
 * @property YeuCauXacThuc[] $yeuCauXacThucs0
 */
class User extends ActiveRecord implements IdentityInterface
{
    /**
     * {@inheritdoc}
     */

    public $vai_tros;
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    public static function tableName()
    {
        return 'ad_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['avatar', 'gioi_thieu', 'type', 'trang_thai'], 'string'],
            [['so_du'], 'number'],
            [['created_at', 'updated_at', 'vai_tros'], 'safe'],
            [['status', 'doanh_nghiep_id', 'active'], 'integer'],
            [['username', 'email'], 'string', 'max' => 50],
            [['password_hash', 'hoten', 'auth_key'], 'string', 'max' => 100],
            [['exp_token_reset'], 'string', 'max' => 32],
            [['dia_chi', 'exp_auth_key'], 'string', 'max' => 150],
            [['dien_thoai'], 'string', 'max' => 13],
            [['cmnd'], 'string', 'max' => 15],
            [['password_reset_token', 'password'], 'string', 'max' => 20],
            [['doanh_nghiep_id'], 'exist', 'skipOnError' => true, 'targetClass' => DoanhNghiep::className(), 'targetAttribute' => ['doanh_nghiep_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password_hash' => 'Password Hash',
            'exp_token_reset' => 'Exp Token Reset',
            'hoten' => 'Hoten',
            'dia_chi' => 'Dia Chi',
            'email' => 'Email',
            'dien_thoai' => 'Dien Thoai',
            'cmnd' => 'Cmnd',
            'auth_key' => 'Auth Key',
            'exp_auth_key' => 'Exp Auth Key',
            'avatar' => 'Avatar',
            'so_du' => 'So Du',
            'gioi_thieu' => 'Gioi Thieu',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'type' => 'Type',
            'trang_thai' => 'Trang Thai',
            'status' => 'Status',
            'doanh_nghiep_id' => 'Doanh Nghiep ID',
            'password_reset_token' => 'Password Reset Token',
            'password' => 'Password',
            'active' => 'Active',
        ];
    }

    /**
     * Gets query for [[AnhChiTietNguoiDungs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAnhChiTietNguoiDungs()
    {
        return $this->hasMany(AnhChiTietNguoiDung::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[Chats]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChats()
    {
        return $this->hasMany(Chat::className(), ['user_created' => 'id']);
    }

    /**
     * Gets query for [[ChatMembers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChatMembers()
    {
        return $this->hasMany(ChatMember::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[ChiTietTienDoGiaoDiches]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChiTietTienDoGiaoDiches()
    {
        return $this->hasMany(ChiTietTienDoGiaoDich::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[DanhGias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDanhGias()
    {
        return $this->hasMany(DanhGia::className(), ['user_duoc_rate_id' => 'id']);
    }

    /**
     * Gets query for [[DanhGias0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDanhGias0()
    {
        return $this->hasMany(DanhGia::className(), ['doanh_nghiep_id' => 'id']);
    }

    /**
     * Gets query for [[DanhGias1]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDanhGias1()
    {
        return $this->hasMany(DanhGia::className(), ['user_created_id' => 'id']);
    }

    /**
     * Gets query for [[DoanhNghieps]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDoanhNghieps()
    {
        return $this->hasMany(DoanhNghiep::className(), ['user_created' => 'id']);
    }

    /**
     * Gets query for [[DoanhNghieps0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDoanhNghieps0()
    {
        return $this->hasMany(DoanhNghiep::className(), ['user_quan_li' => 'id']);
    }

    /**
     * Gets query for [[DonHangs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDonHangs()
    {
        return $this->hasMany(DonHang::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[GiaoDiches]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGiaoDiches()
    {
        return $this->hasMany(GiaoDich::className(), ['user_created' => 'id']);
    }

    /**
     * Gets query for [[GiaoDiches0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGiaoDiches0()
    {
        return $this->hasMany(GiaoDich::className(), ['nguoi_cung_cap_id' => 'id']);
    }

    /**
     * Gets query for [[GiaoDiches1]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGiaoDiches1()
    {
        return $this->hasMany(GiaoDich::className(), ['nguoi_nhan_id' => 'id']);
    }

    /**
     * Gets query for [[LichSuTrangThaiDonHangs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLichSuTrangThaiDonHangs()
    {
        return $this->hasMany(LichSuTrangThaiDonHang::className(), ['user_createad_id' => 'id']);
    }

    /**
     * Gets query for [[LichSuTrangThaiGiaoDiches]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLichSuTrangThaiGiaoDiches()
    {
        return $this->hasMany(LichSuTrangThaiGiaoDich::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[LichSuTrangThaiUsers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLichSuTrangThaiUsers()
    {
        return $this->hasMany(LichSuTrangThaiUser::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[NhaTuyenDungCongTacs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNhaTuyenDungCongTacs()
    {
        return $this->hasMany(NhaTuyenDungCongTac::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[DoanhNghiep]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDoanhNghiep()
    {
        return $this->hasOne(DoanhNghiep::className(), ['id' => 'doanh_nghiep_id']);
    }


    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function setPassword($password)
    {
        $this->password_hash = \Yii::$app->security->generatePasswordHash($password);
    }

    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Gets query for [[Vaitrousers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVaitrousers()
    {
        return $this->hasMany(Vaitrouser::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[YeuCauXacThucs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getYeuCauXacThucs()
    {
        return $this->hasMany(YeuCauXacThuc::className(), ['user_created_id' => 'id']);
    }

    /**
     * Gets query for [[YeuCauXacThucs0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getYeuCauXacThucs0()
    {
        return $this->hasMany(YeuCauXacThuc::className(), ['user_id' => 'id']);
    }

    public function beforeSave($insert)
    {
        if($insert){
            $this->setPassword($this->password_hash);
            $this->active = 1;
            $this->generateAuthKey();
            $this->created_at = new Expression('NOW()');
        }
        else{
            $olduser = self::findOne($this->id);
            $oldPass = $olduser->password_hash;

            if($oldPass != $this->password_hash){
                $this->setPassword($this->password_hash);

                $session = Yii::$app->session;
                unset($session['old_id']);
                unset($session['timestamp']);
                $session->destroy();
            }
            $this->updated_at = date("Y-m-d H:i:s");

        }

        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($this->id != 1) {
            $vaitro = Vaitrouser::findAll(['user_id' => $this->id]);
            foreach ($vaitro as $item) {
                $item->delete();
            }
            if (isset($_POST['Vaitrouser'])) {
                foreach ($_POST['Vaitrouser'] as $item) {
                    $vaitronguoidung = new Vaitrouser();
                    $vaitronguoidung->vaitro_id = $item;
                    $vaitronguoidung->user_id = $this->id;
                    if (!$vaitronguoidung->save()) {
                        throw new HttpException(500, Html::errorSummary($vaitronguoidung));
                    }
                }
            }

        }


        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }
}
