<?php

namespace common\models;

use backend\models\QuanLyPhanQuyen;
use backend\models\QuanLySubtasks;
use backend\models\QuanLyTacVu;
use backend\models\SanPham;
use backend\models\Tasks;
use backend\models\Projects;
use backend\models\VaiTro;
use backend\models\Vaitrouser;
use kartik\mpdf\Pdf;
use yii\bootstrap\ActiveForm;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\Json;
use yii\helpers\Url;
use Yii;
use yii\bootstrap\Html;
use yii\helpers\VarDumper;
use yii\jui\DatePicker;
use yii\swiftmailer\Mailer;

class numberAPI
{
    /**
     * @param $title <p>Backlog<br/>Ready<br/>Work in Progress<br/>Done</p>
     * @return int
     * @throws \yii\db\Exception
     */
    /**
     * @param $active <p>one<br/>zero</p>
     * @return int
     * @throws \yii\db\Exception
     */
    public function getNumberByTitleColumn($title){
        $sql = 'SELECT count(id) as ket_qua FROM quan_ly_tac_vu  WHERE is_active = 1 and title_column=:t';
        $result = \Yii::$app->db->createCommand($sql, [':t' => $title])->queryAll();
        return intval($result[0]['ket_qua']);
    }

    public function getNumberByIsActiveTask($active){
        $sql = 'SELECT count(id) as ket_qua FROM quan_ly_tac_vu  WHERE is_active=:t';

        $result = \Yii::$app->db->createCommand($sql,[':t' => $active])->queryAll();
        return intval($result[0]['ket_qua']);
    }

    public function getNumberTasks(){
       $count_tac_vu_da_dong  = QuanLyTacVu::find()
           ->andWhere('date_due < :t and title_column <> :tt', [':t' => time(), ':tt' => Tasks::Done])
           ->andFilterWhere(['is_active' => 1])
           ->andFilterWhere(['is_active_project' => 1])->all();
       return count($count_tac_vu_da_dong);
    }

    public function getNumberKhachCho(){
        $count_khach_cho = QuanLySubtasks::find()
             ->andFilterWhere(['tags' =>'khach'])
             ->andWhere('converted_khach_hang is null')
             ->andWhere('due_description is not null')->all();

        return count($count_khach_cho);
    }

    public function getNumberKhachTiemNang(){
        $count_tiem_nang = QuanLySubtasks::find()
            ->andFilterWhere(['converted_khach_hang' => 1, 'khach_hang_tiem_nang'=>1])
            ->andWhere('due_description is not null')->all();
        return count($count_tiem_nang);
    }

    public function getNumberKhachHang(){
        $count_khach_hang = QuanLySubtasks::find()
            ->andFilterWhere(['converted_khach_hang' => 1,
                'khach_hang_tiem_nang' => 0, 'khach_hang'=>1])
            ->andWhere('due_description is not null')->all();
        return count($count_khach_hang);
    }

    public function getNumberKhachCu(){
        $count_khach_cu = QuanLySubtasks::find()
            ->andFilterWhere(['converted_khach_hang' => 1, 'khach_hang_tiem_nang' => 0, 'khach_hang'=>0])
            ->andWhere('due_description is not null')->all();
        return count($count_khach_cu);
    }

    public function getAllTask(){
        $sql = 'SELECT count(id) as ket_qua FROM quan_ly_tac_vu';

        $result = \Yii::$app->db->createCommand($sql)->queryAll();
        return intval($result[0]['ket_qua']);
    }

    public function getNumberByIsActivePro($active){
        $sql = 'SELECT count(id) as ket_qua FROM quan_ly_project  WHERE is_active=:t';

        $result = \Yii::$app->db->createCommand($sql,[':t' => $active])->queryAll();
        return intval($result[0]['ket_qua']);
    }



    public function getAllPro(){
        $sql = 'SELECT count(id) as ket_qua FROM quan_ly_project';

        $result = \Yii::$app->db->createCommand($sql)->queryAll();
        return intval($result[0]['ket_qua']);
    }

}
